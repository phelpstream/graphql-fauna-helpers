const webpack = require('./webpack.config')

module.exports = webpack.map(w => ({
  ...w,
  watch: true,
}))
