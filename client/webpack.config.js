// const webpack = require('webpack')
const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = [
  // {
  //   mode: 'development',
  //   entry: {
  //     VueGraphQLFaunaHelpers: './src/index.js',
  //   },
  //   output: {
  //     filename: 'gfh-client.common.js',
  //     library: 'VueGraphQLFaunaHelpers',
  //     libraryTarget: 'commonjs2',
  //   },

  //   module: {
  //     rules: [
  //       {
  //         test: /\.vue$/,
  //         loader: 'vue-loader',
  //       },
  //       {
  //         test: /\.js$/,
  //         exclude: /node_modules/,
  //         use: {
  //           loader: 'babel-loader',
  //         },
  //       },
  //       // { test: /\.json$/, loader: 'json-loader' },
  //     ],
  //   },
  //   resolve: {
  //     alias: {
  //       src: path.resolve(__dirname, '../src'),
  //       components: path.resolve(__dirname, '../components'),
  //     },
  //   },

  //   plugins: [new VueLoaderPlugin()],
  // },
  {
    mode: 'development',
    entry: {
      VueGraphQLFaunaHelpers: './src/index.js',
    },
    devtool: 'source-map',
    // target: 'node',F
    output: {
      filename: 'index.js',
      // library: 'VueGraphQLFaunaHelpers',
      libraryTarget: 'commonjs',
      // umdNamedDefine: true,
    },

    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
            },
          },
        },
      ],
    },
    resolve: {
      alias: {
        src: path.resolve(__dirname, '../src'),
        components: path.resolve(__dirname, '../components'),
      },
    },

    plugins: [new VueLoaderPlugin()],
  },
]
