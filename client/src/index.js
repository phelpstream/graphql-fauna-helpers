import * as componentsList from './../components/index'
import * as vuexHelpers from './vuex/helpers'
import * as vuexStore from './vuex/store'
import * as graphqlImport from './graphql'
import * as apiClient from './api/client'

export const install = (Vue, options) => {
  console.log("componentsList", componentsList);
  
  Object.values(componentsList).forEach(component => {
    console.log('INSTALL GFH CLIENT', component.name)
    Vue.component(component.name, component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

// export default {
//   install: install,
//   vuex: {
//     helpers: vuexHelpers,
//     store: vuexStore,
//   },
//   api: {
//     client: apiClient,
//   },
//   graphql: graphqlImport,
//   components: componentsList,
// }

// export const install = (Vue, options) => {
//   Object.values(componentsList).forEach(component => {
//     console.log('INSTALL GFH CLIENT', component.name)
//     Vue.component(component.name, component)
//   })
// }

// if (typeof window !== 'undefined' && window.Vue) {
//   install(window.Vue)
// }

export const vuex = {
  helpers: vuexHelpers,
  store: vuexStore,
}

export const api = {
  client: apiClient,
}

export const graphql = graphqlImport

export const components = componentsList
