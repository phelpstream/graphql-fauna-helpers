import Debug from 'debug'
const debug = Debug('gfh-client [api:utils]')

import { format } from 'graphql-formatter'
import { get, getOr, dig, has, clone } from 'svpr'
import { is, mergeDeepWith, isNil } from 'ramda'

import compress from 'graphql-query-compress'

const graphqlFormat = format

export const traverse = function(o, fn, additionalTest = x => true) {
  for (let i in o) {
    fn.apply(this, [i, o])
    if (o[i] !== null && typeof o[i] === 'object' && additionalTest(o[i])) {
      traverse(o[i], fn)
    }
  }
}

// const buildDefaultValue = typeObj => {
//   return traverse(
//     clone(typeObj.getFields()),
//     ([name, fields]) => {
//       fields[name] = fields[name].defaultValue;
//     },
//     item => (item.getFields ? true : false)
//   );
// };

export const getQueryBase = (schema, introspection) => {
  let base = getOr({}, 'data.__schema', schema) || getOr({}, '__schema', schema)
  return get('queryType', base)
}

export const getMutationBase = (schema, introspection) => {
  let base = getOr({}, 'data.__schema', schema) || getOr({}, '__schema', schema)
  return get('mutationType', base)
}

export const getTypeFrom = (path, types) => {
  let simplifiedTypes = Object.values(types).reduce((obj, type) => {
    obj[type.name] = {}
    if (type.fields && is(Array, type.fields)) {
      type.fields.map(field => {
        obj[type.name][field.name] =
          get('type.kind', field) === 'OBJECT'
            ? get('type.name', field)
            : get('type.ofType.kind', field) === 'OBJECT'
            ? get('type.ofType.name', field)
            : get('type.ofType.ofType.kind', field) === 'OBJECT'
            ? get('type.ofType.ofType.name', field)
            : null
      })
    }
    return obj
  }, {})

  // debug("simplifiedTypes", simplifiedTypes);

  const getObjDefinition = function(index, obj) {
    if (is(String, obj[index])) {
      let def = simplifiedTypes[obj[index]]
      if (def && is(Object, def) && Object.keys(def).length > 0) {
        obj[index] = def
      } else if (is(String, def)) {
        obj[index] = def
      } else {
        obj[index] = null
      }
    }
  }

  const objGraqhQL = obj =>
    Object.entries({ root: obj }).reduce((str, [index, item]) => {
      if (is(Object, item)) {
        let keys = Object.keys(item)
        str = `${keys
          .map(k => {
            if (is(Object, item[k])) {
              return `${k}{\n${objGraqhQL(item[k])}\n}`
            }
            return k
          })
          .join('\n')}`
      }
      return str
    }, '')

  let result = {
    [path.split('.')[path.split('.').length - 1]]: get(path, simplifiedTypes),
  }
  traverse(result, getObjDefinition)
  return format(`{${objGraqhQL(result)}}`)
}

export const readDefaultValuesMap = (type, defaultValuesMap) => {
  let base = getOr({}, type, defaultValuesMap)
  return dig(value => {
    if (is(Object, value) && has('$RECURSIVE_TYPE', value)) {
      let def = get('$RECURSIVE_TYPE', value)
      let next = clone(get(def.type, defaultValuesMap))
      return def.array ? [next] : next
    }
    return value
  }, base)
}

export const completeWithDefaultValue = (defaultValue, value) => {
  let thisValue = is(Object, value) ? clone(value) : {}
  return mergeDeepWith((two, one) => (isNil(one) ? two : one))(
    clone(defaultValue),
    thisValue
  )
}

export const readDefinitions = (type, definitions) => {
  debug('readDefinitions', type, definitions)
  let base = getOr('', type, definitions)
  let fragmentsObj = {}

  const hasMissingTags = () =>
    (base.match(new RegExp('##', 'g')) || []).length > 0
  const hasFragmentMissingTags = fragmentType =>
    (fragmentsObj[fragmentType].match(new RegExp('##', 'g')) || []).length > 0
  const needFragmentType = (text, defType) =>
    (text.match(new RegExp(`##${defType}##`, 'g')) || []).length > 0

  const replaceAsFragments = text => {
    for (let [defType, definition] of Object.entries(definitions)) {
      let fragmentName = `${defType}Fragment`
      if (needFragmentType(text, defType)) {
        //defType !== type &&
        addFragment(defType, definition)
        text = text.replace(
          new RegExp(`##${defType}##`, 'g'),
          `...${fragmentName}`
        )
      }
    }
    return text
  }

  const addFragment = (fragmentType, definition) => {
    if (!fragmentsObj[fragmentType]) {
      let fragCounter = 0
      fragmentsObj[fragmentType] = definition
      while (hasFragmentMissingTags(fragmentType) && fragCounter < 30) {
        fragmentsObj[fragmentType] = replaceAsFragments(
          fragmentsObj[fragmentType]
        )
        fragCounter++
      }
    }
  }

  let counter = 0
  while (hasMissingTags() && counter < 30) {
    base = replaceAsFragments(base)
    counter++
  }

  let fragments = Object.entries(fragmentsObj).reduce(
    (str, [fragType, fragDef]) => {
      str = `fragment ${fragType}Fragment on ${fragType} {${fragDef}} ${str} `
      return str
    },
    ''
  )

  // compressing
  let compressed_base = compress(base)
  let compressed_fragments = compress(fragments)
  // let compressed_base = base.replace(/\r?\n|\r/g, ' ').replace(/\s{2,}/gim, ' ')

  // console.log('not compressed', base.length, 'then', compressed_base.length)
  // console.log(
  //   'not compressed frags',
  //   fragments.length,
  //   'then',
  //   compressed_fragments.length
  // )

  debug('query', `${compressed_fragments} ${compressed_base}`)

  return { definition: compressed_base, fragments: compressed_fragments }
  // return `${compressed_fragments} ${compressed_base}`
}
