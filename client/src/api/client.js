import Debug from 'debug'
const debug = Debug('gfh-client [api:client]')

import fetch from 'cross-fetch'
// import 'isomorphic-fetch'
// import 'fetch-everywhere'
// import nodeFetch from "node-fetch"
// import unfetch from 'unfetch'
// import 'unfetch/polyfill'
import { is } from 'ramda'
import { get, getOr } from 'svpr'
// import { makeRemoteExecutableSchema, introspectSchema } from 'graphql-tools'
import { ApolloLink, concat } from 'apollo-link'
import { createHttpLink } from 'apollo-link-http'
import ApolloClient from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
// import { onError } from 'apollo-link-error'
// import { setContext } from 'apollo-link-context'
// import { ApolloLink } from 'apollo-link'
import gql from 'graphql-tag'
// import compress from 'graphql-query-compress'
// import { parse } from 'graphql'
// import IQ from 'graphql/utilities/introspectionQuery'

// const compactSXDLQuery = queryString => queryString.replace(/\s*/gm, '')

export const apolloClient = (
  endpointUrl,
  { authorization, headers = {} } = {}
) => {
  let headersParam = headers
  const httpLink = createHttpLink({
    uri: endpointUrl,
    fetch: fetch,
    headers: {
      ...headers,
      ...headersParam,
      authorization: authorization
        ? authorization.startsWith('Bearer')
          ? authorization
          : `Bearer ${authorization}`
        : undefined,
    },
  })

  // const compressMiddleware = new ApolloLink((operation, forward) => {
  //   debug("operation", operation);
  //   return forward(operation)
  // })

  const client = new ApolloClient({
    // link: concat(compressMiddleware, httpLink),
    link: httpLink,
    // queryTransformer: ApolloClient.addTypename,
    cache: new InMemoryCache({
      dataIdFromObject: object => null,
    }),
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'ignore',
      },
      query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
      },
      mutate: {
        errorPolicy: 'all',
      },
    },
  })

  return client
}

const formatDefinition = (defString = '') => {
  let definition = defString
  if (is(Object, defString)) {
    if (defString.definition) {
      definition = defString.definition
    } else {
      definition = getOr('', 'loc.source.body', defString)
    }
  }

  // debug('definition', definition)
  let compacted = definition.replace(/\s*/gim, '')
  // debug('compacted', compacted)
  if (!compacted.startsWith('{') && compacted.length > 0) {
    definition = `{${definition}}`
  }
  // debug('final definition', definition)
  // if (!compacted.endsWith('}')) {
  //   debug("!compacted.endsWith('}')", compacted)
  //   definition = `${definition}}`
  // }
  return definition
}

const formatFragments = (definitionObj = {}) => {
  if (is(Object, definitionObj) && definitionObj.fragments) {
    return definitionObj.fragments
  }
  return ''
}

export const queryBuilder = (queryName, { args = {} } = {}) => client => (
  variables = {},
  definition
) => {
  variables = variables || {}
  let params = Object.keys(variables).reduce((list, param) => {
    list.push(`$${param}: ${getOr('String', param, args)}`)
    return list
  }, [])
  let hasParams = params.length > 0
  let paramsStr = hasParams ? params.join(' ') : ''
  let varsStr = hasParams
    ? Object.keys(variables)
        .map(v => `${v}: $${v}`)
        .join(' ')
    : ''
  let queryStr = ''
  let definitionStr = definition ? formatDefinition(definition) : '' //`{${definition}}` : ''
  let fragmentsStr = formatFragments(definition)
  if (hasParams) {
    queryStr = `
    query ${queryName}(${paramsStr}) {
      ${queryName}(${varsStr})
        ${definitionStr}
    }

    ${fragmentsStr}
    `
  } else {
    queryStr = `
    {
      ${queryName}
        ${definitionStr}
    }

    ${fragmentsStr}
    `
  }
  let query = {
    query: gql`
      ${queryStr}
    `,
    variables
  }

  debug('query', query)
  return client
    .query(query)
    .then(res => {
      debug('query res', res)
      return res
    })
    .then(({ data, errors }) => ({
      data: get(`${queryName}`, data),
      errors,
    }))
}

export const mutationBuilder = (mutationName, { args = {} } = {}) => client => (
  variables = {},
  definition
) => {
  let params = Object.keys(variables).reduce((list, param) => {
    list.push(`$${param}: ${getOr('String', param, args)}`)
    return list
  }, [])
  let hasParams = params.length > 0
  let paramsStr = hasParams ? params.join(' ') : ''
  let varsStr = hasParams
    ? Object.keys(variables)
        .map(v => `${v}: $${v}`)
        .join(' ')
    : ''
  let mutationStr = ''
  let definitionStr = definition ? formatDefinition(definition) : '' //`{${definition}}` : ''
  let fragmentsStr = formatFragments(definition)
  if (hasParams) {
    mutationStr = `
   mutation ${mutationName}(${paramsStr}) {
     ${mutationName}(${varsStr})
       ${definitionStr}
   }

   ${fragmentsStr}
   `
  } else {
    mutationStr = `
  mutation ${mutationName} {
     ${mutationName}
      ${definitionStr}
     
   }

   ${fragmentsStr}
   `
  }
  let mutation = {
    mutation: gql`
      ${mutationStr}
    `,
    variables,
  }
  debug("mutation", mutation)
  return client
    .mutate(mutation)
    .then(res => {
      // debug('mutation res', res, Object.keys(res.data))
      debug('mutation res', res)
      return res
    })
    .then(({ data, errors }) => ({
      data: get(`${mutationName}`, data),
      errors,
    }))
}
