import Debug from 'debug'
const debug = Debug('gfh-client [vuex:helpers]')

import { head, is, omit } from 'ramda'
import { get, getOr, dig, omitList, clone } from 'svpr'

// const error = require("./common/error";
// const result = require("./common/result";
// const forward = require("./common/forward";
// const { startWait, endWait, startWaitID, endWaitID } = require("./common/wait";

export const dataFilter = function({ filters, data } = {}) {
  // console.time('dataFilter')
  // debug("filters", filters);
  filters = clone(filters)
  let output = clone(data)
  if (data && filters && is(Array, filters) && filters.length > 0) {
    output = omitList(filters, output)
    // debug("data", data, "filtered", output);
    // debug("size difference", JSON.stringify(data).length - JSON.stringify(output).length);
  }
  // console.timeEnd('dataFilter')
  return output
}

export const argumentsFilter = function(
  args,
  argumentsSignature = {},
  typeFilters
) {
  // console.time('filter_arguments')
  let filteredArgs = {}
  debug('args', args)

  try {
    filteredArgs = Object.entries(args).reduce((obj, [key, value]) => {
      let type = argumentsSignature[key]
      let hasValueToFilter = !!value
      debug('hasValueToFilter', hasValueToFilter, 'type', type)
      if (type && hasValueToFilter) {
        let cleanType = type.replace(/\W/gim, '').replace('Input', '')
        let filters = getOr([], cleanType, typeFilters)
        debug('filters', filters)
        // let isArray = type.includes("[")
        let isReallyArray = is(Array, value)
        // debug("cleanType", cleanType, "filters", filters, "isReallyArray", isReallyArray);
        if (filters && filters.length > 0) {
          if (isReallyArray) {
            obj[key] = value.map(omitList(filters))
          } else {
            obj[key] = omitList(filters, clone(value))
          }
        }
      }
      return obj
    }, clone(args))

    debug('Filtered args', filteredArgs)
  } catch (error) {
    debug('Error on filteredArgs', error)
  }

  // console.timeEnd('filter_arguments')
  // const argsLength = JSON.stringify(args).length;
  // const filteredArgsLength = JSON.stringify(filteredArgs).length;

  // debug("Filtered args from", argsLength, "to", filteredArgsLength);

  return filteredArgs
}

export const responseCleaner = function(response) {
  return dig(
    value => {
      if (is(Array, value)) return value
      if (is(Object, value)) return omit(['__typename'], value)
      return value
    },
    { response }
  ).response
}

// export const objBuilder = function(methods, buildFn) {
//   return Object.keys(methods).reduce((fnObj, methodName) => {
//     let stateName = head(methodName.split(/(?=[A-Z])/));
//     fnObj[methodName] = buildFn(methodName, { isList: stateName.endsWith("s") });
//     return fnObj;
//   }, {});
// };

// export const actionBuilder = methodName => {
//   return {
//     [methodName]: function({ commit, dispatch }, args = {}) {
//       let key = args.path ? args[args.path] : undefined;
//       startWait(dispatch)(methodName);
//       return this.$gfh.endpoint[methodName](args)
//         .then(forward(value => commit(methodName, { key, value })))
//         .then(forward(() => endWait(dispatch)(methodName)))
//         .catch(error(methodName, () => endWait(dispatch)(methodName)));
//     }
//   };
// };

// export const actions = methods => {
//   return objBuilder(methods, actionBuilder);
// };

// export const getterBuilder = (methodName, { isList = false } = {}) => {
//   return {
//     [methodName]: state => {
//       return isList ? key => get(state[methodName], key) : state[methodName];
//     }
//   };
// };

// export const getters = methods => {
//   return objBuilder(methods, getterBuilder);
// };

// export const mutationBuilder = methodName => {
//   return {
//     [methodName]: (state, { key, value } = {}) => {
//       if (key) {
//         if (!state[methodName]) state[methodName] = {};
//         Vue.set(state[methodName], token, value);
//       } else {
//         state[methodName] = value;
//       }
//     }
//   };
// };

// export const mutations = methods => {
//   return objBuilder(methods, mutationBuilder);
// };

// export const states = methods => {
//   return Object.keys(methods).reduce((stateObj, methodName) => {
//     let stateName = head(methodName.split(/(?=[A-Z])/));
//     stateObj[stateName] = {};
//     return stateObj;
//   }, {});
// };
