import Debug from 'debug'
const debug = Debug('gfh-client [vuex:store]')
// import { getField, updateField } from 'vuex-map-fields';
import { GETTERS, ACTIONS, MUTATIONS } from 'vue-data-utils'
// import { VDU_ITEM_GETTER, VDU_ITEM_PATCHER } from 'vue-data-utils'

// debug fns
const debugAction = (actionName, ...contents) =>
  debug(`[action:${actionName}]`, ...contents)
const debugMutation = (actionName, ...contents) =>
  debug(`[mutation:${actionName}]`, ...contents)

import Vue from 'vue'
import jwt from 'jsonwebtoken'

import memoize from 'memoizee'

import { head, is } from 'ramda'
import { get, getOr, hasAll, has, set, clone } from 'svpr'

import jsonpatch from 'fast-json-patch'

import {
  readDefaultValuesMap,
  readDefinitions,
  completeWithDefaultValue,
} from './../api/utils'

import debounce from 'debounce-promise'
// import debounce from 'lodash.debounce'

// const { states, mutations, getters, actions } from ("./helpers");
import { startWaitID, endWaitID } from './common/wait'
import forward from './common/forward'
import error from './common/error'

import { responseCleaner, argumentsFilter, dataFilter } from './helpers'

export const initState = () => ({
  // ...statesObj,
  _pageRefresh: false,
  _token: undefined,
  _auth0Token: undefined,
  _loggedIn: false,
  _isLoaded: false,
  _methodTypes: {},
  _defaultValuesMap: {},
  _defaultValuesMapCache: {},
  _defaultValues: {},
  _definitions: {},
  _inputTypeFilters: {},
  // _typedMethods: {},
  _methodsArgumentTypes: {},
  _methodsNames: {},
  _methodsCollections: {},
  _resources: {},
  _endpoint: {
    url: '',
    authorization: '',
    headers: [],
  },
  _lastApiRefresh: null,
  _refreshRate: 20 * 60 * 1000,
  _version: null,
  _notifications: [],
})

const parseValues = (
  value,
  { asValues = false, asKeys = false, mapPath } = {}
) => {
  let result = asValues
    ? Object.values(value)
    : asKeys
    ? Object.keys(value)
    : value
  if (mapPath) return result.map(get(mapPath))
  return result
}

const getUserIdFromToken = token => {
  try {
    if (token) {
      let user = jwt.decode(token)
      return get('user.id', user)
    }
  } catch (error) {}
}

const isTokenValid = token => {
  try {
    if (token) {
      let expDate = get('exp', jwt.decode(token) || {})
      if (expDate && is(Number, +expDate)) {
        return expDate >= new Date().getTime() / 1000
      }
      return false
    }
  } catch (error) {}
}

const getters = {
  // ...getters(methods),
  // getField,
  ...GETTERS,
  auth0FirstLogin: ({ _auth0Token }) => {
    if (_auth0Token) {
      try {
        var user = jwt.decode(_auth0Token)
        return user['https://alloclaim.com/logins_count'] < 2
      } catch (error) {}
    }
  },
  myself: ({ _token, users }) => {
    if (is(Object, users) && _token) {
      let userId = getUserIdFromToken(_token)
      if (userId) {
        return get(userId, users)
      }
    }
  },
  myRoles: ({ _token, users }) => {
    if (is(Object, users) && _token) {
      let userId = getUserIdFromToken(_token)
      if (userId) {
        return get(`${userId}.rights.roles_codes`, users).map(rc => rc.data)
      }
    }
  },
  hasRole: ({ _token, users }) => roleCode => {
    if (is(Object, users) && _token && roleCode) {
      let userId = getUserIdFromToken(_token)
      if (userId) {
        return !!(getOr([], `${userId}.rights.roles_codes`, users) || []).find(
          rc => rc.data === roleCode
        )
      }
    }
  },
  pageRefresh: ({ _pageRefresh }) => _pageRefresh,
  notifications: ({ _notifications }) => _notifications,
  state: state => path => get(path, state),
  api: state => (path, options = {}) => {
    return parseValues(getOr(options.defaultValue, path, state), options)
  },
  cache: state => reference =>
    state.cache ? state.cache[reference] : undefined,
  _userId: ({ _token }) => getUserIdFromToken(_token),
  _token: ({ _token }) => _token,
  _isLoggedIn: ({ _auth0Token }) => isTokenValid(_auth0Token),
  _endpoint: ({ _endpoint }) => _endpoint,
  everything: state => state,
  _methodsArgumentTypes: ({ _methodsArgumentTypes }) => _methodsArgumentTypes,
  _methodsNames: ({ _methodsNames }) => _methodsNames,
  _methodsName: ({ _methodsNames }) => name => getOr({}, name, _methodsNames),
  _methodTypes: ({ _methodTypes }) => _methodTypes,
  _inputTypeFilter: ({ _inputTypeFilters }) => type =>
    getOr([], type, _inputTypeFilters),
  _inputTypeFilters: ({ _inputTypeFilters }) => _inputTypeFilters,
  _methodCollection: ({ _methodsCollections }) => name =>
    getOr({}, `${name}.0`, _methodsCollections),
  _methodCollections: ({ _methodsCollections }) => name =>
    getOr([], `${name}`, _methodsCollections),
  _methodsCollections: ({ _methodsCollections }) => _methodsCollections,
  _definitions: ({ _definitions }) => _definitions,
  _definition: ({ _definitions }) =>
    memoize(
      type => {
        return clone(readDefinitions(type, _definitions))
      },
      { length: 1 }
    ),
  _defaultValue: ({ _defaultValues, _defaultValuesMap }) =>
    memoize(
      (type, { useMap = true } = {}) => {
        if (useMap) {
          return clone(readDefaultValuesMap(type, _defaultValuesMap))
        } else {
          return clone(getOr({}, type, _defaultValues))
        }
      },
      { length: 2 }
    ),
  _defaultValuesMap: ({ _defaultValuesMap }) => _defaultValuesMap,
  _defaultValues: ({ _defaultValues }) => _defaultValues,
  _resource: ({ _resources }) => (type, options) =>
    parseValues(_resources[type], options),
  _resources: ({ _resources }) => _resources,
  _refreshRate: ({ _refreshRate }) => _refreshRate,
  _lastApiRefresh: ({ _lastApiRefresh }) => _lastApiRefresh,
  _isReady: state => {
    const check = obj => obj && Object.keys(obj).length > 0
    return [
      state._methodsArgumentTypes,
      state._methodsNames,
      state._methodTypes,
      state._methodsCollections,
      state._definitions,
      state._defaultValuesMap,
      state._resources,
    ].every(v => check(v))
  },
  _isLoaded: ({ _isLoaded }) => _isLoaded,
  _version: ({ _version }) => _version,
}

const actions = {
  ...ACTIONS,
  auth0LogIn: function({ commit, dispatch }, _auth0Token) {
    commit('SET_AUTH0_TOKEN', _auth0Token)
    return dispatch('init_auth', _auth0Token)
  },
  auth0LogOut: function({ commit, dispatch }) {
    commit('SET_AUTH0_TOKEN', null)
    return dispatch('init_auth')
      .then(get('data'))
      .then(() => commit('SET_INITIATE'))
  },
  stateSync({ commit }, { path = '', value, domain, id }) {
    if (!path) path = ''
    debug('stateSync', 'path', path, 'value', value, 'domain', domain, 'id', id)
    let keyArr = path.split('.')
    let pathRoot = keyArr.slice(0, -1).join('.')
    pathRoot = pathRoot.replace('$store.state.gfh.', '')
    let finalKey = keyArr[keyArr.length - 1]
    // debug('stateSync', 'path', pathRoot, 'key', finalKey, 'value', value)
    commit('SET_STATE', { path: pathRoot, key: finalKey, value, domain, id })
    // commit('SET_STATE', { value, domain, id })
  },
  state({ commit }, { path, key, value }) {
    commit('SET_STATE', { path, key, value })
  },
  deleteState({ commit }, { path, key }) {
    commit('DELETE_STATE', { path, key })
  },
  api: async function(context, params = {}) {
    let methodName,
      args,
      definition,
      collection,
      loadOnce,
      loadCondition,
      no_loading,
      context_data,
      abstract,
      reflectInStore,
      dataPatch,
      stateData

    if (is(Array, params)) {
      methodName = get(0, params)
      args = get(1, params)
      definition = get(2, params)
      collection = get(3, params)
    } else if (is(Object, params)) {
      methodName = params.methodName
      args = params.args
      definition = params.definition
      collection = params.collection
      loadOnce = params.loadOnce
      loadCondition = params.loadCondition
      no_loading = params.no_loading
      context_data = params.context_data
      abstract = params.abstract
      reflectInStore = params.reflectInStore || true
      dataPatch = params.dataPatch
      stateData = params.stateData || {}
    }

    if (loadOnce) {
      let v = getOr({}, `state.${collection.name}`, context)
      debugAction(
        'api',
        'methodName',
        methodName,
        'loadOnce',
        loadOnce,
        'value',
        v
      )
      if (is(Function, loadCondition) && !loadCondition(context.state)) {
        debugAction(
          'api',
          'methodName',
          methodName,
          'ALREADY LOADED (loadCondition)'
        )
        return
      } else {
        if (Object.keys(v).length > 0) {
          debugAction(
            'api',
            'methodName',
            methodName,
            'ALREADY LOADED (object.keys)'
          )
          return
        }
      }
    }

    let { commit, dispatch, getters } = context
    let methodsArgumentTypes = getters._methodsArgumentTypes
    let inputTypeFilters = getters._inputTypeFilters
    let getDefinition = getters._definition
    let methodTypes = getters._methodTypes
    // debug('methodTypes', methodTypes)
    debugAction('api', 'methodName', methodName)
    let methodType = get(methodName, methodTypes) || ''
    debugAction('api', 'methodType', methodType)
    definition = definition || getDefinition(methodType)
    // // TODO: WEIRD
    // if (is(Object, definition) && definition.definition) {
    //   definition = definition.definition
    // }
    // debug("definition", getDefinition(methodType));
    // debug("fields", fields);
    // debug("methods", methods);
    let isQuery = get(`queries.${methodName}`, methodsArgumentTypes)
    let isMutation = get(`mutations.${methodName}`, methodsArgumentTypes)
    // debug("isQuery", isQuery);
    // debug("isMutation", isMutation);
    let methodArgs = isQuery || isMutation
    let methodBuilder = isQuery
      ? this.$gfh.client.queryBuilder
      : isMutation
      ? this.$gfh.client.mutationBuilder
      : undefined
    let client = await dispatch('_client')

    // let methodsCollections = getters._methodsCollections
    let methodCollection = getters._methodCollection
    // if (Object.keys(methodsCollections).length < 1) {
    //   console.error('Has no methods collections loaded yet!')
    // }
    collection = collection || clone(methodCollection(methodName))
    debugAction('api', 'collection', collection)
    let eventId = get('id', args) || get('ref_id', args)
    if (!no_loading) {
      startWaitID(dispatch)(methodName, eventId)
    }

    // debugAction('api', 'isQuery || isMutation', isQuery || isMutation)
    // debugAction('api', 'methodBuilder', methodBuilder)
    // debug("client", client);

    if ((isQuery || isMutation) && is(Function, methodBuilder)) {
      if (is(Object, args)) {
        args = argumentsFilter(
          clone(args),
          clone(isQuery || isMutation),
          clone(inputTypeFilters)
        )
        // debug("args", args);
      }

      if (!!dataPatch && stateData) {
        // let patchedData = jsonpatch.applyPatch(clone(stateData), dataPatch)
        //   .newDocument
        // commit('SET_API', {
        //   methodName,
        //   value: patchedData,
        //   collection,
        //   abstract,
        // })

        commit('SET_API', {
          methodName,
          value: stateData,
          collection,
          abstract,
        })
      }

      return methodBuilder(methodName, { args: methodArgs })(client)(
        args,
        definition
      )
        .then(forward(res => debug('raw res', methodName, methodArgs, res)))
        .then(({ data, errors }) => {
          if (errors && errors.length > 0) {
            dispatch('sendNotification', {
              content: errors,
              type: 'error',
            })
          }
          return data
        })
        .then(responseCleaner)
        .then(
          forward(v =>
            debugAction(
              'api',
              'COMMIT INIT value',
              v,
              'methodName',
              methodName,
              'collection',
              collection
            )
          )
        )
        .then(value => {
          let only_value = has('data', value) ? value.data : value

          if (reflectInStore) {
            commit('SET_API', {
              methodName,
              value: only_value,
              collection,
              abstract,
            })
          }

          if (context_data) return val
          return only_value
        })
        .then(
          forward(() => {
            if (!no_loading) {
              endWaitID(dispatch)(methodName, eventId)
            }
          })
        )
        .catch(
          error(methodName, () => {
            if (!no_loading) {
              endWaitID(dispatch)(methodName, eventId)
            }
          })
        )
    } else {
      if (!no_loading) {
        endWaitID(dispatch)(methodName, eventId)
      }
      debugAction('api', `Method ${methodName} not found :x`)
      debugAction(
        'api',
        'methodBuilder',
        methodBuilder,
        'isMutation',
        isMutation,
        'isQuery',
        isQuery
      )
      return undefined
    }
  },
  myself: function({ dispatch, getters }) {
    if (getters._userId) {
      return dispatch('api', {
        methodName: 'userById',
        args: { id: getters._userId },
        collection: { name: 'users', key: 'id' },
      })
    }
  },
  cache: function({ commit }, { reference, data } = {}) {
    return commit('SET_CACHE', { reference, data })
  },
  initiate: async function({ dispatch, commit, getters }) {
    let client = await dispatch('_client')
    return this.$gfh.client
      .queryBuilder('initiate')(client)()
      .then(get('data'))
      .then(responseCleaner)
      .then(forward(value => commit('SET_INITIATE', value)))
  },
  refreshVersion: async function({ dispatch, getters, app }) {
    return dispatch('api', {
      methodName: 'version',
      collection: {
        name: '_version',
      },
    })
  },
  resetMyself({ commit, getters }) {
    commit('RESET_MYSELF', getters._userId)
  },
  _client: function({ getters }) {
    let _endpoint = getters._endpoint || {}
    return this.$gfh.client.apolloClient(_endpoint.url, {
      authorization: _endpoint.authorization,
      headers: _endpoint.headers,
    })
    // return this.$gfh.client.generateClient(_endpoint.url, _typedMethods, { authorization: _endpoint.authorization, headers: _endpoint.headers });
  },
  init: debounce(function({ getters, commit, dispatch }) {
    debug('[init]')
    // commit('SET_IS_LOADED', false)
    let _version = getters._version
    let version

    const initAll = () =>
      dispatch('initiate').then(() => {
        commit('SET_LAST_API_REFRESH', new Date().getTime())
        dispatch('init_auth')
        return true
      })

    // if (!getters._isLoaded) {
    if (!getters._isReady) {
      debug('NOT READY, FIRST FETCH')
      debug('initiate')
      return initAll().then(() => {
        debug('initiate')
        return true
      })
    } else {
      debug('READY, CHECKING VERSION')
      // NOTE: Need to be fully async (dont return it plz)
      return dispatch('refreshVersion').then(fetchedVersion => {
        debug('fetchedVersion', fetchedVersion)
        dispatch('sendNotification', {
          content: `Version ${get('ts', fetchedVersion)} is running.`,
          type: 'info',
        })
        version = fetchedVersion
        debug('version (new)', version, '_version (old)', _version)
        if (version && _version && version.ts > _version.ts) {
          debug('NEED UPDATE')
          commit('RESET_STATE', version)
          return initAll().then(() => {
            debug('initiate')
            commit('PAGE_REFRESH', true)
          })
        }
        return null
      })
    }
  }, 200),
  init_auth: debounce(function({ getters, commit, dispatch }, token) {
    debug('[init_auth]')
    debug('token', token)
    debug('getters._token', getters._token)
    debug('getters._endpoint.authorization', getters._endpoint.authorization)

    if (token) {
      if (!getters._endpoint.authorization || !getters._token) {
        return dispatch('api', {
          methodName: 'signFromAuth0',
          args: { token },
          collection: { name: '_token' },
        })
          .then(token => {
            debug('generated token', token)
            commit('SET_ENDPOINT_TOKEN', token)
            commit('SET_TOKEN', token)
            return dispatch('myself')
          })
          .catch(console.error)
      }
    } else if (!getters._isLoggedIn) {
      return dispatch('resetMyself').then(() => {
        commit('SET_ENDPOINT_TOKEN', null)
        commit('SET_TOKEN', null)
      })
    }
  }, 200),
  sendNotification({ commit }, notif) {
    commit('ADD_NOTIFICATION', notif)
  },
}

const mutations = {
  // updateField,
  ...MUTATIONS,
  PAGE_REFRESH: (state, pageRefresh) => {
    debug(`PAGE_REFRESH: ${pageRefresh}`)
    Vue.set(state, '_pageRefresh', pageRefresh)
  },
  SET_AUTH0_TOKEN: (state, _auth0Token) => {
    Vue.set(state, '_auth0Token', _auth0Token)
  },
  // ...mutations(methods),
  SET_INITIATE: (state, initiatedData = {}) => {
    Object.keys(state).forEach(key => {
      debug(`initiate [remove] key: ${key}`)
      if (!key.startsWith('_')) {
        Vue.delete(state, key)
      }
    })
    Object.entries(initiatedData).forEach(([key, value]) => {
      debug(`initiate [add] key: ${key} value`, value)
      Vue.set(state, key, value)
    })
  },
  SET_API: (state, { methodName, value, collection, abstract } = {}) => {
    let methodsCollections = state._methodsCollections
    // debug(
    //   'SET_API methodName, value, collection',
    //   methodName,
    //   value,
    //   collection
    // )

    collection = collection || clone(get(`${methodName}.0`, methodsCollections))

    if (abstract && collection.name) {
      collection.name = `${collection.name}Abstract`
      debug(
        'get(`${methodName}.0`, methodsCollections)',
        collection.name,
        get(`${methodName}.0`, methodsCollections)
      )
    }

    if (collection) {
      if (collection.no_state) return

      if (
        !is(Object, state[collection.name]) ||
        is(Array, state[collection.name])
      ) {
        Vue.set(state, collection.name, {})
      }

      // set value
      if (!is(Array, value)) value = [value]

      if (collection.method_name_as_key) {
        if (
          !is(Object, state[collection.name][methodName]) ||
          is(Array, state[collection.name][methodName])
        ) {
          Vue.set(state[collection.name], methodName, {})
        }

        value.map(v => {
          let uid = get(collection.key, v)
          if (uid) Vue.set(state[collection.name][methodName], uid, v)
        })
        // } else if (collection.not_a_list) {
      } else {
        if (collection.key) {
          value.map(v => {
            let uid = get(collection.key, v)
            // debug("set", collection.name, uid, v);
            if (uid) Vue.set(state[collection.name], uid, v)
          })
        } else {
          // debug("set ", collection.name, value[0], "from", value);
          Vue.set(state, collection.name, value[0])
        }
      }
    } else {
      debug('No way to save the state :x for ', methodName)
      Vue.set(state, methodName, value)
      // Vue.set(state, stateName, value);
    }
  },
  SET_STATE(state, { path, key, value, modifications, domain, id }) {
    debugMutation('SET_STATE', { path, key, value, modifications, domain, id })
    if (domain && id) {
      if (!state[domain]) Vue.set(state, domain, {})
      Vue.set(state[domain], id, value)
    } else if (path && (is(String, key) || is(Number, key)) && value) {
      // debug('SET_STATE, !get(path, state)', !get(path, state))
      // if (!get(path, state)) {
      //   state = set(path, {}, state)
      //   // Vue.set(state, path, {})
      // }
      // debug('SET_STATE, get(path, state)', get(path, state))
      // Vue.set(get(path, state), key, value)
    }
  },
  DELETE_STATE(state, { path, key }) {
    if (path && (is(String, key) || is(Number, key)) && has(path, state)) {
      Vue.delete(get(path, state), key)
    }
  },
  SET_CACHE: function(state, { reference, data } = {}) {
    debug('SET_CACHE', reference, data)
    if (!state.cache) state.cache = {}
    if (reference) {
      Vue.set(state.cache, reference, data)
    }
  },
  RESET_MYSELF: (state, userId) => {
    if (state.users && userId && state.users.userId) {
      Vue.delete(state.users, userId)
    }
  },
  SET_TOKEN: (state, token) => {
    Vue.set(state, '_token', token)
  },
  SET_ENDPOINT_URL: (state, url) => {
    Vue.set(state._endpoint, 'url', url)
  },
  SET_ENDPOINT_TOKEN: (state, token) => {
    Vue.set(
      state._endpoint,
      'authorization',
      !!token ? `Bearer ${token}` : undefined
    )
  },
  SET_ENDPOINT: (state, endpoint) => {
    Vue.set(state, '_endpoint', endpoint)
  },
  SET_LAST_API_REFRESH: (state, _lastApiRefresh) => {
    Vue.set(state, '_lastApiRefresh', _lastApiRefresh)
  },
  SET_IS_LOADED: (state, _isLoaded) => {
    Vue.set(state, '_isLoaded', _isLoaded)
  },
  RESET_STATE: (state, version) => {
    state = initState()
    state._version = version
  },
  ADD_NOTIFICATION: (state, notif) => {
    if (!state._notifications) state._notifications = []
    state._notifications = [notif]
  },
}

export const store = {
  namespaced: true,
  strict: true,
  state: initState,
  getters,
  actions,
  mutations,
}
