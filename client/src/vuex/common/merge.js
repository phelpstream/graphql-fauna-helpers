const { mergeDeepWith, isNil, is } = require('ramda')
const { clone } = require('svpr')
module.exports.mergePriorityToSecond = (defaultValue, value) => {
  let thisValue = is(Object, value) ? clone(value) : {}
  return mergeDeepWith((two, one) => (isNil(one) ? two : one))(
    clone(defaultValue),
    thisValue
  )
}
