import Debug from 'debug'
const debug = Debug('gfh-client [vuex:common:store]')

export const startWait = dispatch => name => {
  dispatch('wait/start', name, { root: true })
}
export const endWait = dispatch => name =>
  dispatch('wait/end', name, { root: true })

export const startWaitID = dispatch => (name, id = '') => {
  debug(`start:${name}/${id}`)
  startWait(dispatch)(name)
  startWait(dispatch)(`${name}/${id}`)
}
export const endWaitID = dispatch => (name, id = '') => {
  debug(`end:${name}/${id}`)
  endWait(dispatch)(name)
  endWait(dispatch)(`${name}/${id}`)
}
