const { get } = require("svpr");
export default  function(path) {
  return function(result) {
    return get(`data.${path}`, result);
  };
};
