export default  function(fnName, callback) {
  return function(err) {
    console.error(`Error in module on (${fnName}):`);
    console.log(err);
    callback();
  };
}
