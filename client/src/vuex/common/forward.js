const forward = actionFn => items => {
  actionFn(items);
  return items;
};

export default  forward;
