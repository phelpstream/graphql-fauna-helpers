const Vue = require("vue");
const { is } = require("ramda");

export default  function(arr = [], key = "id") {
  if (!is(Array, arr)) arr = [];
  return arr.reduce((obj, item) => {
    if (item[key]) Vue.set(obj, item[key], item);
    return obj;
  }, {});
};
