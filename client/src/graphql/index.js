import { get } from 'svpr'

export function toString(doc) {
  return get('loc.source.body', doc)
}
