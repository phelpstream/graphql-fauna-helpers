import Debug from 'debug'
const debug = Debug('gfh-client [plugin:index]')

import { pick, omit, is } from 'ramda'
import { get, getOr, each, clone } from 'svpr'
import VuexPersistence from 'vuex-persist'

// import gfh_client from 'gfh-client'
import Pusher from 'pusher-js'

import Vue from 'vue'
import * as gfh_client from 'gfh-client'
// import { default as install, api, vuex, graphql, components } from 'gfh-client'

console.log('gfh_client', gfh_client)
Vue.use(gfh_client.install)

const { api, vuex, graphql, components } = gfh_client
console.log('api, vuex, graphql, components', api, vuex, graphql, components)

export default async (context, inject) => {
  // const moduleOptions = '<%= options %>'
  const endpointUrl = '<%= options.endpointUrl %>'
  const PUSHER_API_KEY = '<%= options.options.PUSHER_API_KEY %>'
  const PUSHER_CLUSTER = '<%= options.options.PUSHER_CLUSTER %>'

  for (let [name, component] of Object.entries(components)) {
    Vue.component(name, component)
  }

  // if (process.client) {
  if (PUSHER_API_KEY) {
    inject('pusher', {
      c: new Pusher(PUSHER_API_KEY, {
        cluster: PUSHER_CLUSTER,
      }),
      available: true,
    })
  } else {
    inject('pusher', {
      available: false,
    })
  }

  // Persist the states
  new VuexPersistence({
    key: 'gfh_base',
    modules: ['gfh'],
    reducer: state => ({
      gfh: Object.entries(getOr({}, 'gfh', state)).reduce(
        (obj, [name, value]) => {
          if (name.startsWith('_')) obj[name] = value
          return obj
        },
        {}
      ),
    }),
  }).plugin(context.store)

  new VuexPersistence({
    key: 'gfh_data',
    modules: ['gfh'],
    reducer: state => ({
      gfh: Object.entries(getOr({}, 'gfh', state)).reduce(
        (obj, [name, value]) => {
          if (!name.startsWith('_')) obj[name] = value
          return obj
        },
        {}
      ),
    }),
  }).plugin(context.store)

  inject('gfh', {
    client: api.client,
    graphql: graphql,
    helpers: {
      filterData: vuex.helpers.dataFilter,
    },
  })

  let gfhInstalled = context.store._modules.get(['gfh'])

  if (!gfhInstalled) {
    debug('GFH is not installed yet!')
      try {
        const gfh_base = get(
          'gfh',
          JSON.parse(window.localStorage.getItem('gfh_base'))
        )
        const gfh_data = get(
          'gfh',
          JSON.parse(window.localStorage.getItem('gfh_data'))
        )
        if (gfh_base && gfh_data)
          vuex.store.store.state = () => Object.assign({}, gfh_base, gfh_data)
        console.log(' vuex.store.store.state', vuex.store.store.state())
      } catch (error) {
        // console.error(error);
      }
    context.store.registerModule('gfh', vuex.store.store, {
      preserveState: false,
    })
  }

  debug('NEW LOADING')

  context.store.commit('gfh/SET_ENDPOINT_URL', endpointUrl)
  await context.store.dispatch('gfh/init')

  debug('REFRESHING?!', context.store.getters['gfh/pageRefresh'])

  if (context.store.getters['gfh/pageRefresh']) {
    context.store.commit('gfh/PAGE_REFRESH', false)
    context.redirect(context.route.path)
  }

  debug('GFH LOADED YEAH')
}
