const { merge } = require('ramda')
const { clone } = require('svpr')
const { resolve } = require('path')

module.exports = function nuxtGFHVueModel(moduleOptions = {}) {
  let defaultOptions = {
    endpointUrl: null,
    options: {
      PUSHER_API_KEY: process.env.PUSHER_API_KEY,
      PUSHER_CLUSTER: process.env.PUSHER_CLUSTER,
    },
  }

  const options = merge(clone(defaultOptions), moduleOptions)

  this.nuxt.hook('build:before', async () => {
    // Register plugin
    this.addPlugin({
      src: resolve(__dirname, './plugin.js'),
      fileName: 'vue-gfh-client-plugin.js',
      ssr: false,
      options,
    })
  })
}

// required by nuxt
module.exports.meta = require('./../package.json')
