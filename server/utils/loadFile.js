const fs = require("fs");
const path = require("path");

module.exports = function(dirname, file, defaultValue) {
  let relativePath = path.resolve(dirname, file);
  if (fs.existsSync(relativePath)) {
    return require(relativePath);
  }
  return defaultValue;
};
