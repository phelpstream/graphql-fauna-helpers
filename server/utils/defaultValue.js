const { get, getOr } = require("svpr");

const defaultValue = (typeName, typeMap) => {
  return Object.entries(typeMap[typeName]._fields).reduce((def, [name, field]) => {
    let isArray = field.type && `${field.type}`.startsWith("[");
    let cleanType = `${field.type}`.replace(/\W/gim, "");
    // console.log("isArray", isArray, "cleanType", cleanType);
    let type = getOr({}, cleanType, typeMap);
    // console.log("cleanType", cleanType);

    if (isArray && !field.defaultValue) {
      def[name] = [];
    } else {
      if (type && type._fields) {
        def[name] = defaultValue(cleanType, typeMap);
      } else {
        def[name] = field.defaultValue ?  field.defaultValue : null;
      }
      if (isArray && field.defaultValue === []) {
        def[name] = [def[name]];
      }
    }
    return def;
  }, {});
};

module.exports = defaultValue;
