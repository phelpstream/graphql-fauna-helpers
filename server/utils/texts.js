const { upper } = require('svpr')
const { filter, pipe, isNil, not, join, replace, trim, is } = require('ramda')

const joinTexts = (arr, joiner = ' ') =>
  pipe(
    filter(
      pipe(
        isNil,
        not
      )
    ),
    join(joiner)
  )(arr)

// const capitalize = pipe(
//   split(/\W/, "gim"),
//   replace(/^./, upper),
//   join(" ")
// );

// const capitalize = replace(/^./, upper);

const capitalize = function(string) {
  return string.toLowerCase().replace(/(^|[^a-zA-Z\u00C0-\u017F'])([a-zA-Z\u00C0-\u017F])/g, function(m) {
    return m.toUpperCase()
  })
}

const mask = (value, template) => (is(String, value) && template.includes('#') ? template.replace('#', value) : undefined)

module.exports = {
  joinTexts: (arr, joiner) => {
    let res = joinTexts(arr, joiner)
    return trim(res).length > 0 ? res : undefined
  },
  capitalize: str => (is(String, str) ? capitalize(str) : str),
  trim: str => (is(String, str) ? trim(str) : str),
  mask
}
