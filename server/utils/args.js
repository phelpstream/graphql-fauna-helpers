const args = () => {
  return process.argv.slice(2) || [];
};

module.exports = args;
