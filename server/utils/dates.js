const moment = require('moment')
const momentTimezone = require('moment-timezone')

const formattedDate = (date, { format = 'L', locale = 'en' } = {}) => {
  if (date) {
    // console.log('date', date)
    let d
    try {
      d = moment(date)
      return d.isValid() ? d.locale(locale).format(format) : undefined
    } catch (error) {
      console.log('Error with DATE:', date)
    }
  }
  return undefined
}

module.exports = { formattedDate }
