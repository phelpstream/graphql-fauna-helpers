const forward = actionFn => items => {
  actionFn(items);
  return items;
};

module.exports = { forward };
