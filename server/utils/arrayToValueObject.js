module.exports = function(arr) {
  return arr.reduce((obj, item) => {
    obj[item] = { value: item };
    return obj;
  }, {});
};
