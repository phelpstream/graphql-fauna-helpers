module.exports = list => list.reduce((obj, i = {}) => Object.assign(obj, i), {});
