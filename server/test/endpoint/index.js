const gfh = require("./../../src/index");

const { server } = gfh.graphql.server;

const schema = gfh.graphql.type.Schema({
  queries: [
    {
      myself: gfh.back.queries.user.myself
    }
  ],
  mutations: [{}]
});

server(schema);
