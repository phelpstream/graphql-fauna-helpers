const { getOr, entries } = require("svpr");

const express = require("express");
const graphqlHTTP = require("express-graphql");
const cors = require("cors");

module.exports = function(schema, { secrets, context } = {}) {
  const app = express();

  app.use(cors());

  for (let [key, secret] of entries(secrets)) {
    process.env[key] = secret;
  }

  app.use(
    "/graphql",
    graphqlHTTP((req, res) => {
      return {
        schema,
        context,
        graphiql: true,
        formatError: error => {
          return {
            message: error.message,
            locations: error.locations,
            stack: error.stack ? error.stack.split("\n") : [],
            path: error.path
          };
        }
      };
    })
  );

  app.listen(9300, () => {
    console.log("Started on 9000 port");
  });
};
