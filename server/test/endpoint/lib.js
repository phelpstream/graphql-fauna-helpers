const { generateClient } = require("./../../graphql/client");

(async () => {
  let local = await generateClient({
    endpointUrl: "http://localhost:6000/graphql"
  });

  console.log("local", local);

  // let myself = await local.query.myself({}, `{ id profile { picture }}`);
  // console.log("myself", myself);
})();
