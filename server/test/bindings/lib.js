const gfh = require("./../../src/index");

const { generateClient } = gfh.graphql.client;

(async () => {
  let management = await generateClient({
    endpointUrl: "http://localhost:6000/graphql"
  });

  let claims = await management.query.claims();
  console.log("claims", claims);
})();
