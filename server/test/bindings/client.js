require("isomorphic-fetch");
const { importSchema } = require("graphql-import");
const { HttpLink, createHttpLink } = require("apollo-link-http");
const { makeExecutableSchema, makeRemoteExecutableSchema, introspectSchema } = require("graphql-tools");
const { typeFromAST, buildClientSchema, TypeInfo } = require("graphql/utilities");
const { printSchema } = require("graphql/utilities/schemaPrinter");
const { Binding } = require("graphql-binding");

const gfh = require("./../../src/index");

const binding = gfh.back.context.binding;
const graphqlUtils = gfh.graphql.utils;

(async () => {
  const endpoint = "https://r42ivaj4y7.execute-api.us-east-1.amazonaws.com/beta/api/management";
  const endpointLink = () =>
    createHttpLink({
      uri: endpoint,
      fetch
    });
  const schema = await introspectSchema(endpointLink());
  let remoteSchema = makeRemoteExecutableSchema({ link: endpointLink(), schema });
  let b = new Binding({ schema: remoteSchema });
  console.log("b", b);
})();
