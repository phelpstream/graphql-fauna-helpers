const fs = require("fs");
// const momentTimezone = require("moment-timezone");
// fs.writeFileSync('./timezones.json', JSON.stringify(momentTimezone.tz.names(), null, 2))

const timezones = require("./timezones.json");
fs.writeFileSync(
  "./timezones_full.json",
  JSON.stringify(
    timezones.map(tz => ({
      name: tz,
      code: tz.replace(/\W/gim, "_")
    })),
    null,
    2
  )
);
