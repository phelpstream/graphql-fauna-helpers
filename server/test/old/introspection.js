const { getIntrospectionSchema, getTypeFrom, getQueryBase, getMutationBase } = require("../../src/graphql/utils");
// const { getIntrospectionSchema } = require("../../src/graphql/schema");
const { get } = require("svpr");

const { typeFromAST, buildClientSchema, TypeInfo } = require("graphql/utilities");

getIntrospectionSchema("https://r42ivaj4y7.execute-api.us-east-1.amazonaws.com/beta/api/management").then(schema => {
  console.log("schema", schema);
  // console.log(get("ManagementQuery.claims", schema));
  // console.log(buildClientSchema(schema.data));
  let type = getTypeFrom("ManagementQuery.claims", schema.data);
  // console.log("type", type);
  // let type = typeFromAST(buildClientSchema(schema.data), "Action");
  // let type = new TypeInfo(buildClientSchema(schema.data))
  console.log("type", type);
});
