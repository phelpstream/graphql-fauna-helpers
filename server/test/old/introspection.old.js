const fs = require("fs");
const path = require("path");

require("isomorphic-fetch");
const ApolloClient = require("apollo-client");
const ApolloClientConstructor = ApolloClient.default || ApolloClient;
const { InMemoryCache } = require("apollo-cache-inmemory");
const { createHttpLink } = require("apollo-link-http");
const { onError } = require("apollo-link-error");
const { ApolloLink } = require("apollo-link");

const { parse } = require("graphql");
const introspectionQuery = require("graphql/utilities/introspectionQuery").introspectionQuery;

const GRAPHQL_URL = process.argv.slice(-1)[0];
const query = parse(introspectionQuery);

const httpLink = createHttpLink({
  uri: GRAPHQL_URL
});

const graphql = new ApolloClientConstructor({
  uri: GRAPHQL_URL,
  queryTransformer: ApolloClient.addTypename,
  link: ApolloLink.from([httpLink]),
  cache: new InMemoryCache({
    dataIdFromObject: object => null
  }),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: "no-cache",
      errorPolicy: "ignore"
    },
    query: {
      fetchPolicy: "no-cache",
      errorPolicy: "all"
    },
    mutate: {
      errorPolicy: "all"
    }
  }
});

graphql
  .query({ query })
  .then(result => {
    fs.writeFileSync(path.resolve(__dirname, "./schema.json"), JSON.stringify(result, null, 2));
  })
  .catch(err => console.error(err));
