const fs = require('fs')
const path = require('path')

const { format } = require('graphql-formatter')

const { parse, visit } = require('graphql/language')

const schema = require('./schema.json')
const types = schema.data.__schema.types

const { get, digStop, Stop, each, dig, clone } = require('svpr')
const { is } = require('ramda')

let simplifiedTypes = types.reduce((obj, type) => {
  obj[type.name] = {}
  if (type.fields && is(Array, type.fields)) {
    type.fields.map(field => {
      obj[type.name][field.name] =
        get('type.kind', field) === 'OBJECT'
          ? get('type.name', field)
          : get('type.ofType.kind', field) === 'OBJECT'
          ? get('type.ofType.name', field)
          : get('type.ofType.ofType.kind', field) === 'OBJECT'
          ? get('type.ofType.ofType.name', field)
          : null
    })
  }
  return obj
}, {})

// let completedTypes = digStop((value, prop) => {
//   if (is(String, value)) {
//     let valueObj = get(value, simplifiedTypes);
//     return valueObj && is(Object, valueObj) ? valueObj : null;
//   } else if (value && is(Object, value) && Object.keys(value).length > 0) {
//     return value;
//   } else if (!value) {
//     return Stop(null);
//   }
// }, simplifiedTypes);

var traverse = function(o, fn) {
  for (let i in o) {
    fn.apply(this, [i, o])
    if (o[i] !== null && typeof o[i] == 'object') {
      traverse(o[i], fn)
    }
  }
}

// const getDefinition = str => clone(get(str, simplifiedTypes));
const getObjDefinition = function(index, obj) {
  // console.log(index, obj);
  if (is(String, obj[index])) {
    let def = simplifiedTypes[obj[index]]
    // console.log("def", def);
    if (def && is(Object, def) && Object.keys(def).length > 0) {
      // console.log("object");
      obj[index] = def
    } else if (is(String, def)) {
      // console.log("string");
      obj[index] = def
    } else {
      // console.log("null");
      obj[index] = null
    }
  }
}

// let completedTypes = clone(simplifiedTypes);
let completedTypes = {
  claim: 'Claim'
}

traverse(completedTypes, getObjDefinition)

const objGraqhQL = obj =>
  Object.entries({ root: obj }).reduce((str, [index, item]) => {
    if (is(Object, item)) {
      let keys = Object.keys(item)
      str = `${keys
        .map(k => {
          if (is(Object, item[k])) {
            return `${k}{\n${objGraqhQL(item[k])}\n}`
          }
          return k
        })
        .join('\n')}`
    }
    return str
  }, '')

// getObjDefinition(obj);

// const getObjDefinition = obj =>
//   each((value, index) => {
//     if (is(String, value)) {
//       let valueObj = getDefinition(value);
//       return valueObj && is(Object, valueObj) ? getObjDefinition(valueObj) : null;
//     } else if (value && is(Object, value) && Object.keys(value).length > 0) {
//       return getObjDefinition(value);
//     }
//     return null;
//   }, obj);

// let completedTypes = getObjDefinition(simplifiedTypes);
// console.log(JSON.stringify(simplifiedTypes, null, 2));
console.log(format(`{${objGraqhQL(completedTypes)}}`))
// console.log(JSON.stringify(completedTypes, null, 2));
fs.writeFileSync(path.resolve(__dirname, './schema.graphql'), format(`{${objGraqhQL(completedTypes)}}`))
// fs.writeFileSync(path.resolve(__dirname, "./types_tess.JSON"), JSON.stringify(completedTypes, null, 2));
// fs.writeFileSync(path.resolve(__dirname, "./types.json"), JSON.stringify(completedTypes, null, 2));
