const grafhelpers = require("./../src/back");


(async () => {
  const { clientCreate, query, display, instanceHistory, instances, instanceCancelEvent, paginate, q, load } = grafhelpers;
  const gh = grafhelpers;
  const client = clientCreate(key);
  const clientQ = query(client);

  let result;

  // result = clientQ(instanceHistory("users", { id: "227907052030132742", paginateOptions: { desc: true, size: 1 } }));
  // result = clientQ(instances("users", { paginateOptions: { asc: true } }));

  // instanceCancelEvent
  // console.log("req", JSON.stringify(instanceCancelEvent("users", { id: "227907052030132742" }), null, 2));
  // result = clientQ(instanceCancelEvent("users", { id: "227907058699076102" }));
  // console.log(JSON.stringify(gh.instanceCreateEvent("users", { id: "227907058699076102" }), null, 2));

  // result = clientQ(gh.instanceCreateEvent("users", { id: "227907058699076102" }));
  result = clientQ(q.Select(["data", 0, "ts"], q.Paginate(q.Ref(q.Class("users"), "227907058699076102"), { events: true, size: 1 })));

  // result = clientQ(gh.instanceUpdate("users", { id: "227907058699076102", input: { ok: { deep: true } } }));
  // result = clientQ(gh.instanceUpdate("users", { id: "227907058699076102", input: { "ok": { deep: true } } }));
  display(result);
})();
