const Debug = require('debug')
const debug = Debug('gfh-server [algolia:builders:indices]')

const algoliasearch = require('algoliasearch')
const { getOr } = require('svpr')
const { pick } = require('ramda')

const indices = async (indicesDef = {}) => {
  const client = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_API_KEY)

  const getIndex = indexName => client.initIndex(indexName)

  const getSettings = (indexDef = {}) => pick(['searchableAttributes', 'attributesForFaceting', 'ranking'], getOr({}, 'settings', indexDef))
  const getReplicasNames = (indiceDef = {}) => Object.keys(getOr({}, 'replicas', indiceDef))
  const getReplicasEntries = (indiceDef = {}) => Object.entries(getOr({}, 'replicas', indiceDef))
  const setIndiceSettings = index => (settings, forwardToReplicas) =>
    index.setSettings(settings, {
      forwardToReplicas
    })

  const setIndiceReplicas = index => (replicas = []) =>
    index.setSettings({
      replicas
    })

  debug(indicesDef)

  await Promise.all(
    Object.entries(indicesDef).map(async ([indiceName, indiceDef]) => {
      debug(indiceName)
      const ind = getIndex(indiceName)
      const settings = getSettings(indiceDef)
      await setIndiceSettings(ind)(settings, true)
        .then(() => debug('setIndiceSettings', settings))
        .catch(debug)
      const replicasNames = getReplicasNames(indiceDef)
      await setIndiceReplicas(ind)(replicasNames)
        .then(() => debug('setIndiceReplicas', replicasNames))
        .catch(debug)
      await Promise.all(
        getReplicasEntries(indiceDef).map(async ([replicaName, replicaDef]) => {
          const replicaSettings = getSettings(replicaDef)
          await setIndiceSettings(getIndex(replicaName))(replicaSettings)
            .then(() => debug('setIndiceSettings', replicaSettings))
            .catch(debug)
        })
      )
    })
  )
}

// ;(async () => {
//   debug('coucou')
//   let def = {
//     claims_beta: {
//       settings: {
//         searchableAttributes: ['reference', 'members.owner_participation.full_name', 'members.participations.full_name', 'objectID'],
//         attributesForFaceting: ['claim.admin.status'],
//         ranking: ['desc(log.created_at_ts)']
//       },
//       replicas: {
//         claims_beta_created_at_desc: {
//           settings: {
//             ranking: ['desc(log.created_at_ts)']
//           }
//         },
//         claims_beta_created_at_asc: {
//           settings: {
//             ranking: ['asc(log.created_at_ts)']
//           }
//         },
//         claims_beta_updated_at_desc: {
//           settings: {
//             ranking: ['desc(log.updated_at_ts)']
//           }
//         },
//         claims_beta_updated_at_asc: {
//           settings: {
//             ranking: ['asc(log.updated_at_ts)']
//           }
//         }
//       }
//     }
//   }
//   await indices(def)
// })()

module.exports = indices
