const Debug = require('debug')
const debug = Debug('gfh-server [aws:events:index]')

const AWS = require('aws-sdk')

module.exports = ({ endpoint, region, GRAPHQL_API_TOPIC_ARN }) => {
  const sns = new AWS.SNS({
    endpoint,
    region
  })

  debug('endpoint', endpoint, 'region', region)

  const publish = ({ message, topicArn, messageStructure = 'json' }) => {
    // return new Promise((resolve, reject) => {
    let event = {
      Message:
        messageStructure === 'json' ? JSON.stringify({ default: `event:${message.method}`, lambda: JSON.stringify(message), http: JSON.stringify(message), https: JSON.stringify(message) }) : message,
      MessageStructure: messageStructure,
      TopicArn: topicArn
    }
    console.log('publish event', event)
    //   sns.publish(event, (error, response) => {
    //     if (error) reject(error)
    //     resolve(response)
    //   })
    // })

    return sns
      .publish(event)
      .promise()
      .then(response => {
        console.log('publish response', response)
      })
      .catch(err => {
        console.error('publish error', err)
      })
  }

  const triggerBinding = ({ query, mutation, method, args, definition, max_recursion = 10, recursion_count, topicArn }, ctx) => {
    return publish({
      message: {
        query,
        mutation,
        method,
        args,
        definition,
        max_recursion,
        recursion_count,
        authorization: ctx.storage.token ? `Bearer ${ctx.storage.token}` : undefined
      },
      topicArn: GRAPHQL_API_TOPIC_ARN || topicArn
    })
  }

  return { publish, triggerBinding }
}
