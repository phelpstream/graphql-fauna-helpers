const graphql = require('./graphql')
const fauna = require('./fauna')
const algolia = require('./algolia')
const back = require('./back')
const short = require('./short')

module.exports = {
  graphql,
  algolia,
  fauna,
  back,
  short
}
