// const graphql = require("./graphql");
const back = require("./back");

// console.log("sca", back.scalars);

module.exports = {
  d: back.domains,
  // t: graphql.type,
  s: back.scalars,
  e: back.entities,
  enu: back.enums,
  r: back.resources,
  mg: back.mutations.generators,
  qg: back.queries.generators,
  rg: back.resolvers.generators
};
