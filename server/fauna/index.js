module.exports = {
  builders: require('./builders'),
  methods: require("./methods"),
  queries: require("./queries")
};
