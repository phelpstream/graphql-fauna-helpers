module.exports = {
  fetchAll: require("./methods/fetchAll"),
  fetchAllBuilder: require("./methods/fetchAllBuilder"),
  indexCount: require("./methods/indexCount"),
  query: require("./methods/query"),
  clientCreate: require("./methods/clientCreate"),
  jwtGenerate: require("./methods/jwtGenerate"),
  referenceGetId: require("./methods/referenceGetId"),
  instancePatch: require("./methods/instancePatch"),
  query: require("./methods/query"),
  jwtRead: require("./methods/jwtRead"),
  uid: require("./methods/uid"),
  instanceUpdateListItem: require("./methods/instanceUpdateListItem"),
  addRefId: require("./methods/addRefId"),
  mapAddRefId: require("./methods/mapAddRefId"),
  addDataContext: require("./methods/addDataContext")
};
