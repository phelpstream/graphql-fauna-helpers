const { isNil } = require("ramda");
const faunadb = require("faunadb");
const q = faunadb.query;

function instancesOwnDef(className, { user_id, queryMatch } = {}) {
  let union = q.Union(q.Match(q.Index(`${className}_by_created_by`), user_id), q.Match(q.Index(`${className}_by_owned_by`), user_id));
  let intersection = q.Intersection(union, queryMatch);
  return queryMatch ? intersection : union;
}

module.exports = instancesOwnDef;
