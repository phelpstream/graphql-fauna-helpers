const faunadb = require("faunadb");

const instanceHistory = require("./instanceHistory");
const selectFirstData = require("./selectFirstData");

function instanceLastEvent(className, { id, ref_id, ref } = {}) {
  return selectFirstData(instanceHistory(className, { id, ref_id, ref, paginateOptions: { desc: true, size: 1 } }));
}

module.exports = instanceLastEvent;
