const faunadb = require('faunadb')
const q = faunadb.query

const indexMatch = require('./indexMatch')
const paginate = require('./paginate')
const map = require('./map')

function instances(className, { index, matchValue, paginateOptions, ref_only = false } = {}) {
  let queryDefinition = indexMatch(className, { index, matchValue })
  let paginated = paginate(queryDefinition, { className, ...paginateOptions })
  return ref_only ? paginated : map(paginated, row => q.Get(row))
}

module.exports = instances
