const paginate = require("./paginate");
const instanceRef = require("./instanceRef");

async function history(className, { ref_id, ref, paginateOptions = {} } = {}) {
  let buildRef = instanceRef(className, { ref_id, ref });
  return paginate(buildRef, { ...paginateOptions, events: true });
}

module.exports = history
