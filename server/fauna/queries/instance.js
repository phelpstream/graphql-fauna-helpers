const faunadb = require('faunadb')
const q = faunadb.query

// const instanceRef = require('./instanceRef')
const instanceRefAt = require('./instanceRefAt')

function instance(className, { ref_id, ref, at, ids = {} } = {}) {
  // let instanceBase = instanceRef(className, { ref_id, ref })
  // console.log('instance', { ref_id, ref, at, ids })
  return q.Get(instanceRefAt(className, { ref_id: ref_id || ids.ref_id, ref, at: at || ids.at }))
}

module.exports = instance
