const { over, lensPath } = require("ramda");
const instanceUpdate = require("./instanceUpdate");

function updatePathInstance(className, { path, input } = {}) {
  let newInput = over(lensPath(path.split(".")), x => input, {});
  return instanceUpdate(className, { ...args, input: newInput });
}

module.exports = updatePathInstance;
