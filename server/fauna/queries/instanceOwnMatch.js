const faunadb = require('faunadb')
const q = faunadb.query

const selectFirstData = require('./selectFirstData')
const instancesOwn = require('./instancesOwn')
const indexMatch = require('./indexMatch')

function instanceOwnMatch(className, { user_id, index, matchValue } = {}) {
  return q.Get(selectFirstData(instancesOwn(className, { user_id, queryMatch: indexMatch(className, { index, matchValue, ref_only: true }) })))
}

module.exports = instanceOwnMatch
