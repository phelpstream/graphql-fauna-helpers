const faunadb = require("faunadb");
const q = faunadb.query;

const paginate = require("./paginate");
const map = require("./map");
const indexMatch = require("./indexMatch");
const selectData = require("./selectData");

function instancesCreatedAt(className, { matchValue, reverse = false, paginateOptions } = {}) {
  let queryDefinition = indexMatch(className, { index: `${className}_by_created_at${reverse ? "__reverse" : ""}`, matchValue });
  return selectData(map(paginate(queryDefinition, paginateOptions), row => q.Get(row)));
}

module.exports = instancesCreatedAt;
