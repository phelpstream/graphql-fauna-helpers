const faunadb = require("faunadb");
const q = faunadb.query;

function keyCreate(databaseName, { role = "server" } = {}) {
  return q.CreateKey({
    database: q.Database(databaseName),
    role
  });
}

module.exports = keyCreate;
