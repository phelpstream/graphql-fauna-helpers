const selectFirstData = require('./selectFirstData')
const instances = require('./instances')

function instanceMatchRef(className, { index, matchValue, paginateOptions } = {}) {
  return selectFirstData(instances(className, { index, matchValue, paginateOptions, ref_only: true }))
}

module.exports = instanceMatchRef
