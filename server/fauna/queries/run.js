const faunadb = require("faunadb");
const q = faunadb.query;

function run(...expressions) {
  return q.Do(...expressions);
}

module.exports = run;
