const faunadb = require("faunadb");
const q = faunadb.query;

function map(query, actionFn) {
  return q.Map(query, actionFn);
}

module.exports = map;
