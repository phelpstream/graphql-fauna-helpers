const Debug = require('debug')
const debug = Debug('gfh-server [fauna:queries:instancesCreate]')

const faunadb = require('faunadb')
const q = faunadb.query

const uid = require('./../methods/uid')
const runParallel = require('./runParallel')

function instancesCreate(className, { input = [] }) {
  let ids = input.map(i => uid())
  let runs = input.map((i, index) => {
    debug('create', {
      data: { ...i, id: ids[index] }
    })

    return q.Create(q.Class(className), {
      data: { ...i, id: ids[index] }
    })
  })
  return runParallel(...runs)
}

module.exports = instancesCreate
