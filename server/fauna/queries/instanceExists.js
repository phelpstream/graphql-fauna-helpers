const faunadb = require("faunadb");
const q = faunadb.query;

const instanceRef = require("./instanceRef");

function instanceExists(className, { ref_id, ref } = {}) {
  return q.Exists(instanceRef(className, { ref_id, ref }));
}

module.exports = instanceExists;
