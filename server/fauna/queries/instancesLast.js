const faunadb = require("faunadb");
const q = faunadb.query;

const instances = require("./instances");

function instancesLast(className, { index, matchValue, size = 1 } = {}) {
  return instances(className, { index, matchValue, paginateOptions: { desc: true, size } });
}

module.exports = instancesLast;
