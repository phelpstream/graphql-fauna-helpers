const faunadb = require("faunadb");
const q = faunadb.query;

function refExists(ref) {
  return q.Exists(ref);
}

module.exports = refExists;
