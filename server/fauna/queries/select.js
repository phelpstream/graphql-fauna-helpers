const { curryN, is } = require("ramda");

const faunadb = require("faunadb");
const q = faunadb.query;

function select(path, definition) {
  if (!is(Array, path)) path = path.split(".");
  return q.Select(path, definition);
}

module.exports = curryN(2, select);
