const faunadb = require("faunadb");
const q = faunadb.query;

function login(id, password) {
  return q.Login(q.Ref(q.Class("users"), id), { password });
}

module.exports = login;
