const faunadb = require("faunadb");

const instanceRef = require("./instanceRef");
const paginate = require("./paginate");

function instanceHistory(className, { ref_id, ref, paginateOptions = {} } = {}) {
  let finalRef = instanceRef(className, { ref_id, ref });
  return paginate(finalRef, { ...paginateOptions, events: true });
}

module.exports = instanceHistory;
