const faunadb = require("faunadb");
const q = faunadb.query;

const instanceLastEvent = require("./instanceLastEvent");
const load = require("./load");
const remove = require("./remove");
const select = require("./select");

function instanceCancelEvent(className, { id, ref_id, ref } = {}) {
  return load(
    {
      lastEvent: instanceLastEvent(className, { id, ref_id, ref })
    },
    load(
      {
        ref: select("instance", q.Var("lastEvent")),
        ts: select("ts", q.Var("lastEvent")),
        //NOTE: Trick to get Action name (to be fixed by Fauna)
        action: q.If(q.Equals(select("action", q.Var("lastEvent")), "update"), "update", q.If(q.Equals(select("action", q.Var("lastEvent")), "create"), "create", "delete"))
      },
      remove(q.Var("ref"), q.Var("ts"), q.Var("action"))
    )
  );
}

module.exports = instanceCancelEvent;
