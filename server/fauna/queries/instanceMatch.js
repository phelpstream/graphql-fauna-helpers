const faunadb = require('faunadb')
const q = faunadb.query

const instanceMatchRef = require('./instanceMatchRef')

function instanceMatch(className, { index, matchValue, paginateOptions, at } = {}) {
  let instanceMatchBase = instanceMatchRef(className, { index, matchValue, paginateOptions })
  return at ? q.Get(q.At(at), instanceMatchBase) : q.Get(instanceMatchBase)
}

module.exports = instanceMatch
