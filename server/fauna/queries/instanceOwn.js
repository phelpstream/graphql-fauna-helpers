const { equals } = require("ramda");
const faunadb = require("faunadb");
const q = faunadb.query;

const selectFirstData = require("./selectFirstData");
const instancesOwn = require("./instancesOwn");
const instanceRef = require("./instanceRef");

function instanceOwn(className, { id, ref_id, ref, user_id } = {}) {
  let refQuery = instanceRef(className, { ref_id, ref });
  let instanceQuery = ref_id || ref ? refQuery : q.Match(q.Index(`${className}_by_id`), id);

  if (equals("users", className)) {
    return q.Get(q.Match(q.Index(`users_by_id`), user_id));
  } else {
    return q.Get(selectFirstData(instancesOwn(className, { user_id, queryMatch: instanceQuery, ref_only: true })))
  }
}

module.exports = instanceOwn;
