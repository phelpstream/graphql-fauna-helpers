const { isNil } = require('ramda')
const faunadb = require('faunadb')
const q = faunadb.query

const paginate = require('./paginate')

const instancesOwnDef = require('./instancesOwnDef')

function instancesDeleteAllOwn(className, { user_id, queryMatch, paginateOptions } = {}) {
  return q.Map(paginate(instancesOwnDef(className, { user_id, queryMatch }), { className, ...paginateOptions }), x => q.Delete(x))
}

module.exports = instancesDeleteAllOwn
