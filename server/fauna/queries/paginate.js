const { get } = require("svpr");

const faunadb = require("faunadb");
const q = faunadb.query;

const referenceGetClass = require("./../methods/referenceGetClass");

function paginate(ref, options = {}) {
  const { className, events, ts, after, afterCursor, before, beforeCursor, asc, desc, size } = options;
  let optionKeys = Object.keys(options);
  // console.log("ref", JSON.stringify(ref, null, 2));
  // console.log("q", JSON.stringify(q, null, 2));

  // let className = referenceGetClass(ref);
  let expression = { size };

  if (optionKeys.includes("events")) {
    expression.events = events;
  } else if (optionKeys.includes("ts")) {
    expression.ts = ts;
  }

  if (optionKeys.includes("after") && className) {
    expression.after = q.Ref(q.Class(className), after);
  } else if (optionKeys.includes("before") && className) {
    expression.before = q.Ref(q.Class(className), before);
  } else if (optionKeys.includes("afterCursor")) {
    expression.afterCursor = afterCursor;
  } else if (optionKeys.includes("beforeCursor")) {
    expression.beforeCursor = beforeCursor;
  }

  if (desc) {
    expression.before = null;
  } else if (asc) {
    expression.after = 0;
  }

  return q.Paginate(ref, expression);
}

module.exports = paginate;
