const faunadb = require('faunadb')
const q = faunadb.query

function instanceRefAt(className, { ref_id, ref, at } = {}) {
  // console.log(className, { ref_id, ref, at });
  let base = ref || q.Ref(q.Class(className), ref_id)
  return at ? q.At(at, base) : base
}

module.exports = instanceRefAt
