const faunadb = require("faunadb");
const q = faunadb.query;

function runParallel(...expressions) {
  const expName = index => `expression_n${index}`;
  let nextIndex = 0;
  let vars = expressions.reduce((letObj, exp) => {
    letObj[expName(nextIndex)] = exp;
    nextIndex++;
    return letObj;
  }, {});
  return q.Let(vars, Object.keys(vars).map(v => q.Var(v)));
}
module.exports = runParallel;
