const faunadb = require("faunadb");
const q = faunadb.query;

function identity() {
  return q.Identity();
}

module.exports = identity;
