const faunadb = require("faunadb");
const q = faunadb.query;

function logout() {
  return q.Logout();
}

module.exports = logout;
