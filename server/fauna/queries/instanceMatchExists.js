const faunadb = require('faunadb')
const q = faunadb.query

const instances = require('./instances')

function instanceMatchExists(className, { index, matchValue } = {}) {
  return q.Let({ results: instances(className, { index, matchValue, paginateOptions: { size: 1 }, ref_only: true }) }, q.If(q.IsNonEmpty(q.Var('results')), true, false))
}

module.exports = instanceMatchExists
