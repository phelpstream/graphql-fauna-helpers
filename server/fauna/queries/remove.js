const faunadb = require("faunadb");
const q = faunadb.query;

function remove(ref, ts, action) {
  return q.Remove(ref, ts, action);
}

module.exports = remove;
