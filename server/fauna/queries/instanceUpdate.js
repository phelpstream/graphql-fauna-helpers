const Debug = require('debug')
const debug = Debug('gfh-server [fauna:methods:instanceUpdate]')

const faunadb = require('faunadb')
const q = faunadb.query

const instanceRef = require('./instanceRef')

function instanceUpdate(className, { ref_id, ref, input }) {
  // debug('input', input)

  let refItem = instanceRef(className, { ref_id, ref })
  return q.Update(refItem, { data: input })
}

module.exports = instanceUpdate
