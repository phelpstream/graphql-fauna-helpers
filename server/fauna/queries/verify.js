const faunadb = require("faunadb");
const q = faunadb.query;

function verify(id, password) {
  return q.Identify(q.Ref(q.Class("users"), id), password);
}

module.exports = verify;
