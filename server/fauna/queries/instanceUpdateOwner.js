const faunadb = require('faunadb')
const q = faunadb.query

const instanceRef = require('./instanceRef')
const InstanceLogHelper = require('./../../back/entities/instance_log').helper

function instanceUpdateOwner(className, { ref_id, ref, user_id }) {
  let refItem = instanceRef(className, { ref_id, ref })
  return q.Update(refItem, { data: { log: InstanceLogHelper.own(user_id) } })
}

module.exports = instanceUpdateOwner
