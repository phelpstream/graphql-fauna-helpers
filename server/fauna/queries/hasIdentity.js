const faunadb = require("faunadb");
const q = faunadb.query;

function hasIdentity() {
  return q.HasIdentity();
}

module.exports = hasIdentity;
