const faunadb = require("faunadb");
const q = faunadb.query;

function load(variables = {}, next = {}) {
  return q.Let(variables, next);
}

module.exports = load;
