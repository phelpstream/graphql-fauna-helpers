const faunadb = require("faunadb");
const q = faunadb.query;

const uid = require("./../methods/uid");

function instanceCreate(className, { input = {}, credentials } = {}) {
  let createObj = {
    data: { ...input, id: uid() }
  };
  if (credentials) createObj.credentials = credentials;
  return q.Create(q.Class(className), createObj);
}

module.exports = instanceCreate;
