const faunadb = require("faunadb");
const q = faunadb.query;

function instanceRef(className, { ref_id, ref } = {}) {
  return ref || q.Ref(q.Class(className), ref_id);
}

module.exports = instanceRef;
