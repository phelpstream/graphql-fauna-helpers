const { isNil } = require('ramda')
const faunadb = require('faunadb')
const q = faunadb.query

const map = require('./map')
const paginate = require('./paginate')

const instancesOwnDef = require('./instancesOwnDef')

function instancesOwn(className, { user_id, queryMatch, paginateOptions, ref_only = false } = {}) {
  let paginated = paginate(instancesOwnDef(className, { user_id, queryMatch }), { className, ...paginateOptions })
  return ref_only ? paginated : q.Map(paginated, x => q.Get(x))
}

module.exports = instancesOwn
