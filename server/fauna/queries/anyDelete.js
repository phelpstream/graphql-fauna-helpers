const faunadb = require("faunadb");
const q = faunadb.query;

function anyDelete(ref) {
  return q.Delete(ref);
}

module.exports = anyDelete;
