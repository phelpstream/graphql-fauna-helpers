const Debug = require('debug')
const debug = Debug('gfh-server [fauna:queries:instancesList]')

const { get, getOr } = require('svpr')

const faunadb = require('faunadb')
const q = faunadb.query

// const instance = require('./instance')
const instanceRefAt = require('./instanceRefAt')
// const instanceMatch = require('./instanceMatch')

function instancesList(className, { index, matchValue, ref_ids = [], idsList = [], ats = [] }) {
  // , refs = []
  // ...refs.map((ref, i) => instance(className, { ref, at: get(i, ats) }))
  // ...ref_ids.map((ref_id, i) => (i ? instanceMatch(className, { index, matchValue }) : instance(className, { ref_id, at: get(i, ats) }))),
  // debug("className", className, "ref_ids", ref_ids, "ids", ids)
  let list = []
  list.push(...ref_ids.map((ref_id, i) => instanceRefAt(className, { ref_id, at: get(i, ats) })))
  list.push(...idsList.map(getOr({}, 'data')).map(ids => instanceRefAt(className, { ref_id: ids.ref_id, at: ids.at })))
  // debug("list", JSON.stringify(list, null, 2))
  return q.Map(list, x => q.Get(x))
}

module.exports = instancesList
