const Debug = require('debug')
const debug = Debug('gfh-server [fauna:methods:instanceReplace]')

const faunadb = require('faunadb')
const q = faunadb.query

const instanceRef = require('./instanceRef')

function instanceReplace(className, { ref_id, ref, input }) {
  let refItem = instanceRef(className, { ref_id, ref })
  return q.Replace(refItem, { data: input })
}

module.exports = instanceReplace
