const instanceHistory = require("./instanceHistory");
const selectFirstData = require("./selectFirstData");

function instanceCreationEvent(className, { id, ref_id, ref } = {}) {
  return selectFirstData(instanceHistory(className, { id, ref_id, ref, paginateOptions: { size: 1 } }));
}

module.exports = instanceCreationEvent;
