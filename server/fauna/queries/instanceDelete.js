const faunadb = require("faunadb");
const q = faunadb.query;

const instanceRef = require("./instanceRef");

function instanceDelete(className, { ref_id, ref } = {}) {
  return q.Delete(instanceRef(className, { ref_id, ref }));
}

module.exports = instanceDelete;
