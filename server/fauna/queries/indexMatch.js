const faunadb = require("faunadb");
const q = faunadb.query;

function indexMatch(className, { index, matchValue } = {}) {
  let indexName = index || `all_${className}`;
  return matchValue ? q.Match(q.Index(indexName), matchValue) : q.Match(q.Index(indexName));
}

module.exports = indexMatch;
