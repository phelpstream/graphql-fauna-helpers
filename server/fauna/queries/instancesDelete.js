const faunadb = require('faunadb')
const q = faunadb.query

const runParallel = require('./runParallel')
const instanceRef = require('./instanceRef')

function instancesCreate(className, { ref_ids = [] }) {
  let runs = ref_ids.map((ref_id, index) => {
    return q.Delete(instanceRef(className, { ref_id }))
  })
  return runParallel(...runs)
}

module.exports = instancesCreate
