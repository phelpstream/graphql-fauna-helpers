const faunadb = require("faunadb");
const q = faunadb.query;

function databaseCreate(name, { data, api_version, priority } = {}) {
  return q.CreateDatabase({
    name,
    data,
    api_version,
    priority
  });
}

module.exports = databaseCreate;
