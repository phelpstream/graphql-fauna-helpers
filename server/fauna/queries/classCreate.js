const faunadb = require("faunadb");
const q = faunadb.query;

function classCreate(name, { history_days = 30, ttl_days = null } = {}) {
  return q.CreateClass({
    name,
    history_days,
    ttl_days
  });
}

module.exports = classCreate;
