const faunadb = require("faunadb");
const q = faunadb.query;

function indexCreate(name, source, { terms, values, active, unique, serialized } = {}) {
  return q.CreateIndex({
    name,
    source,
    terms,
    values,
    active,
    unique,
    serialized
  });
}

module.exports = indexCreate;
