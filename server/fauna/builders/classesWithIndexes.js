const { is } = require("ramda");
const { buildIndexes } = require("./indexes");
const { buildClasses } = require("./classes");

const indexGenerators = require("./../../back/indexes/generators");

const args = require("./../../utils/args");

const pickIndexesOrDomains = ({ indexes = [], domains = {} } = {}) => {
  console.log("indexes", indexes, "domains", domains);
  return indexes.length > 0
    ? indexes
    : Object.keys(domains).length > 0
    ? Object.values(domains).reduce((list, d) => {
      // console.log("d.indexes", d.indexes);
        if (d && d.indexes) {
          list.push(d.indexes);
        }
        return list;
      }, [])
    : [];
};

const classesWithIndexes = ({ indexes = [], domains = {}, token = args()[0] } = {}) => {
  let localIndexes = pickIndexesOrDomains({ indexes, domains });
  // console.log("localIndexes", localIndexes);
  localIndexes.map(index => {
    if (is(Function, index)) index = index({ ig: indexGenerators, indexGenerators });
    // console.log("index", index);
    let classes = Object.keys(index);
    // console.log("buildClasses", classes);
    buildClasses(classes, token);
    // console.log("classes", classes, index);
    for (let c of classes) {
      // console.log("buildIndexes", index[c], c);
      buildIndexes(index[c], c, token);
    }
  });
};

// (async () => {
//   classesWithIndexes([
//     {
//       users: [
//         {
//           name: "classesWithIndexes_users"
//         }
//       ]
//     }
//   ]);
// })();

module.exports = classesWithIndexes;
