module.exports = {}

const clientCreate = require('./../methods/clientCreate')
const { q, refExists, indexCreate } = require('./../queries')
const args = require('./../../utils/args')

const buildIndexes = async (indexes = [], className, token = args()[0]) => {
  // let trackedIndexes = []
  let trackedIndexes = ['users_by_role_code', 'claims_by_participation_access_token', 'claims_by_assignee']
  const client = clientCreate(token)
  for (let index of indexes) {
    let isTracked = trackedIndexes.includes(index.name)
    // console.log("indexCreate", indexCreate);
    // console.log("index", index, "class", className);
    // indexCreate(index, q.Class(className))
    // console.log("index", { ...index, source: q.Class(className) });

    let createObj = Object.assign(
      {
        source: {
          collection: q.Collection(className)
        }
      },
      index
    )
    if (isTracked) {
      console.log('createObj:', createObj)
    }
    await client
      .query(q.If(refExists(q.Index(index.name)), null, q.CreateIndex(createObj)))
      .then((res) => {
        // if (res) {
        //   console.log('Index build:', index, 'for', className, res)
        // } else {
        //   console.log('Index exists!', index, 'for', className)
        // }
        if (isTracked) {
          console.log('Index:', index, 'for', className, res)
        }
      })
      .catch((err) => {
        if (isTracked) {
          console.log('Index:', index.name)
          console.error(err)
        }
      })
  }
}

const deleteIndexes = async (indexes = [], token = args()[0]) => {
  const client = clientCreate(token)
  for (let index of indexes) {
    await client.query(q.Delete(q.Index(index))).catch(console.error)
  }
}

const deleteListIndexes = async (token = args()[0]) => {
  const client = clientCreate(token)
  await client.query(q.Map(q.Paginate(q.Indexes(), { size: 1024 }), (x) => q.Delete(x)))
}

// (async () => {
//   deleteIndexes(["all_users"]);
// })();

module.exports = {
  buildIndexes,
  deleteIndexes,
  deleteListIndexes
}
