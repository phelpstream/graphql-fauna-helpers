const { clone } = require("ramda");
const clientCreate = require("./../methods/clientCreate");
const { q, refExists, indexCreate, classCreate } = require("./../queries");
const args = require("./../../utils/args");

const buildClasses = async (classes = [], token = args()[0]) => {
  const client = clientCreate(token);
  for (let c of clone(classes)) {
    await client
      .query(q.If(refExists(q.Class(c)), null, classCreate(c)))
      .then(res => {
        if (res) {
          console.log("Class build:", c, res);
        } else {
          console.log("Class exists!", c);
        }
      })
      .catch(console.error);
  }
};

module.exports = { buildClasses };
