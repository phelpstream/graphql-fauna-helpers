const fauna = require("faunadb");

function clientCreate(secret) {
  return new fauna.Client({
    secret: secret
  });
}

module.exports = clientCreate;
