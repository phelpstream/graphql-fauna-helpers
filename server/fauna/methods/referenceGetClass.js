const { get } = require("svpr");

module.exports = reference => get("@ref.class", JSON.parse(JSON.stringify(reference)));
