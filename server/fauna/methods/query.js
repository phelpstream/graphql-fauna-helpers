const fauna = require("faunadb");
const { is, pipe, curryN } = require("ramda");

function query(client, queryDefinition) {
  if (is(Array, queryDefinition)) {
    return queryDefinition.reduce(async (response, qDef) => {
      return await client.query(is(Function, qDef) ? qDef(response) : qDef);
    }, {});
  }
  return client.query(queryDefinition);
}

module.exports = curryN(2, query);
