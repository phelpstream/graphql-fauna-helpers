const Debug = require('debug')
const debug = Debug('gfh-server [fauna:methods:instancePatch]')

const { is } = require('ramda')
const { get } = require('svpr')
const jsonpatch = require('fast-json-patch')

// const instanceUpdate = require("./../queries/instanceUpdate");
// const instance = require('./../queries/instance')

const instancePatch = ctx => (className, { instanceType, instanceDefinition, ref_id, ref, input, replaceFn, argumentsModifier = x => x }) => {
  // ctx.query(instance(className, { ref_id, ref }))
  return ctx.binding.query[instanceType]({ ref_id }, instanceDefinition, { context: ctx }).then(instance => {
    let instanceData = get('data', instance)
    // debug('instanceData', instanceData)
    if (instanceData && is(Array, input) && input.length > 0) {
      let patchedInstanceData = jsonpatch.applyPatch(instanceData, input).newDocument
      // debug('instanceData', instanceData)
      debug('input', input)
      // debug('patchedInstanceData', patchedInstanceData.members.participations[0])

      if (is(Function, replaceFn)) {
        // debug("patchedInstanceData", patchedInstanceData);
        let filtered = argumentsModifier({ ref_id, ref, input: patchedInstanceData })
        // debug('filteredInstanceData', filtered)

        return replaceFn(filtered, ctx).then(res => {
          // debug('REPLACED REPLACED REPLACED', res)
          return res
        })
        // return ctx.query(instanceUpdate(className, { ref_id, ref, input: patchedInstanceData }))
      } else {
        // debug("CAN'T REPLACED", input.length, replaceFn)
      }
    }
    return instance
  })
}

module.exports = instancePatch
