const { is } = require("ramda");
const addRefId = require("./addRefId");
module.exports = arr => (is(Array, arr) ? arr.map(addRefId) : []);
