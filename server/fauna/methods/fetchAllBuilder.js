const faunadb = require("faunadb");
const q = faunadb.query;

const { get } = require("svpr");

const indexMatch = require("./../queries/indexMatch");

const fetchAllBuilder = clientQuery =>
  async function fetchAll(className, { index, matchValue, queryDefinition } = {}) {
    queryDefinition = queryDefinition || indexMatch(className, { index, matchValue });

    let data = [];

    const fetchNext = after =>
      clientQuery(
        q.Map(
          q.Paginate(queryDefinition, {
            size: 1024,
            after
          }),
          x => q.Get(x)
        )
      );

    let result = {};
    let after = undefined;

    do {
      result = await fetchNext(after);
      after = get("after.0", result);
      if (result.data) {
        data.push(...result.data);
      }
    } while (after);

    return {
      index: index || `all_${className}`,
      query: matchValue,
      data
    };
  };

module.exports = fetchAllBuilder;
