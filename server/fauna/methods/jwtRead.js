const faunadb = require("faunadb");
const q = faunadb.query;

var jwt = require("jsonwebtoken");

function jwtRead(token, secret) {
  return jwt.verify(token, secret);
}

module.exports = jwtRead;
