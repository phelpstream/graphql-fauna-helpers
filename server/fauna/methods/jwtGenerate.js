var jwt = require("jsonwebtoken");

function jwtGenerate({ data, secret }, ctx) {
  return jwt.sign(JSON.stringify(data), secret);
}

module.exports = jwtGenerate;
