const Debug = require('debug')
const debug = Debug('gfh-server [fauna:methods:instanceUpdateListItem]')

const { get, equals, set } = require('svpr')
const { over, lensPath, mergeDeepLeft } = require('ramda')

// const instance = require('./../queries/instance')
// const instanceUpdate = require('./../queries/instanceUpdate')

const instanceUpdateListItem = ctx => (
  className,
  { instanceType, instanceDefinition, ref_id, id, listPath, uniquePath, uniqueValue, input = {}, remove = false, updateFn, argumentsModifier = x => x } = {}
) =>
  // ctx.query(instance(className, { ref_id })).
  {
    // console.log('ctx.binding.query', instanceType, ctx.binding.query[instanceType])
    return ctx.binding.query[instanceType]({ id, ref_id }, instanceDefinition, { context: ctx }).then(instanceData => {
      let instanceRefId = get('data.ref_id', instanceData)
      let updatedItem = {}
      let updatedItems = []
      // debug('ref_id', ref_id, 'input', input, 'listPath', listPath)
      let items = get(`${listPath}`, get('data', instanceData)) || []
      // debug('items', items)
      let getValue = value => (uniquePath ? get(uniquePath, value) : value)
      let getUniqueValue = () => uniqueValue || input
      let existingItem = items.find(p => equals(getValue(p), getUniqueValue()))
      let filteredItems = items.filter(p => !equals(getValue(p), getUniqueValue()))
      debug('filteredItems', filteredItems)
      if (remove) {
        updatedItems = filteredItems
      } else {
        if (existingItem) {
          // debug('mergeDeepLeft(item, updatedItem)', mergeDeepLeft(item, existingItem), item, existingItem)
          if (uniquePath) {
            updatedItem = mergeDeepLeft(over(lensPath(uniquePath.split('.')), x => getUniqueValue(), {}), mergeDeepLeft(input, existingItem))
            // } else if (uniquePath.length === 0) {
          } else {
            updatedItem = input
          }
        } else {
          updatedItem = input
        }
        debug('updatedItem', updatedItem)
        updatedItems = [...filteredItems, updatedItem]
        debug('updatedItems', updatedItems)
      }
      let newInput = set(listPath, updatedItems, {})
      debug('ref_id', ref_id, 'input', newInput)
      let cleanedArgs = argumentsModifier({ ref_id: ref_id || instanceRefId, input: newInput })
      debug('cleanedArgs', cleanedArgs)
      return updateFn(cleanedArgs, ctx)
      // return ctx.query(instanceUpdate(className, { ref_id, input: set(listPath, updatedItems, {}) })).then(result => {
      //   debug('result', result)
      //   return result
      // })
      //  over(lensPath(listPath.split('.')), x => updatedItems, {}) }))
    })
  }

module.exports = instanceUpdateListItem
