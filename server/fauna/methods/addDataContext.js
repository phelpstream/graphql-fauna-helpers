const Debug = require('debug')
const debug = Debug('gfh-server [fauna:methods:addDataContext]')

const { is, merge, pipe, omit } = require('ramda')
const { get, getOr, set, clone } = require('svpr')
const referenceGetId = require('./referenceGetId')

// const fs = require('fs')
// const path = require('path')

const arraylike = require('@mnmldev/arraylike')

const addDataContext = (ctx, { boolean = false } = {}) => (res = {}) => {
  const addRefId = ref => obj => {
    // debug('obj', ref, obj)
    return { ref_id: obj.ref_id || referenceGetId(ref), ...obj }
  }
  const addLogTs = ts => obj => {
    if (get('log', obj) && ts) return set('log.ts', ts / 1000, obj)
    return obj
  }

  // debug('res', res)
  // debug('boolean', boolean)

  let returnedData = res
  // if (!fs.existsSync(path.resolve(__dirname, './db.json')) && is(Object, returnedData) && returnedData.data.reference === 'F3QRESPEP') fs.writeFileSync(path.resolve(__dirname, './db.json'), JSON.stringify(returnedData, null, 2))

  let request_duration = new Date().getTime() - (ctx.request.start || new Date().getTime() + 1)
  let next_ref_id = referenceGetId(getOr({}, 'after.0', res))
  let previous_ref_id = referenceGetId(getOr({}, 'before.0', res))

  if (is(Object, returnedData) && get('isCount', returnedData || {})) {
    return {
      data: returnedData.data,
      context: {
        next_ref_id,
        previous_ref_id,
        request_duration
      }
    }
  } else if (!boolean) {
    let { ref, data, ts, after, before } = res
    returnedData = clone(data)
    // debug('res', res)

    // Array result
    if (is(Array, res)) {
      returnedData = clone(res).map(item => pipe(addRefId(get('ref', item)), addLogTs(get('ts', item)))(get('data', item)))
    } else if (is(String, res) || is(Number, res)) {
      returnedData = res
    } else {
      // Obj result
      if (is(Array, returnedData)) {
        returnedData = returnedData.map(item => pipe(addRefId(get('ref', item)), addLogTs(get('ts', item)))(get('data', item)))
      } else if (is(Object, data)) {
        returnedData = pipe(addRefId(ref), addLogTs(ts))(returnedData)
      }
    }
    // console.time('containsArrayLike')
    let containsArrayLike = arraylike.containsArrayLike(returnedData || {})
    // console.timeEnd('containsArrayLike')
    // console.log('containsArrayLike', containsArrayLike)
    if (containsArrayLike) {
      // console.time('fromArrayLike')
      // console.log('returnedData is Array', is(Array, returnedData), returnedData)
      // if (!fs.existsSync(path.resolve(__dirname, './raw.json')) &&is(Object, returnedData) && returnedData.reference === 'F3QRESPEP') fs.writeFileSync(path.resolve(__dirname, './raw.json'), JSON.stringify(returnedData, null, 2))
      returnedData = is(Array, returnedData) ? returnedData.map(arraylike.fromArrayLike) : arraylike.fromArrayLike(returnedData)
      // if (!fs.existsSync(path.resolve(__dirname, './arraylikearray.json')) &&is(Object, returnedData) && returnedData.reference === 'F3QRESPEP') fs.writeFileSync(path.resolve(__dirname, './arraylikearray.json'), JSON.stringify(returnedData, null, 2))
      // console.timeEnd('fromArrayLike')
    }
    // console.log('returnedData', returnedData)

    // debug('returned', {
    //   data: returnedData,
    //   context: {
    //     ref,
    //     ts,
    //     next_ref_id,
    //     previous_ref_id,
    //     request_duration
    //   }
    // })
    return {
      data: returnedData,
      context: {
        ref,
        ts,
        next_ref_id,
        previous_ref_id,
        request_duration
      }
    }
  } else {
    let returned = {
      data: returnedData,
      context: {
        next_ref_id,
        previous_ref_id,
        request_duration
      }
    }
    // debug('returned', returned)
    return returned
  }
}

module.exports = addDataContext
