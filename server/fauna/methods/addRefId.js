const { get, getOr } = require("svpr");
const referenceGetId = require("./referenceGetId");

const addRefId = (res = {}) => {
  // console.log("res", res);
  let n = {
    ref_id: referenceGetId(getOr({}, "ref", res)),
    ...getOr({}, "data", res),
    log: Object.assign(getOr({}, "log", res), {
      _ref: res.ref,
      _ts: res.ts
    })
  };
  // console.log("n", n);
  return n;
};

module.exports = addRefId;
