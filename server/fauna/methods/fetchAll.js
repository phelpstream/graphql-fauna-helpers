const faunadb = require("faunadb");
const q = faunadb.query;

const { has } = require("ramda");
const { get } = require("svpr");

const fetchAllBuilder = require("./fetchAllBuilder");

async function fetchAll(className, { index, matchValue, queryDefinition } = {}, ctx) {
  return fetchAllBuilder(ctx.query)(className, ({ index, matchValue, queryDefinition } = {}));
}

module.exports = fetchAll;
