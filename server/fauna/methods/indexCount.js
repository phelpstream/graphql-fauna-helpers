const faunadb = require('faunadb')
const q = faunadb.query

const { has } = require('ramda')
const { get } = require('svpr')

const runParallel = require('./../queries/runParallel')
const indexMatch = require('./../queries/indexMatch')

async function indexCount(className, { index, matchValue, matchValues = [], queryDefinition } = {}, ctx) {
  // const fetchNext = after =>
  //   ctx.query(
  //     q.Paginate(queryDefinition, {
  //       size: 1024,
  //       after
  //     })
  //   )

  // let number = 0
  // let result = {}
  // let after = undefined

  // do {
  //   result = await fetchNext(after)
  //   if (has('data', result)) {
  //     number = number + (result.data.length || 0)
  //   }
  //   after = get('after.0', result)
  // } while (after)

  let usedIndex = index || `all_${className}`

  // console.log("matchValues", matchValues);
  if (matchValues.length > 0) {
    let queryDefs = matchValues.map(mv => q.Count(indexMatch(className, { index: usedIndex, matchValue: mv })))
    let numbers = await ctx.query(runParallel(queryDefs))
    
    return {
      isCount: true,
      data: numbers[0].map((number, index) => ({
        index: usedIndex,
        number,
        input: matchValues[index],
        query: queryDefs[index]
      }))
    }
  } else {
    let queryDef = queryDefinition || indexMatch(className, { index: usedIndex, matchValue })
    let number = await ctx.query(q.Count(queryDef))
    return {
      isCount: true,
      data: [
        {
          index: usedIndex,
          number,
          input: matchValue,
          query: queryDef
        }
      ]
    }
  }
}

module.exports = indexCount
