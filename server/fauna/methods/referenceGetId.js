const { is } = require('ramda')
const { get } = require('svpr')

module.exports = reference => (is(Object, reference) ? get('@ref.id', JSON.parse(JSON.stringify(reference))) : undefined)
