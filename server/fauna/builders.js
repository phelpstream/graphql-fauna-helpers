module.exports = {
  classesWithIndexes: require("./builders/classesWithIndexes"),
  indexes: require("./builders/indexes"),
  classes: require("./builders/classes")
};
