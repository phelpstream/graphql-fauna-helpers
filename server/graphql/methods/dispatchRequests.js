const Debug = require('debug')
const debug = Debug('gfh-server [graphql:methods:dispatchRequests]')
const { pick } = require('ramda')

const dispatchRequests = (ctx, requests = []) => (res = {}) => {
  let rqs = requests.map(rq => {
    let req = pick(['method', 'variables', 'definition'], rq)
    try {
      // debug('rq', req)
      ctx.graphql.dispatch(req)
    } catch (error) {
      debug('error', req, error)
    }
    return req
  })

  res.requests = { list: rqs }

  return res
}

module.exports = dispatchRequests
