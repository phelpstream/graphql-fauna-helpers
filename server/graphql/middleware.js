module.exports = {
  defaults: require('./middleware/defaults'),
  cache: require('./middleware/cache'),
  sentry: require('./middleware/sentry'),
  log: require('./middleware/log'),
  auth: require('./middleware/auth')
}
