const { uniq, is } = require("ramda");

const fnOptions = require("./fnOptions");
const processBuilder = input => (is(Function, input) ? input(fnOptions) : input);

const mergeMethodsByDomain = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, methods]) => {
    if (!baseObj[key]) baseObj[key] = [];
    let methodsObj = {};
    if (methods.queries) methodsObj = Object.assign(methodsObj, processBuilder(methods.queries));
    if (methods.mutations) methodsObj = Object.assign(methodsObj, processBuilder(methods.mutations));
    baseObj[key] = uniq([...baseObj[key], ...Object.keys(methodsObj)]);
  });
};

const buildMethodsByDomain = ({ domains = {}, domainsList = [] }) => {
  let methodsByDomain = {};

  mergeMethodsByDomain(domains, methodsByDomain);
  domainsList.forEach(domainsItem => mergeMethodsByDomain(domainsItem, methodsByDomain));

  return methodsByDomain;
};

module.exports = buildMethodsByDomain;
