const { pick, is } = require("ramda");
const { get, getOr } = require("svpr");

const buildFullValues = (typeMap, typeName) => {
  console.time("fullValues");
  let types = [];
  if (typeName) {
    types = [typeName];
  } else {
    types = Object.entries(typeMap).reduce((list, [name, type]) => {
      if (!type || name.startsWith("__")) return list;
      if (type._fields && Object.keys(type._fields).length > 0) list.push(name);
      return list;
    }, []);
  }

  let cachedFullValues = {};

  const makeDefaultValue = defTypeName => {
    if (cachedFullValues[defTypeName]) return cachedFullValues[defTypeName];
    return Object.entries(typeMap[defTypeName]._fields).reduce((def, [name, field]) => {
      let isArray = field.type && `${field.type}`.startsWith("[");
      let cleanTypeName = `${field.type}`.replace(/\W/gim, "");
      let type = getOr({}, cleanTypeName, typeMap);
      if (type && type._fields) {
        def[name] = `##${cleanTypeName}##`;
        if (isArray) def[name] = [def[name]];
      } else {
        def[name] = field.defaultValue ? field.defaultValue : null;
      }
      return def;
    }, {});
  };

  let result = {};
  for (let typeNameItem of types) {
    result[typeNameItem] = JSON.stringify(makeDefaultValue(typeNameItem));
  }

  const hasMissingTags = () => Object.values(result).reduce((count, v) => count + (v.match(new RegExp("##", "g")) || []).length, 0);

  const updateFields = () =>
    Object.entries(result).forEach(([type, def]) => {
      for (let [entryType, entryDef] of Object.entries(result)) {
        if (entryDef !== type) {
          result[type] = result[type].replace(new RegExp(`"##${entryType}##"`, "g"), entryDef);
        }
      }
    });

  let counter = 0;
  while (hasMissingTags() > 0 && counter < 300) {
    console.log("hasMissingTags", hasMissingTags());
    updateFields();
    // if(hasMissingTags() < 10){}
    counter++;
  }

  result = Object.entries(result).reduce((res, [key, str]) => {
    res[key] = JSON.parse(str);
    return res;
  }, {});

  console.timeEnd("fullValues");
  return result;
};

module.exports = buildFullValues;
