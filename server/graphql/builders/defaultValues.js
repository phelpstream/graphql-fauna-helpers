const { pick, is } = require('ramda')
const { has, get, getOr } = require('svpr')

const buildDefaultValues = (typeMap, typeName) => {
  console.time('defaultValues')
  let types = []
  if (typeName) {
    types = [typeName]
  } else {
    types = Object.entries(typeMap).reduce((list, [name, type]) => {
      if (!type || name.startsWith('__')) return list
      // console.log("type", type);
      if (type._fields && Object.keys(type._fields).length > 0) list.push(name)
      return list
    }, [])
  }

  let cachedDefaultValues = {}

  const makeDefaultValue = defTypeName => {
    if (cachedDefaultValues[defTypeName]) return cachedDefaultValues[defTypeName]
    return Object.entries(typeMap[defTypeName]._fields).reduce((def, [name, field]) => {
      let isArray = field.type && `${field.type}`.includes('[')
      // console.log("field.type", field.type, isArray);
      let cleanTypeName = `${field.type}`.replace(/[\[\]\!]/gim, '') //.replace(/\W/gim, '')
      let type = getOr({}, cleanTypeName, typeMap)

      console.log('sepa_accounts')
      if (name === 'sepa_accounts') {
        console.log('cleanTypeName', cleanTypeName)
        console.log('type', type)
        console.log('field.defaultValue', field.defaultValue)
      }
      if (isArray) {
        //&& field.defaultValue
        if (field.defaultValue == [null] && type && type._fields) {
          console.log('field.defaultValue == [null]', [`##${cleanTypeName}##`])
          def[name] = [`##${cleanTypeName}##`]
        } else {
          def[name] = field.defaultValue || []
        }
      } else if (!isArray) {
        let valueToSet
        if (type && type._fields) {
          valueToSet = `##${cleanTypeName}##` //defaultValue(cleanType, typeMap);
        } else {
          valueToSet = field.defaultValue || null
        }
        // if (field.defaultValue) {
        // 	console.log('isArray', field.type, isArray, 'want[]', field.defaultValue, )
        // }

        // if (isArray && is(Array, field.defaultValue) && field.defaultValue.length === 0) {
        //   def[name] = [valueToSet]
        // } else {
        def[name] = valueToSet
        // }
      }
      return def
    }, {})
  }

  let result = {}
  for (let typeNameItem of types) {
    result[typeNameItem] = JSON.stringify(makeDefaultValue(typeNameItem))
  }

  const hasMissingTags = () => Object.values(result).reduce((count, v) => count + (v.match(new RegExp('##', 'g')) || []).length, 0)

  const updateFields = () =>
    Object.entries(result).forEach(([type, def]) => {
      for (let [entryType, entryDef] of Object.entries(result)) {
        if (entryDef !== type) {
          result[type] = result[type].replace(new RegExp(`"##${entryType}##"`, 'g'), entryDef)
        }
      }
    })

  let counter = 0
  while (hasMissingTags() > 0 && counter < 300) {
    console.log('hasMissingTags', hasMissingTags())
    updateFields()
    counter++
  }

  result = Object.entries(result).reduce((res, [key, str]) => {
    res[key] = JSON.parse(str)
    return res
  }, {})

  console.timeEnd('defaultValues')
  return result
}

module.exports = buildDefaultValues
