const { pick, is } = require('ramda')
const { has, get, getOr, equals } = require('svpr')

const buildDefaultValues = (typeMap, typeName) => {
  console.time('defaultValues')
  let types = []
  if (typeName) {
    types = [typeName]
  } else {
    types = Object.entries(typeMap).reduce((list, [name, type]) => {
      if (!type || name.startsWith('__')) return list
      // console.log("type", type);
      if (type._fields && Object.keys(type._fields).length > 0) list.push(name)
      return list
    }, [])
  }

  let cachedDefaultValues = {}

  const makeDefaultValue = defTypeName => {
    if (cachedDefaultValues[defTypeName]) return cachedDefaultValues[defTypeName]
    return Object.entries(typeMap[defTypeName]._fields).reduce((def, [name, field]) => {
      let isArray = field.type && `${field.type}`.includes('[')
      let cleanTypeName = `${field.type}`.replace(/[\[\]\!]/gim, '') //.replace(/\W/gim, '')
      let type = getOr({}, cleanTypeName, typeMap)
      let wrappedInArray = equals(field.defaultValue, [1])

      if (type && type._fields && isArray === wrappedInArray) {
        def[name] = {
          ['$RECURSIVE_TYPE']: {
            type: cleanTypeName,
            array: wrappedInArray
          }
        }
      } else {
        def[name] = field.defaultValue || (isArray ? [] : null)
      }

      return def
    }, {})
  }

  for (let typeNameItem of types) {
    cachedDefaultValues[typeNameItem] = makeDefaultValue(typeNameItem)
  }

  console.timeEnd('defaultValues')
  return cachedDefaultValues
}

module.exports = buildDefaultValues
