const namings = require("./../../back/namings");

const buildDomainsMethodsNames = (domains) => {
  return Object.keys(domains).reduce((obj, name) => {
    obj[name] = Object.entries(namings).reduce((namingsObj, [n, fn]) => {
      namingsObj[n] = fn(name);
      return namingsObj;
    }, {});
    return obj;
  }, {});
};

module.exports = buildDomainsMethodsNames;
