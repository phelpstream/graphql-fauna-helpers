const { getOr } = require("svpr");

const methodsArgumentTypes = schema => {
  let queryFields = schema.getQueryType()._fields;
  queryFields = Object.entries(queryFields).map(([name, def = {}]) => ({
    name,
    args: getOr([], "args", def)
  }));
  let mutationFields = schema.getMutationType()._fields;
  mutationFields = Object.entries(mutationFields).map(([name, def = {}]) => ({
    name,
    args: getOr([], "args", def)
  }));

  let queries = {};
  let mutations = {};

  const argTypesBuilder = args => {
    let argTypes = args.reduce((argsObj, arg) => {
      argsObj[arg.name] = arg.type;
      return argsObj;
    }, {});
    return argTypes;
  };

  queries = queryFields.reduce((obj, query) => {
    obj[query.name] = argTypesBuilder(query.args);
    return obj;
  }, {});

  mutations = mutationFields.reduce((obj, mutation) => {
    obj[mutation.name] = argTypesBuilder(mutation.args);
    return obj;
  }, {});

  return {
    queries,
    mutations
  };
};

module.exports = methodsArgumentTypes;
