const { is } = require('ramda')
const { has, get } = require('svpr')

const fnOptions = require('./fnOptions')

const defaultDefinitionsBuilder = require('./defaultDefinitions')
const defaultValuesBuilder = require('./defaultValues')
const defaultValuesMapBuilder = require('./defaultValuesMap')
const fullValuesBuilder = require('./fullValues')
const domainsMethodsNamesBuilder = require('./domainsMethodsNames')
const methodsByDomainBuilder = require('./methodsByDomain')
const inputTypeFiltersBuilder = require('./inputTypeFilters')
const methodsArgumentTypesBuilder = require('./methodsArgumentTypes')

const fnToObj = (input = {}) => (is(Function, input) ? input(fnOptions) : input)

const methodTypesBuilder = (methodsObj = {}) =>
	Object.entries(methodsObj).reduce((listObj, [name, method]) => {
		if (has('type.name', method)) {
			listObj[name] = `${get('type.name', method)}`.replace(/\W/gim, '')
			// console.log("method name", name, "type", listObj[name]);
		}
		return listObj
	}, {})

const collectionBuilder = (methodsObj = {}) => {
	let collections = Object.entries(methodsObj).reduce((listObj, [name, method]) => {
		if (method._collections) {
			// console.log("name", name, method._collections);
			listObj[name] = Object.entries(method._collections).reduce((list, [colName, collection = {}]) => {
				list.push({
					name: collection.name || colName,
					...collection
				})
				return list
			}, [])
		}
		return listObj
	}, {})

	// console.log("collections", collections);

	return collections
}

module.exports = {
	defaultDefinitionsBuilder,
	defaultValuesBuilder,
	defaultValuesMapBuilder,
	domainsMethodsNamesBuilder,
	genericBuilder: (whatever = {}) => fnToObj(whatever),
	mutationBuilder: (mutation = {}) => fnToObj(mutation),
	queryBuilder: (query = {}) => fnToObj(query),
	resolverBuilder: (resolver = {}) => fnToObj(resolver),
	collectionBuilder,
	methodTypesBuilder,
	methodsByDomainBuilder,
	inputTypeFiltersBuilder,
	fullValuesBuilder,
	methodsArgumentTypesBuilder
}
