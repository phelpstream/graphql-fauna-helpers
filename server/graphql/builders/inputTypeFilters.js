const { pick, is, difference, uniq } = require('ramda')
const { get, getOr, crunch } = require('svpr') // crunch
// const fullValues = require("./fullValues");

const inputTypeFilters = types => {
  let inputTypes = Object.keys(types).filter(t => t.endsWith('Input'))

  return inputTypes.reduce((obj, itype) => {
    let local_type = types[itype.replace('Input', '')]
    let local_input_type = types[itype]

    if (local_type && local_input_type) {
      let flat = crunch(local_type, { includeTree: true })
      let flat_input = crunch(local_input_type, { includeTree: true })

      let flat_keys = uniq(Object.keys(flat).map(i => i.replace(/\.0/gim, '..').replace(/\.\.\./gim, '..')))
      let flat_input_keys = uniq(Object.keys(flat_input).map(i => i.replace(/\.0/gim, '..').replace(/\.\.\./gim, '..')))

      let keys_diff = difference(flat_keys, flat_input_keys)

      // if (itype === 'ClaimInput') {
      //   console.log('flat[flight.route.first_flight_gps]', flat['flight.route.first_flight_gps'])
      //   console.log('flat[flight.route.last_flight_gps]', flat['flight.route.last_flight_gps'])
      //   // console.log("keys_diff", keys_diff);
      // }

      // Remove all the unecessary deeper item to remove when parent is removed :D
      let keys_diff_cleaned = keys_diff
        .sort((a, b) => {
          // sort from deep to root
          let aLength = a.split('.').length - 1
          let bLength = b.split('.').length - 1
          return aLength - bLength
        })
        .reduce((list, path) => {
          if (!list.some(item => path.startsWith(`${item}.`))) {
            list.push(path)
          }
          return list
        }, [])

      // console.log("flat_keys", flat_keys.length, "flat_input_keys", flat_input_keys.length);

      obj[itype.replace('Input', '')] = keys_diff_cleaned
    }

    return obj
  }, {})
}

module.exports = inputTypeFilters
