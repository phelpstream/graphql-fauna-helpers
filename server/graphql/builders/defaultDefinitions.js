// const { pick, is } = require("ramda");
// const { get, getOr } = require("svpr");

const builDefinition = (typeMap, typeName) => {
	console.time('defaultDefinitions')
	let types = ["claims_by_participation_access_token"]
	if (typeName) {
		types = [typeName]
	} else {
		types = Object.entries(typeMap).reduce((list, [name, type]) => {
			if (!type || name.startsWith('__')) return list
			// console.log("type", type);
			if (type._fields && Object.keys(type._fields).length > 0) list.push(name)
			return list
		}, [])
	}

	let cachedDefinitions = {}

	const makeDefinition = defTypeName => {
		if (cachedDefinitions[defTypeName]) return cachedDefinitions[defTypeName]
		let fieldsList = Object.entries(typeMap[defTypeName]._fields || {})
		let definition = ''
		if (fieldsList.length > 0) {
			let sortFields = fieldsList.reduce(
				(subLists, [name, field]) => {
					let cleanTypeName = `${field.type}`.replace(/\W/gim, '')
					// console.log("!typeMap[cleanTypeName]", cleanTypeName);
					if (typeMap[cleanTypeName] && !typeMap[cleanTypeName]._fields) subLists.solo.push([name, field])
					else subLists.recursive.push([name, cleanTypeName])
					return subLists
				},
				{ solo: [], recursive: [] }
			)
			definition = sortFields.solo.map(([name]) => name).join(' ')
			let additionalDefinition = sortFields.recursive.reduce((def, [name, cleanTypeName]) => {
				def = `${def} ${name}{ ##${cleanTypeName}## }`
				return def
			}, '')
			// console.log("additionalDefinition", additionalDefinition, sortFields.recursive.length);
			return `${definition} ${additionalDefinition}`
		}
		return definition
	}

	for (let typeNameItem of types) {
		cachedDefinitions[typeNameItem] = makeDefinition(typeNameItem)
	}

	// const hasMissingTags = () => Object.values(result).reduce((count, v) => count + (v.match(new RegExp("##", "g")) || []).length, 0);

	// const updateFields = () =>
	//   Object.entries(result).forEach(([type, def]) => {
	//     for (let [entryType, entryDef] of Object.entries(result)) {
	//       if (entryDef !== type) {
	//         // console.log("`##${entryType}##`", `##${entryType}##`);
	//         // let savedStr = result[type];
	//         result[type] = result[type].replace(new RegExp(`##${entryType}##`, "g"), entryDef);
	//         // let removed = savedStr.length - result[type];
	//       }
	//     }
	//   });

	// let counter = 0;
	// while (hasMissingTags() > 0 && counter < 20) {
	//   console.log("hasMissingTags", hasMissingTags());
	//   updateFields();
	//   counter++;
	// }

	// if(hasMissingTags()){
	//   console.error("STILL MISSING TAGS OMG");
	//   throw Error('STILL MISSING TAGS OMG')
	// }

	console.timeEnd('defaultDefinitions')
	return cachedDefinitions
}

module.exports = builDefinition
