const short = require("./../../short");
const t = require("./../type");

const fnOptions = {
  types: t,
  t: t,
  scalars: short.s,
  s: short.s,
  entities: short.e,
  e: short.e,
  enums: short.enu,
  enu: short.enu,
  resources: short.r,
  r: short.r,
  mutationGenerators: short.mg,
  mg: short.mg,
  queryGenerators: short.qg,
  qg: short.qg,
  resolverGenerators: short.rg,
  rg: short.rg
};

module.exports = fnOptions;
