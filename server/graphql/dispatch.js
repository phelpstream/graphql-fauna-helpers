const Debug = require('debug')
const debug = Debug('gfh-server [graphql:dispatch]')

const { merge, is } = require('ramda')
const { getOr, get } = require('svpr')
const aws = require('aws-sdk')
const gql = require('graphql-tag')

const formatDefinition = (defString = '') => {
  if (is(Object, defString)) defString = getOr('', 'loc.source.body', defString)
  let definition = defString
  let compacted = definition.replace(/\s*/gim, '')
  if (!compacted.startsWith('{')) {
    definition = `{${definition}}`
  }
  return definition
}

const formatFragments = (definitionObj = {}) => {
  if (is(Object, definitionObj) && definitionObj.fragments) {
    return definitionObj.fragments
  }
  return ''
}

const queryBuilder = (queryName, { args = {}, variables = {}, definition } = {}) => {
  variables = variables || {}
  let params = Object.keys(variables).reduce((list, param) => {
    list.push(`$${param}: ${getOr('String', param, args)}`)
    return list
  }, [])
  let hasParams = params.length > 0
  let paramsStr = hasParams ? params.join(' ') : ''
  let varsStr = hasParams
    ? Object.keys(variables)
        .map(v => `${v}: $${v}`)
        .join(' ')
    : ''
  let queryStr = ''
  let definitionStr = definition ? formatDefinition(definition) : '' //`{${definition}}` : ''
  let fragmentsStr = formatFragments(definition)
  if (hasParams) {
    queryStr = `
    query ${queryName}(${paramsStr}) {
      ${queryName}(${varsStr})
        ${definitionStr}
    }

    ${fragmentsStr}
    `
  } else {
    queryStr = `
    {
      ${queryName}
        ${definitionStr}
    }

    ${fragmentsStr}
    `
  }
  let query = {
    // gql
    query: `
      ${queryStr}
    `,
    variables
  }

  // debug('query', query)
  return query
}

const mutationBuilder = (mutationName, { args = {}, variables = {}, definition } = {}) => {
  let params = Object.keys(variables).reduce((list, param) => {
    list.push(`$${param}: ${getOr('String', param, args)}`)
    return list
  }, [])
  let hasParams = params.length > 0
  let paramsStr = hasParams ? params.join(' ') : ''
  let varsStr = hasParams
    ? Object.keys(variables)
        .map(v => `${v}: $${v}`)
        .join(' ')
    : ''
  let mutationStr = ''
  let definitionStr = definition ? formatDefinition(definition) : '' //`{${definition}}` : ''
  let fragmentsStr = formatFragments(definition)
  if (hasParams) {
    mutationStr = `
   mutation ${mutationName}(${paramsStr}) {
     ${mutationName}(${varsStr})
       ${definitionStr}
   }

   ${fragmentsStr}
   `
  } else {
    mutationStr = `
   {
     ${mutationName}
      ${definitionStr}
     
   }

   ${fragmentsStr}
   `
  }
  let mutation = {
    // gql
    mutation: `
      ${mutationStr}
    `,
    variables
  }
  return mutation
}

const bodyBuilder = (method, { variables = {}, args = {}, definition, isMutation, isQuery } = {}) => {
  return isMutation ? mutationBuilder(method, { variables, args, definition }) : queryBuilder(method, { variables, args, definition })
}

// const bodyBuilder = (method, { variables = {}, definition, isMutation, isQuery } = {}) => {
//   variables = variables || {}
//   let params = Object.keys(variables).reduce((list, param) => {
//     list.push(`$${param}: ${getOr('String', param, args)}`)
//     return list
//   }, [])
//   let hasParams = params.length > 0
//   let paramsStr = hasParams ? params.join(' ') : ''
//   let varsStr = hasParams
//     ? Object.keys(variables)
//         .map(v => `${v}: $${v}`)
//         .join(' ')
//     : ''
//   let requestDefinition = ''
//   let definitionStr = definition ? formatDefinition(definition) : '' //`{${definition}}` : ''
//   if (hasParams) {
//     requestDefinition = `${isQuery ? 'query' : 'mutation'} ${method}(${paramsStr}) {
//       ${method}(${varsStr})
//         ${definitionStr}
//     }`
//   } else {
//     requestDefinition = `
//     {
//       ${method}
//         ${definitionStr}
//     }
//     `
//   }
//   let body = {
//     [isQuery ? 'query' : 'mutation']: `
//       ${requestDefinition}
//     `,
//     variables
//   }

//   debug('body', body)
//   return body
// }

const dispatchBuilder = ({ event = {}, context = {}, callback, endpoints = {} } = {}, { collectMethod, serverless } = {}) => {
  // debug('dispatchBuilder')
  return function dispatch({ method, variables = {}, definition }) {
    // debug('dispatch')
    let { isQuery, isMutation, methodArgs, fullDefinition, argsFilter } = collectMethod(method)

    debug('method', method)
    let body = bodyBuilder(method, { variables: argsFilter(variables), args: methodArgs, definition: definition || fullDefinition, isQuery, isMutation })
    // debug('body', JSON.stringify(body))

    let newEvent = merge(event, { body: JSON.stringify(body) })
    // debug('newEvent', newEvent)

    if (process.env.IS_OFFLINE) {
      endpoints.graphql(newEvent, context, (error, response) => {
        if(error) debug('callback', error)
        // if(response.body.errors) debug('body errors', response)
        // debug(response)
        // debug("Response received!", response)
      })
      debug(`Dispatched (locally) new event to ${context.functionName}`) // newEvent
    } else {
      const lambda = new aws.Lambda({
        region: process.env.SERVERLESS_REGION,
        sessionToken: process.env.AWS_SESSION_TOKEN
      })
      // debug('received', event)
      const params = {
        FunctionName: context.functionName,
        InvocationType: 'Event',
        Payload: JSON.stringify(newEvent),
        Qualifier: context.functionVersion
      }
      lambda.invoke(params, (error, response) => {
        // debug('callback', error, response)
      })
      debug(`Dispatched (online) new event to ${context.functionName}`) // newEvent
    }
  }
}

module.exports = dispatchBuilder
