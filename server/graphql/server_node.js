const Debug = require('debug')
const debug = Debug('gfh-server [graphql:server_node]')

const uuid = require('uuid')
const md5 = require('md5')
const memoize = require('memoizee')
const { is, omit } = require('ramda')
const { getOr, get, clone } = require('svpr')
const { Binding } = require('graphql-binding')
const graphqlMethods = require('./methods')
const aws = require('./../aws')
const awsEventsBuilder = aws.events
const f = require('./../fauna')
const a = require('./../algolia')
const ResolverCache = require('./cache/resolver')
const { resolverBuilder, mutationBuilder, queryBuilder, collectionBuilder, methodTypesBuilder } = require('./builders')
const { readDefinitions, readDefaultValuesMap } = require('./readers')
const dispatchBuilder = require('./dispatch')
const argumentsFilter = require('./filters/arguments')

const baseResources = require('./../back/resources')
const namings = require('./../back/namings')
const EventsManager = require('./../back/events')
const eventsHelpers = require('./../back/events/helpers')

const baseUtils = {
  texts: require('./../utils/texts'),
  dates: require('./../utils/dates'),
  functions: require('./../utils/functions'),
  jwt: require('./../utils/jwt'),
  if: (value, elseFn = x => x) => (value ? elseFn(value) : undefined)
}


const mergeMethodsQueryTypes = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, domain]) => {
    if (domain.queries) {
      let buildQueries = queryBuilder(domain.queries)
      for (let name of Object.keys(buildQueries)) {
        Object.assign(baseObj, { [name]: 'Query' })
      }
    }
    if (domain.mutations) {
      let buildMutations = mutationBuilder(domain.mutations)
      for (let name of Object.keys(buildMutations)) {
        Object.assign(baseObj, { [name]: 'Mutation' })
      }
    }
  })
}

const mergeAlgoliaIndices = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, domain]) => {
    if (domain.algolia_indices) {
      return Object.entries(domain.algolia_indices).map(([indiceName, indiceDef]) => {
        Object.assign(baseObj, { [indiceName]: indiceDef })
      })
    }
  })
}

const mergeCollections = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, domain]) => {
    if (domain.collections) Object.assign(baseObj, domain.collections)
  })
}

const mergeMethodsCollections = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, domain]) => {
    if (domain.queries) {
      let buildQueries = queryBuilder(domain.queries)
      Object.assign(baseObj, collectionBuilder(buildQueries))
    }
    if (domain.mutations) {
      let buildMutations = mutationBuilder(domain.mutations)
      Object.assign(baseObj, collectionBuilder(buildMutations))
    }
  })
}

const mergeTypeMethods = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, domain]) => {
    if (domain.queries) {
      let buildQueries = queryBuilder(domain.queries)
      Object.assign(baseObj, methodTypesBuilder(buildQueries))
    }
    if (domain.mutations) {
      let buildMutations = mutationBuilder(domain.mutations)
      Object.assign(baseObj, methodTypesBuilder(buildMutations))
    }
  })
}

const mergeResolvers = (domains = {}, baseObj = {}) => {
  return Object.entries(domains).map(([key, rsv]) => {
    if (rsv.resolvers) {
      if (!baseObj[key]) baseObj[key] = {}
      baseObj[key] = Object.assign(baseObj[key], resolverBuilder(rsv.resolvers))
    }
  })
}

function serverNode({ schema, executableSchema } = {}, options = {}) {
  // , function_state = {}
  let {
    version = {},
    middlewares = [],
    context = {},
    resolvers = {},
    events = {},
    resources = {},
    domains = {},
    domainsList = [],
    utils = {},
    definitions = {},
    definitionsOmit = ['Query', 'Mutation'],
    defaultValues = {},
    defaultValuesMap = {},
    defaultValuesOmit = ['Query', 'Mutation'],
    methodsNames = {},
    methodsByDomain = {},
    inputTypeFilters = {},
    methodsArgumentTypes = {},
    ctxExtend = x => x
  } = options

  let eventsProviders = getOr({}, 'providers', events)
  let eventsDomains = getOr({}, 'domains', events)

  // console.time('buildingtime')
  let localResolvers = resolvers
  // mergeResolvers(domains, localResolvers)
  // domainsList.map(dl => mergeResolvers(dl, localResolvers));

  let localResources = baseResources

  let methodsQueryTypes = {}
  // mergeMethodsQueryTypes(domains, methodsQueryTypes)

  let mergedCollections = {}
  // mergeCollections(domains, mergedCollections)
  // domainsList.map(dl => mergeCollections(dl, mergedCollections));

  let mergedMethodsCollections = {}
  // mergeMethodsCollections(domains, mergedMethodsCollections)
  // domainsList.map(dl => mergeMethodsCollections(dl, mergedMethodsCollections));

  let mergedMethodTypesCollections = {}
  // mergeTypeMethods(domains, mergedMethodTypesCollections)
  // domainsList.map(dl => mergeTypeMethods(dl, mergedMethodTypesCollections));

  let mergedAlgoliaIndices = {}

  if (Object.keys(domains).length > 0) {
    domainsList.push(domains)
  }

  domainsList.map(dl => {
    mergeResolvers(dl, localResolvers)
    mergeCollections(dl, mergedCollections)
    mergeMethodsCollections(dl, mergedMethodsCollections)
    mergeTypeMethods(dl, mergedMethodTypesCollections)
    mergeMethodsQueryTypes(dl, methodsQueryTypes)
    mergeAlgoliaIndices(dl, mergedAlgoliaIndices)
  })

  // debug('methodsQueryTypes', methodsQueryTypes)

  // debug("mergedMethodTypesCollections", mergedMethodTypesCollections);

  // debug("mergedMethodsCollections", mergedMethodsCollections.resources);

  // debug("localResolvers", localResolvers);
  // debug("localResolvers", localResolvers.user.userCreate);

  defaultValues = omit(defaultValuesOmit, defaultValues)
  defaultValuesMap = omit(defaultValuesOmit, defaultValuesMap)
  definitions = omit(definitionsOmit, definitions)

  const memoizeDefaultValue = memoize(
    function (type) {
      return clone(readDefaultValuesMap(type, defaultValuesMap))
    },
    { length: 1 }
  )

  const memoizeDefinition = memoize(
    function (type) {
      let def = readDefinitions(type, definitions)
      return def.length > 0 ? `{${def}}` : undefined
    },
    { length: 1 }
  )

  const collectMethod = methodName => {
    let isQuery = get(`queries.${methodName}`, methodsArgumentTypes)
    let isMutation = get(`mutations.${methodName}`, methodsArgumentTypes)
    const methodArgs = isQuery || isMutation
    let methodType = get(methodName, mergedMethodTypesCollections)
    const fullDefinition = methodType ? memoizeDefinition(methodType) : undefined

    return {
      isQuery: !!isQuery,
      isMutation: !!isMutation,
      methodArgs,
      fullDefinition,
      argsFilter: args => argumentsFilter(clone(args), clone(methodArgs), clone(inputTypeFilters))
    }
  }

  // let { GRAPHQL_API_TOPIC_ARN, region } = getOr({}, 'aws', options)

  // const aws_events = awsEventsBuilder({ region, endpoint: process.env.IS_OFFLINE ? process.env.OFFLINE_SNS_URL : undefined, region: process.env.AWS_REGION, GRAPHQL_API_TOPIC_ARN })

  const context_data = (result = {}) => {
    let data = result.data
    if (data) {
      if (!is(Array, data)) return [data]
      else return data
    }
    return []
  }

  let ctx = { ...context }
  ctx.binding = new Binding({ schema })
  ctx = {
    ...ctx,
    uuid,
    cache: {
      resolvers: new ResolverCache(100)
    },
    schema: {
      version,
      typeMap: schema.getTypeMap()
    },
    domains: {
      list: domainsList
    },
    date: {
      ts(date) {
        if (!date) return
        // let d = moment(date)
        // return d.isValid() ? d.unix() : undefined
        try {
          return new Date(date).getTime() // 1000
        } catch (error) { }
      }
    },
    graphql: {
      defaultValues,
      defaultValuesMap,
      defaultValue: memoizeDefaultValue,
      definitions,
      definition: memoizeDefinition,
      methodsQueryTypes,
      methodsQueryType: methodName => get(methodName, methodsQueryTypes),
      methodTypes: mergedMethodTypesCollections,
      methodType: methodName => get(methodName, mergedMethodTypesCollections),
      collections: mergedCollections,
      methods_collections: mergedMethodsCollections,
      methods_collections_list: function () {
        return Object.entries(this.methods_collections || {}).reduce((list, [method, collections]) => {
          list.push({
            method,
            collections
          })
          return list
        }, [])
      },
      methodsNames,
      methodsByDomain,
      inputTypeFilters,
      collectMethod,
      methodsArgumentTypes,
      // dispatch: dispatchBuilder(function_state, { collectMethod, serverless: serverless(schema, options) }) //collectMethod
    },
    gm: graphqlMethods,
    namings,
    rsc: localResources,
    resources: localResources,
    rsv: localResolvers,
    resolvers: localResolvers,
    storage: {
      token: undefined,
      valid_token: false,
      user: {
        id: null
        // id: "3ce4efbb-b85d-433b-812e-59ef9bbb06b6"
      }
    },
    query: undefined,
    fauna: {
      client: null,
      queries: f.queries,
      methods: f.methods
    },
    q: f.queries.q,
    fq: f.queries,
    fm: f.methods,
    algolia: {
      indices: mergedAlgoliaIndices,
      indice(name) {
        return get(name, indices)
      },
      builders: a.builders
    },
    aws: {
      // GRAPHQL_API_TOPIC_ARN,
      // events: aws_events
    },
    context_event: {},
    events: {
      sync: new EventsManager({ providers: eventsProviders, domains: eventsDomains, ctx }),
      dispatch: eventsHelpers.dispatch,
      dispatchList: eventsHelpers.dispatchList
    },
    request: {
      start: new Date().getTime(),
      end: new Date().getTime()
    },
    utils: {
      ...baseUtils,
      ...utils,
      random_string: (length = 1) => {
        const letter = () =>
          Math.random()
            .toString(36)
            .replace(/\W/gim, '')
            .replace(/\d/gim, '')
            .substr(0, 1)

        return [...Array(length)]
          .map((v, index) => {
            let l = letter()
            return l
          })
          .join('')
      },
      forward: actionFn => items => {
        actionFn(clone(items))
        return items
      },
      async_forward: actionFn => async items => {
        await actionFn(clone(items))
        return items
      },
      context_data,
      context_data_ids: (result = {}, { ref_id_path, id_path } = {}) => {
        // console.log("result", result);
        return context_data(result).reduce((list, obj) => {
          if (get(ref_id_path, obj) && get(id_path, obj)) {
            // debug("log.ts", obj.log)
            list.push({ id: get(id_path, obj), ref_id: get(ref_id_path, obj), ts: get('log.ts', obj) })
          }
          return list
        }, [])
      }
    }
  }

  let serverObj = {
    schema,
    middlewares,
    context: async (request, reply) => {
      let headers = getOr({}, 'headers', request)
      let body = getOr({}, 'body', request)
      let user = body.user || {}
      // debug("headers", headers);
      let token = headers.Authorization || headers.authorization
      // debug('Authorization', token)
      if (is(String, token)) token = token.split('Bearer ')[1]
      ctx.storage.token = token
      let valid_token = false
      user = Object.assign({
        id: null,
        ref_id: null,
        rights: {
          roles_codes: [],
          roles: []
        },
        preferences: {
          language: 'en'
        }
      }, user)
      // debug('token', token)

      if (token) {
        valid_token = await ctx.binding.query.verify({}, null, { context: ctx })
        // debug('valid_token', valid_token)
        user = await ctx.binding.query.decode({}, null, { context: ctx }).then(getOr(user, 'user'))
        // debug('user', user)
      }

      // debug("user", user);

      ctx.storage.user = user
      ctx.storage.valid_token = valid_token
      ctx.fauna.client = f.methods.clientCreate(process.env.FAUNA_KEY)
      ctx.query = q => {
        const hash = md5(JSON.stringify(q))
        const mark = `ctx.query_${hash}_${new Date().getTime()}`
        // console.time(mark)
        let markDuration = new Date().getTime() / 1000
        debug(`Started ${mark} at ${new Date().toISOString()}`)
        return ctx.fauna.client.query
          .call(ctx.fauna.client, q)
          .then(
            ctx.utils.forward(() => {
              markDuration = new Date().getTime() / 1000 - markDuration
              debug(mark, `${markDuration}ms`)
              // console.timeEnd(mark)
              // debug('Query', JSON.stringify(q, null, 2))
            })
          )
          .catch(err => {
            console.timeEnd(mark)
            console.error('Error on Fauna Query')
            try {
              // debug('Query', JSON.stringify(q, null, 2))
            } catch (error) { }
            console.error(err)
          })
      }

      ctx = ctxExtend(ctx)

      return ctx
    }
  }

  return serverObj
}



module.exports = { serverNode }
