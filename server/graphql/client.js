require("isomorphic-fetch");
const { createHttpLink } = require("apollo-link-http");
const { makeRemoteExecutableSchema, introspectSchema } = require("graphql-tools");
const { Binding } = require("graphql-binding");
const ApolloClient = require("apollo-client").default;
const { InMemoryCache } = require("apollo-cache-inmemory");
const { onError } = require("apollo-link-error");
const { ApolloLink } = require("apollo-link");

// const { getIntrospectionSchema } = require("./schema");

const generateClient = async ({ endpointUrl, schema, headers = {}, credentials } = {}) => {
  const endpointLink = () =>
    createHttpLink({
      uri: endpointUrl,
      headers,
      credentials,
      fetch
    });
  let fetchedSchema = await introspectSchema(endpointLink());
  // console.log("fetchedSchema");
  const localSchema = schema || fetchedSchema;
  let remoteSchema = makeRemoteExecutableSchema({ link: endpointLink(), schema: localSchema });
  let b = new Binding({ schema: remoteSchema });
  return b;
};

const apolloClient = url => {
  const httpLink = createHttpLink({
    uri: url
  });

  const client = new ApolloClient({
    link: ApolloLink.from([httpLink]),
    queryTransformer: ApolloClient.addTypename,
    cache: new InMemoryCache({
      dataIdFromObject: object => null
    }),
    defaultOptions: {
      watchQuery: {
        fetchPolicy: "no-cache",
        errorPolicy: "ignore"
      },
      query: {
        fetchPolicy: "no-cache",
        errorPolicy: "all"
      },
      mutate: {
        errorPolicy: "all"
      }
    }
  });

  return client;
};

module.exports = { generateClient, apolloClient };
