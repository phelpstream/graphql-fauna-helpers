const Debug = require('debug')
const debug = Debug('gfh-server [graphql:filters:arguments]')

const { is } = require('ramda')
const { getOr, get, omitList, clone } = require('svpr')

const argumentsFilter = function(args, argumentsSignature = {}, typeFilters) {
  console.time('filter_arguments')
  let filteredArgs = {}

  try {
    filteredArgs = Object.entries(args).reduce((obj, [key, value]) => {
      let type = argumentsSignature[key]
      let hasValueToFilter = !!value
      // debug('hasValueToFilter', hasValueToFilter, 'type', type)
      if (type && hasValueToFilter) {
        let cleanType = type.replace(/\W/gim, '').replace('Input', '')
        let filters = getOr([], cleanType, typeFilters)
        // debug('filters', filters)
        // let isArray = type.includes("[")
        let isReallyArray = is(Array, value)
        // debug("cleanType", cleanType, "filters", filters, "isReallyArray", isReallyArray);
        if (filters && filters.length > 0) {
          if (isReallyArray) {
            obj[key] = value.map(omitList(filters))
          } else {
            obj[key] = omitList(filters, clone(value))
          }
        }
      }
      if (value === undefined) {
        // debug('key', key, 'value', value)
        obj[key] = null
      }
      return obj
    }, clone(args))

    // debug('Filtered args', filteredArgs)
  } catch (error) {
    debug('Error on filteredArgs', error)
  }

  console.timeEnd('filter_arguments')
  // const argsLength = JSON.stringify(args).length;
  // const filteredArgsLength = JSON.stringify(filteredArgs).length;

  // debug("Filtered args from", argsLength, "to", filteredArgsLength);

  return filteredArgs
}

module.exports = argumentsFilter
