const { graphql } = require("graphql");
const { is } = require("ramda");
const { get, getOr } = require("svpr");

function graphqlServer(schema, context = event => {}, event = {}, cb = (error, result) => {}) {
  let body = getOr({}, "body", event);
  if (is(String, body)) {
    try {
      body = JSON.parse(body);
    } catch (error) {}
  }
  let { query, variables } = body;
  // patch to allow queries from GraphiQL
  // like the initial introspectionQuery
  if (is(String, body.query)) {
    query = body.query.replace("\n", " ", "g");
  }

  return graphql(schema, query, {}, context(event), variables)
    .then(result => cb(null, result))
    .catch(cb);
}

module.exports = graphqlServer;
