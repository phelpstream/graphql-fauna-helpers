require("isomorphic-fetch");
const { createHttpLink } = require("apollo-link-http");
const { introspectSchema, makeExecutableSchema } = require("graphql-tools");

const { omit } = require("ramda");

const graphql = require("graphql");
const { printSchema } = require("graphql/utilities");

const { genericBuilder, mutationBuilder, queryBuilder } = require("./builders");
// console.log("genericBuilder", genericBuilder, mutationBuilder, queryBuilder);

const getIntrospectionSchema = async endpointUrl => {
  const endpointLink = () =>
    createHttpLink({
      uri: endpointUrl,
      fetch
    });
  const localSchema = await introspectSchema(endpointLink());
  return localSchema;
};

const Schema = ({ queries = [], mutations = [], domains = {}, domainsList = [] } = {}) => {
  const mergeItems = (items = [], initObj = {}) => items.reduce((obj, i = {}) => Object.assign(obj, omit(["_collections"], genericBuilder(i))), initObj);

  let localDomainsList = [];

  localDomainsList.push(...Object.values(domains));
  localDomainsList.push(
    ...domainsList.reduce((list, d) => {
      list.push(...Object.values(d));
      return list;
    }, [])
  );

  let queriesObj = mergeItems(queries);
  let mutationsObj = mergeItems(mutations);

  // if (Object.keys(domains).length > 0) {
  let domainsQueries = localDomainsList.reduce((list, d) => {
    if (d.queries) list.push(d.queries);
    return list;
  }, []);
  let domainsMutations = localDomainsList.reduce((list, d) => {
    if (d.mutations) list.push(d.mutations);
    return list;
  }, []);
  queriesObj = mergeItems(domainsQueries, queriesObj);
  mutationsObj = mergeItems(domainsMutations, mutationsObj);
  // }

  // console.log("queries", queriesObj);
  // console.log("mutations", mutationsObj);

  let schemaDef = {};
  if (Object.keys(queriesObj).length > 0) {
    schemaDef.query = new graphql.GraphQLObjectType({
      name: "Query",
      fields: () => queriesObj
    });
  }
  if (Object.keys(mutationsObj).length > 0) {
    schemaDef.mutation = new graphql.GraphQLObjectType({
      name: "Mutation",
      fields: () => mutationsObj
    });
  }


  // console.log("queriesObj", JSON.stringify(queriesObj, null, 2));
  // console.log("mutationsObj", JSON.stringify(mutationsObj, null, 2));
  // console.log("schemaDef", JSON.stringify(schemaDef, null, 2));

  return new graphql.GraphQLSchema(schemaDef);
};

function ExecutableSchema({ queries = [], mutations = [], domains = {}, domainsList = [] } = {}) {
  let schema = Schema({ queries, mutations, domains, domainsList })
  let typeDefs = printSchema(schema)
  console.log("typeDefs", typeDefs);
  let resolvers = {}

  const mergeItems = (items = [], initObj = {}) => items.reduce((obj, i = {}) => Object.assign(obj, omit(["_collections"], genericBuilder(i))), initObj);

  let localDomainsList = [];

  localDomainsList.push(...Object.values(domains));
  localDomainsList.push(
    ...domainsList.reduce((list, d) => {
      list.push(...Object.values(d));
      return list;
    }, [])
  );

  let queriesObj = mergeItems(queries);
  let mutationsObj = mergeItems(mutations);

  let domainsQueries = localDomainsList.reduce((list, d) => {
    if (d.queries) list.push(d.queries);
    return list;
  }, []);
  let domainsMutations = localDomainsList.reduce((list, d) => {
    if (d.mutations) list.push(d.mutations);
    return list;
  }, []);
  queriesObj = mergeItems(domainsQueries, queriesObj);
  mutationsObj = mergeItems(domainsMutations, mutationsObj);


  if (Object.keys(queriesObj).length > 0) {
    resolvers.Query = Object.entries(queriesObj).reduce((Query, [key, obj]) => {
      Query[key] = obj.resolve
      return Query
    }, {})
  }
  if (Object.keys(mutationsObj).length > 0) {
    resolvers.Mutation = Object.entries(mutationsObj).reduce((Mutation, [key, obj]) => {
      Mutation[key] = obj.resolve
      return Mutation
    }, {})
  }


  return makeExecutableSchema({ typeDefs, resolvers })
}

module.exports = { Schema, ExecutableSchema, getIntrospectionSchema };
