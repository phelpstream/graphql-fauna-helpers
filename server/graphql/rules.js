const { isNil } = require("ramda");
const { get, rejectDeep } = require("svpr");
const { rule, shield, and, or, not } = require("graphql-shield");

const fieldEqual = (field, ruleA, checkB) => ruleA[field] === checkB[field];

module.exports = {
  // isAuthed: rule()(async (parent, args, ctx, info) => {
  //   return get("storage.user", ctx) !== null;
  // }),
  isRole: ({ role }) => {
    return rule()(async (parent, args, ctx, info) => {
      const userRoles = getOr([], "storage.user.rights.roles", ctx);
      return userRoles.inclues(role);
    });
  },
  hasPermission: ({ role, scope, resource, action, zone } = {}) => {
    return rule()(async (parent, args, ctx, info) => {
      let ruleA = { role, scope, resource, action, zone };
      ruleA = rejectDeep(isNil, ruleA);
      const permissions = getOr([], "storage.user.rights.permissions", ctx);
      return !!permissions.find(p => Object.keys(ruleA).every(key => fieldEqual(key, ruleA, p)));
    });
  }
};
