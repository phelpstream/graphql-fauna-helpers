require("isomorphic-fetch");
const { createHttpLink } = require("apollo-link-http");
const { makeRemoteExecutableSchema, introspectSchema } = require("graphql-tools");
const { Binding } = require("graphql-binding");

module.exports = {
  local: Binding,
  remote: async ({  url }) => {
    const endpointLink = () => {
      return createHttpLink({
        uri: url,
        fetch
      });
    }
    const schema = await introspectSchema(endpointLink());
    let remoteSchema = makeRemoteExecutableSchema({ link: endpointLink(), schema });
    return new Binding({ schema: remoteSchema });
  }
}
