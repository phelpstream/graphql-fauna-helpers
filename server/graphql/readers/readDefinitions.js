const { getOr } = require('svpr')

const readDefinitions = (type, definitions) => {
	let base = getOr('', type, definitions)

	const hasMissingTags = () => (base.match(new RegExp('##', 'g')) || []).length

	const updateFieldWithTypes = () => {
		for (let [defType, definition] of Object.entries(definitions)) {
			if (defType !== type) {
				base = base.replace(new RegExp(`##${defType}##`, 'g'), definition)
			}
		}
	}

	let counter = 0
	while (hasMissingTags() && counter < 30) {
		updateFieldWithTypes()
		counter++
	}

	return base
}

module.exports = readDefinitions
