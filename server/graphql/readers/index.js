module.exports = {
	readDefaultValuesMap: require('./readDefaultValuesMap'),
	readDefinitions: require('./readDefinitions')
}
