const { is } = require('ramda')
const { has, getOr, get, dig, clone } = require('svpr')

const readDefaultValuesMap = (type, defaultValuesMap) => {
  let base = getOr({}, type, defaultValuesMap)
  if (Object.keys(base).length < 1) return undefined
  return dig(value => {
    if (is(Object, value) && has('$RECURSIVE_TYPE', value)) {
      let def = get('$RECURSIVE_TYPE', value)
      let next = clone(get(def.type, defaultValuesMap))
      return def.array ? [next] : next
    }
    return value
  }, base)
}

module.exports = readDefaultValuesMap
