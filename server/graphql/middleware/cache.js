const Debug = require('debug')
const debug = Debug('gfh-server [graphql:middleware:cache]')

const { get, normalize } = require('svpr')
const md5 = require('md5')

const cacheMiddleware = async (resolve, source, args, ctx, info) => {
  let cacheInput = { action: info.fieldName, args }
  let normalizedCacheInput, hash
  let isRegisteredQuery = !!ctx.graphql.methodsQueryType(info.fieldName)
  let canBeCached = isRegisteredQuery && Object.keys(args).length > 0
  if (canBeCached) {
    // console.time('normalizeCacheInput')
    normalizedCacheInput = normalize(cacheInput)
    hash = md5(JSON.stringify(normalizedCacheInput))
    // console.timeEnd('normalizeCacheInput')
  }

  if (canBeCached) {
    debug('[CACHE MIDDLEWARE]', info.fieldName, hash)
  }
  let res
  // Caching only Query
  if (canBeCached) {
    if (ctx.cache.resolvers.has(hash)) {
      res = await ctx.cache.resolvers.get(hash)
    }
  }

  if (!res) {
    try {
      if (canBeCached) {
        ctx.cache.resolvers.set(hash, resolve(source, args, ctx, info))
        res = await ctx.cache.resolvers.get(hash)
      } else {
        // debug('requesting:', info.fieldName, 'type', info.returnType)
        return resolve(source, args, ctx, info)
      }
    } catch (error) {
      throw error
    }
  } else {
    // debug('SKIP ACTUAL FETCH OF', normalizedCacheInput)
  }

  return res
}

module.exports = cacheMiddleware
