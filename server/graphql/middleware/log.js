const Debug = require('debug')
const debug = Debug('gfh-server [graphql:middleware:log]')

const { get } = require('svpr')

const logMiddleware = async (resolve, source, args, ctx, info) => {
  let log = { action: info.fieldName, args }
  let res = undefined
  let err = null
  try {
    // debug('[LOG MIDDLEWARE]', info.fieldName)
    res = await resolve(source, args, ctx, info)
    log.ref = get('log._ref', res)
    log.ts = get('log._ts', res)
  } catch (error) {
    log.is_error = true
    log.ts = new Date().getTime() * 1000
    // debug('log', log)
    debug("error", error, log)
    throw error
  } finally {
    if (!log.action.startsWith('log') && ((log.action && log.ref) || log.is_error)) {
      // debug("log", log);
      let hasLogFn = get('rsv.log.logCreate', ctx)
      if (ctx.query && hasLogFn) {
        // Not blocking
        ctx.rsv.log
          .logCreate({ input: log, skipEvent: true }, ctx)
          // .then(res => debug(res))
          .catch(debug)
      }
    }
    return res
  }
}

module.exports = logMiddleware
