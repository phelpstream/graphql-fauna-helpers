const Debug = require('debug')
const debug = Debug('gfh-server [graphql:middleware:defaults]')

const { get, clone } = require('svpr')
const { is, mergeDeepWith, isNil } = require('ramda')

const completeWithDefaultValue = (value, defaultValue) => {
  return mergeDeepWith((a, b) => {
    if (is(Array, b) && is(Array, a) && b.length > a.length) return b
    return isNil(a) ? b : a
  })(clone(value) || {}, clone(defaultValue))
}

const defaultsMiddleware = async (resolve, source, args, ctx, info) => {
  let isRootQuery = `${info.parentType}` === 'Query'
  let isRegisteredQuery = !!ctx.graphql.methodsQueryType(info.fieldName)
  if (isRootQuery && isRegisteredQuery) {
    let res
    try {
      res = await resolve(source, args, ctx, info)
      let methodType = ctx.graphql.methodType(info.fieldName)
      // debug('methodType', methodType)
      if (methodType) {
        debug('methodName', info.fieldName)
        let defaultValue = ctx.graphql.defaultValue(methodType)
        // debug('defaultValue found?', !!defaultValue)
        if (defaultValue) {
          const getFlights = get('data.flight.route.flights')

          // console.log(defaultValue)
          // console.log(res)
          let merged = completeWithDefaultValue(res, defaultValue)
          // console.log(merged)
          // await new Promise(resolve => setTimeout(resolve, 15000))

          return merged
        }
      }
    } catch (error) {
      throw error
    }

    return res
  }
  return resolve(source, args, ctx, info)
}

module.exports = defaultsMiddleware
