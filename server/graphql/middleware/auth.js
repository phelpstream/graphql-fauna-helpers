const Debug = require('debug')
const debug = Debug('gfh-server [graphql:middleware:auth]')

const { is } = require('ramda')
const { get, getOr } = require('svpr')

const authMiddleware = async (resolve, source, args, ctx, info) => {
  // debug('user_id', ctx.storage.user.id)
  // debug('ctx.storage', ctx.storage)

  if (!ctx.storage.token) {
    throw 'No valid token'
  } else {
    let res = await resolve(source, args, ctx, info)
    return res
  }
}

module.exports = authMiddleware
