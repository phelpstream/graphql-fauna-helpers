const { get, getOr } = require("svpr");
const { sentry } = require("graphql-middleware-sentry");

const sentryMiddleware = (SENTRY_DSN, ENV) => {
  return sentry({
    config: {
      dsn: SENTRY_DSN,
      environment: ENV
    },
    withScope: (scope, error, context) => {
      scope.setUser(getOr({}, "storage.user", context));
    }
  });
};

module.exports = sentryMiddleware;
