const { is } = require("ramda");
const { rule, shield, and, or, not } = require("graphql-shield");
const mergeObjList = require("./../utils/mergeObjList");
const rules = require("./rules");

module.exports = {
  rules,
  middleware: (resolve, root, args, ctx, info) => {

    // const roles = await ctx.binding.roles()



    // let processedFirwalls = firewalls.map(fw => (is(Function, fw) ? fw({ rules, isRole: rules.isRole, hasPermission: rules.hasPermission, rule, and, or, not }) : fw));
    // let queries = processedFirwalls.reduce((list, pfw) => {
    //   if (pfw.queries) list.push(pfw.queries);
    //   return list;
    // }, []);
    // let mutations = processedFirwalls.reduce((list, pfw) => {
    //   if (pfw.mutations) list.push(pfw.mutations);
    //   return list;
    // }, []);

    // return shield({
    //   Query: mergeObjList(queries),
    //   Mutation: mergeObjList(mutations)
    // });

    return resolve(root, args, ctx, info);
  }
};
