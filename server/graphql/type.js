const Debug = require('debug')
const debug = Debug('gfh-server [graphql:type]')

const { omit, pick, is } = require('ramda')
const { eachFlip, get, getOr } = require('svpr')

const graphql = require('graphql')

const arrayToValueObject = require('./../utils/arrayToValueObject')

const builder = (instanceConstructor, { secondArgIs = 'fields', modifier = (name, secondArg, options) => ({ name, secondArg, options }) } = {}) => {
  return (name, secondArg, options) => {
    let buildData = modifier(name, secondArg, options) // mutate
    let extraProperties = {}
    if (secondArgIs === 'fields') {
      extraProperties = Object.assign(extraProperties, { fields: () => buildData.secondArg })
    } else if (secondArgIs === 'values') {
      extraProperties = Object.assign(extraProperties, { values: buildData.secondArg })
    } else {
      extraProperties = buildData.secondArg
    }

    let o = { name: buildData.name, ...extraProperties, ...buildData.options }
    return new instanceConstructor(o)
  }
}

const typeObject = (name, fields = {}) => {
  return new graphql.GraphQLObjectType({ name, fields: () => fields })
}

const typeFullInput = builder(graphql.GraphQLInputObjectType, {
  modifier: (name, secondArg, options) => ({
    name: `${name}FullInput`,
    secondArg,
    options
  })
})

const typeInput = builder(graphql.GraphQLInputObjectType, {
  modifier: (name, secondArg, options) => ({
    name: `${name}Input`,
    secondArg,
    options
  })
})

const typeDef = (type, options = {}) => {
  let validate = get('validate', options)
  let res = { type, defaultValue: get('default', options), validate, ...omit(['default', 'validate'], options) }
  return res
}

const typeList = type => new graphql.GraphQLList(type)

module.exports = {
  Enum: builder(graphql.GraphQLEnumType, {
    secondArgIs: 'values',
    modifier: function(name, values, options) {
      return {
        name: `${name}Enum`,
        secondArg: arrayToValueObject(values),
        options
      }
    }
  }),
  Input: typeInput,
  FullInput: typeFullInput,
  Object: typeObject,
  Entity: function(name, { objectDefinition, inputDefinition, fullInputDefinition, instanceDefaultType, contextType, requestsType, errorsType, eventsType, countType, namings, ArrayLikeItem } = {}) {
    const type = typeObject(name, objectDefinition)
    const input = typeInput(name, inputDefinition)
    const arraylike = ArrayLikeItem ? ArrayLikeItem(name, { type, input }) : {}
    const full_input = typeFullInput(name, fullInputDefinition)
    const in_context_default = typeObject(namings.inContextDataDefault(name), {
      data: typeDef(instanceDefaultType),
      context: typeDef(contextType),
      requests: typeDef(requestsType),
      events: typeDef(eventsType),
      errors: typeDef(errorsType)
    })
    const in_context_count = typeObject(namings.inContextDataCount(name), {
      data: typeDef(typeList(countType)),
      context: typeDef(contextType),
      requests: typeDef(requestsType),
      events: typeDef(eventsType),
      errors: typeDef(errorsType)
    })
    const in_context_exists = typeObject(namings.inContextDataExists(name), {
      data: typeDef(graphql.GraphQLBoolean, { default: false }),
      context: typeDef(contextType),
      requests: typeDef(requestsType),
      events: typeDef(eventsType),
      errors: typeDef(errorsType)
    })
    const in_context_simple = typeObject(namings.inContextDataSimple(name), {
      data: typeDef(type),
      context: typeDef(contextType),
      requests: typeDef(requestsType),
      events: typeDef(eventsType),
      errors: typeDef(errorsType)
    })
    const in_context_list = typeObject(namings.inContextDataList(name), {
      data: typeDef(typeList(type)),
      context: typeDef(contextType),
      requests: typeDef(requestsType),
      events: typeDef(eventsType),
      errors: typeDef(errorsType)
    })

    let buildEntity = { type, input, arraylike, full_input, in_context_simple, in_context_exists, in_context_count, in_context_list, in_context_default }
    return buildEntity
  },
  // Object: builder(graphql.GraphQLObjectType),
  Interface: builder(graphql.GraphQLInterfaceType),
  List: typeList,
  NonNull: type => new graphql.GraphQLNonNull(type),
  Scalar: builder(graphql.GraphQLScalarType),
  Union: builder(graphql.GraphQLUnionType),
  Def: typeDef,
  DefExtend: (...defs) => defs.reduce((extendObj, def = {}) => Object.assign(extendObj, def), {})
}
