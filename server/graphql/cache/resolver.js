const { get, has } = require('svpr')
// const lru = require('lru-cache')

module.exports = function ResolverCache(options) {
  // const maxItems = options && options.maxItems
  // this.cache = new lru(maxItems)
  this.cache = {}

  this.has = key => {
    return has(key, this.cache)
    // return this.cache.get(key)
  }

  this.get = key => {
    return get(key, this.cache)
    // return this.cache.get(key)
  }

  this.set = (key, value) => {
    return (this.cache[key] = value)
  }

  return this
}
