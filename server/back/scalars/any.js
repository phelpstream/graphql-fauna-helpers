const { GraphQLScalarType } = require("graphql");

const Any = new GraphQLScalarType({
  name: "Any",
  description: "Any type",
  serialize(value) {
    return value;
  },
  parseValue(value) {
    return value;
  },
  parseLiteral(ast) {
    return ast.value;
  }
});

module.exports = Any;
