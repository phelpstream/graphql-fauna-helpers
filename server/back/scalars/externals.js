const graphql = require("graphql");
const GraphQLJSON = require("graphql-type-json");
const { GraphQLDate, GraphQLTime, GraphQLDateTime } = require("graphql-iso-date");
const GraphQLLong = require("graphql-type-long");
const {
  NonPositiveInt,
  PositiveInt,
  NonNegativeInt,
  NegativeInt,
  NonPositiveFloat,
  PositiveFloat,
  NonNegativeFloat,
  NegativeFloat,
  EmailAddress,
  URL,
  PhoneNumber,
  PostalCode
} = require("@okgrow/graphql-scalars");

module.exports = {
  // ID: graphql.GraphQLID,
  ID: graphql.GraphQLString,
  REF_ID: graphql.GraphQLString,
  String: graphql.GraphQLString,
  Boolean: graphql.GraphQLBoolean,
  Float: graphql.GraphQLFloat,
  Int: graphql.GraphQLInt,
  JSON: GraphQLJSON,
  Long: GraphQLLong,
  Date: GraphQLDate,
  Time: GraphQLTime,
  DateTime: GraphQLDateTime,
  NonPositiveInt,
  PositiveInt,
  NonNegativeInt,
  NegativeInt,
  NonPositiveFloat,
  PositiveFloat,
  NonNegativeFloat,
  NegativeFloat,
  EmailAddress,
  URL,
  PhoneNumber,
  PostalCode
};
