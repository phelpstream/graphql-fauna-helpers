// const g = require("../../graphql");
// const s = g.scalar;
// const t = g.type;
const { GraphQLScalarType } = require("graphql");

const Joi = require("joi");

const schema = Joi.object().keys({
  "@ref": Joi.object().keys({
    id: Joi.string().required(),
    class: Joi.object().keys({
      "@ref": Joi.object()
        .keys({
          id: Joi.string().required()
        })
        .required(),
      class: Joi.object().keys({
        "@ref": Joi.object().keys({
          id: Joi.string()
        })
      })
    })
  })

  // database: Joi.object().keys({
  //   id: Joi.string().required()
  // })
});

const parseFaunRef = fref => {
  try {
    return JSON.parse(JSON.stringify(fref));
  } catch (error) {}
  return fref;
};

const FaunaRef = new GraphQLScalarType({
  name: "FaunaRef",
  description: "Fauna Reference object",
  serialize(value) {
    value = parseFaunRef(value);
    // console.log("faunaref", JSON.stringify(value, null, 2));
    const test = Joi.validate(value, schema);
    if (test.error !== null) {
      throw new TypeError(`Validation failed for ${value} with: ${test.error}`);
    }
    return value;
  },
  parseValue(value) {
    value = parseFaunRef(value);
    // console.log("faunaref", JSON.stringify(value, null, 2));
    const test = Joi.validate(value, schema);
    if (test.error !== null) {
      throw new TypeError(`Validation failed for ${value} with: ${test.error}`);
    }
    return value;
  },
  parseLiteral(ast) {
    let value = parseFaunRef(ast.value);
    // console.log("faunaref", JSON.stringify(value, null, 2));
    const test = Joi.validate(value, schema);
    if (test.error !== null) {
      throw new TypeError(`Validation failed for ${value} with: ${test.error}`);
    }
    return value;
  }
});

module.exports = FaunaRef;
