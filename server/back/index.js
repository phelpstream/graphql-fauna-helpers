module.exports = {
  domains: require("./domains"),
  resolvers: require("./resolvers"),
  context: require("./context"),
  entities: require("./entities"),
  indexes: require("./indexes"),
  enums: require("./enums"),
  scalars: require("./scalars"),
  queries: require("./queries"),
  mutations: require("./mutations"),
  resources: require("./resources"),
  namings: require("./namings")
};
