const Debug = require('debug')
const debug = Debug('gfh-server [back:mutations:generators:index]')

const { get } = require('svpr')

const s = require('./../../scalars')
const t = require('./../../../graphql/type')

const namings = require('./../../namings')

const InstanceIdentity = require('./../../entities/instance_identity')
const PaginateOptions = require('./../../entities/paginate_options')

const setCollection = (collections, instanceName) => {
  return get(instanceName, collections) || collections
}

const instanceDefault = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceDefault(instanceName)]: {
    type: entity.in_context_default,
    args: {
      typeName: t.Def(s.String),
      ...additionalArgs
    },
    resolve: async function(source, { typeName }, ctx, info) {
      return ctx.rsv[domain][namings.instanceDefault(instanceName)]({ typeName: typeName || entity.input }, ctx, info)
    },
    _collections: {
      instanceDefaults: {
        instanceName: 'instanceDefaults',
        key: 'type'
      }
    }
  }
})

const instanceById = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceById(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      id: t.Def(t.NonNull(s.ID)),
      ...additionalArgs
    },
    resolve: async function(source, { id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceById(instanceName)]({ id }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceSync = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceSync(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(t.NonNull(s.ID)),
      ...additionalArgs
    },
    resolve: async function(source, { ref_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceSync(instanceName)]({ ref_id, typeName: entity.in_context_simple.name }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceSyncById = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceSyncById(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      id: t.Def(t.NonNull(s.ID)),
      ...additionalArgs
    },
    resolve: async function(source, { id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceSyncById(instanceName)]({ id, typeName: entity.in_context_simple.name }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceMatch = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceMatch(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      index: t.Def(t.NonNull(s.String)),
      value: t.Def(s.Any),
      values: t.Def(t.List(s.Any)),
      ...additionalArgs
    },
    resolve: async function(source, { index, value, values }, ctx, info) {
      return ctx.rsv[domain][namings.instanceMatch(instanceName)]({ index, value, values }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instance = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instance(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.String),
      ids: t.Def(InstanceIdentity.input),
      ...additionalArgs
    },
    resolve: async function(source, { ref_id, ids }, ctx, info) {
      return ctx.rsv[domain][namings.instance(instanceName)]({ ref_id, ids }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instances = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instances(instanceName)]: {
    type: entity.in_context_list,
    args: {
      index: t.Def(s.String),
      matchValue: t.Def(s.Any),
      paginateOptions: t.Def(PaginateOptions.input),
      ...additionalArgs
    },
    resolve: async function(source, { index, matchValue, paginateOptions }, ctx, info) {
      return ctx.rsv[domain][namings.instances(instanceName)]({ index, matchValue, paginateOptions }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instancesSync = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instancesSync(instanceName)]: {
    type: entity.in_context_list,
    args: {
      index: t.Def(s.String),
      matchValue: t.Def(s.Any),
      paginateOptions: t.Def(PaginateOptions.input),
      ...additionalArgs
    },
    resolve: async function(source, { index, matchValue, paginateOptions }, ctx, info) {
      return ctx.rsv[domain][namings.instancesSync(instanceName)]({ index, matchValue, paginateOptions, typeName: entity.in_context_list.name }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instancesList = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instancesList(instanceName)]: {
    type: entity.in_context_list,
    args: {
      // values: t.Def(t.List(s.Any)),
      ref_ids: t.Def(t.List(s.String)),
      idsList: t.Def(t.List(InstanceIdentity.arraylike.input)),
      ...additionalArgs
    },
    resolve: async function(source, { ref_ids, idsList }, ctx, info) {
      return ctx.rsv[domain][namings.instancesList(instanceName)]({ ref_ids, idsList }, ctx, info) //, matchValues: values
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instancesLast = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instancesLast(instanceName)]: {
    type: entity.in_context_list,
    args: {
      size: t.Def(s.Int),
      ...additionalArgs
    },
    resolve: async function(source, { size }, ctx, info) {
      return ctx.rsv[domain][namings.instancesLast(instanceName)]({ size }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceOwnMatch = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceOwnMatch(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      index: t.Def(t.NonNull(s.String)),
      value: t.Def(s.Any),
      values: t.Def(t.List(s.Any)),
      user_id: t.Def(s.ID),
      ...additionalArgs
    },
    resolve: async function(source, { index, value, values, user_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceOwnMatch(instanceName)]({ index, value, values, user_id }, ctx, info)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceOwn = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceOwn(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      user_id: t.Def(s.ID),
      ...additionalArgs
    },
    resolve: async function(source, { user_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceOwn(instanceName)]({ user_id }, ctx, info)
    },
    _collections: setCollection(collections, 'instancesOwn')
  }
})

const instanceOwnById = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceOwnById(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      id: t.Def(t.NonNull(s.ID)),
      user_id: t.Def(s.ID),
      ...additionalArgs
    },
    resolve: async function(source, { id, user_id }, ctx, info) {
      user_id = user_id || ctx.storage.user.id
      return ctx.rsv[domain][namings.instanceOwnById(instanceName)]({ id, user_id }, ctx, info)
    },
    _collections: setCollection(collections, 'instancesOwn')
  }
})

const instancesOwn = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instancesOwn(instanceName)]: {
    type: entity.in_context_list,
    args: {
      user_id: t.Def(s.ID),
      paginateOptions: t.Def(PaginateOptions.input),
      ...additionalArgs
    },
    resolve: async function(source, { user_id, paginateOptions }, ctx, info) {
      return ctx.rsv[domain][namings.instancesOwn(instanceName)]({ user_id, paginateOptions }, ctx, info)
    },
    _collections: setCollection(collections, 'instancesOwn')
  }
})

const instanceExists = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceExists(instanceName)]: {
    type: entity.in_context_exists,
    args: {
      ref_id: t.Def(t.NonNull(s.String)),
      ...additionalArgs
    },
    resolve: async function(source, { ref_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceExists(instanceName)]({ ref_id }, ctx, info)
    },
    _collections: {}
  }
})

const instanceByIdExists = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instanceByIdExists(instanceName)]: {
    type: entity.in_context_exists,
    args: {
      id: t.Def(t.NonNull(s.ID)),
      ...additionalArgs
    },
    resolve: async function(source, { id }, ctx, info) {
      // debug("id", id)
      return ctx.rsv[domain][namings.instanceByIdExists(instanceName)]({ id }, ctx, info)
    },
    _collections: {}
  }
})

const instancesCount = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instancesCount(instanceName)]: {
    type: entity.in_context_count,
    args: {
      index: t.Def(s.String),
      value: t.Def(s.Any),
      values: t.Def(t.List(s.Any)),
      ...additionalArgs
    },
    resolve: async function(source, { index, value, values }, ctx, info) {
      return ctx.rsv[domain][namings.instancesCount(instanceName)]({ index, value, values }, ctx, info)
    },
    _collections: {
      indexCounts: {
        instanceName: 'indexCounts',
        key: 'index'
      }
    }
  }
})

const instancesOwnCount = ({ instanceName, entity, domain, collections, additionalArgs }) => ({
  [namings.instancesOwnCount(instanceName)]: {
    type: entity.in_context_count,
    args: {
      user_id: t.Def(s.ID),
      ...additionalArgs
    },
    resolve: async function(source, { user_id }, ctx, info) {
      user_id = user_id || ctx.storage.user.id
      return ctx.rsv[domain][namings.instancesOwnCount(instanceName)]({ user_id }, ctx, info)
    },
    _collections: {
      indexCounts: {
        instanceName: 'indexCounts',
        key: 'index'
      }
    }
  }
})

const R = ({ instanceName, entity, domain, collections, additionalArgs = {}, includeOwn = false }) => {
  if (!collections) {
    collections = {
      [`${instanceName}s`]: {
        key: 'id'
      }
    }
  }

  // let additionalArgs = additionalArgsFn({ t, s })
  let ownFns = [instanceOwnMatch, instanceOwn, instanceOwnById, instancesOwn, instancesOwnCount]

  // let ownFns = {
  //   ...instanceOwnMatch({ instanceName, entity, domain, collections, additionalArgs }),
  //   ...instanceOwn({ instanceName, entity, domain, collections, additionalArgs }),
  //   ...instanceOwnById({ instanceName, entity, domain, collections, additionalArgs }),
  //   ...instancesOwn({ instanceName, entity, domain, collections, additionalArgs }),
  //   ...instancesOwnCount({ instanceName, entity, domain, collections, additionalArgs })
  // }

  let fns = [
    instanceDefault,
    instanceById,
    instanceMatch,
    instance,
    instanceSync,
    instanceSyncById,
    instances,
    instancesSync,
    instancesList,
    instancesLast,
    instanceExists,
    instanceByIdExists,
    instancesCount
  ]

  return [...fns, ...ownFns].reduce((rsv, fn) => {
    return { ...fn({ instanceName, entity, domain, collections, additionalArgs }), ...rsv }
  }, {})
  // {
  // ...instanceDefault({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instanceById({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instanceMatch({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instance({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instanceSyncById({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instances({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instancesSync({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instancesList({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instancesLast({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instanceExists({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instanceByIdExists({ instanceName, entity, domain, collections, additionalArgs }),
  // ...instancesCount({ instanceName, entity, domain, collections, additionalArgs }),
  // ...(includeOwn ? ownFns : {})
  // }
}

module.exports = {
  R,
  instanceDefault,
  instanceSync,
  instanceById,
  instanceSyncById,
  instanceMatch,
  instance,
  instances,
  instancesSync,
  instancesList,
  instancesLast,
  instanceOwnMatch,
  instanceOwn,
  instanceOwnById,
  instancesOwn,
  instanceExists,
  instanceByIdExists,
  instancesCount,
  instancesOwnCount
}
