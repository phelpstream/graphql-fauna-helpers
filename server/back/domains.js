module.exports = {
  event: require('./domains/event'),
  auth: require('./domains/auth'),
  resource: require('./domains/resource'),
  notification: require('./domains/notification'),
  user: require('./domains/user'),
  userRole: require('./domains/userRole'),
  log: require('./domains/log'),
  graphql: require('./domains/graphql'),
  unitTest: require('./domains/unitTest'),
  fauna: require('./domains/fauna'),
  algolia: require('./domains/algolia'),
  comment: require('./domains/comment')
}
