const base = require("./base");
const instanceLog = require("./instance_log");

const fns = { base, instanceLog };

module.exports = (name, generators = Object.keys(fns)) => {
  return generators.reduce((list, gen) => {
    list.push(...fns[gen](name));
    return list;
  }, []);
};
