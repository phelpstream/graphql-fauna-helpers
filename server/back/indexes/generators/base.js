module.exports = (name) => [
  {
    name: `all_${name}`
  },
  {
    name: `${name}_by_id`,
    terms: [ { field: [ 'data', 'id' ] } ],
    unique: true
  },
  {
    name: `${name}_by_id_no_soft_deleted`,
    terms: [ { field: [ 'data', 'id' ] }, { field: [ 'data', 'log', 'deleted' ] } ],
    unique: true
  }
]
