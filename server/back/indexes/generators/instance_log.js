module.exports = name => [
  {
    name: `${name}_by_created_at`,
    terms: [{ field: ["data", "log", "created_at"] }]
  },
  {
    name: `${name}_by_created_at__reverse`,
    terms: [{ field: ["data", "log", "created_at"], reverse: true }, { field: "ref" }]
  },
  {
    name: `${name}_by_created_by`,
    terms: [{ field: ["data", "log", "created_by_id"] }]
  },
  {
    name: `${name}_by_owned_by`,
    terms: [{ field: ["data", "log", "owned_by_id"] }]
  },
  {
    name: `${name}_by_created_by`,
    terms: [{ field: ["data", "log", "created_by_id"] }]
  },
  {
    name: `${name}_by_updated_at`,
    terms: [{ field: ["data", "log", "updated_at"] }]
  },
  {
    name: `${name}_by_updated_at__reverse`,
    terms: [{ field: ["data", "log", "updated_at"], reverse: true }, { field: "ref" }]
  },
  {
    name: `${name}_by_updated_by`,
    terms: [{ field: ["data", "log", "updated_by_id"] }]
  },
  {
    name: `${name}_by_deleted`,
    terms: [{ field: ["data", "log", "deleted"] }]
  },
  {
    name: `${name}_by_deleted_at`,
    terms: [{ field: ["data", "log", "deleted_at"] }]
  },
  {
    name: `${name}_by_deleted_at__reverse`,
    terms: [{ field: ["data", "log", "deleted_at"], reverse: true }, { field: "ref" }]
  },
  {
    name: `${name}_by_deleted_by`,
    terms: [{ field: ["data", "log", "deleted_by_id"] }]
  },
  {
    name: `${name}_by_archived`,
    terms: [{ field: ["data", "log", "archived"] }]
  },
  {
    name: `${name}_by_archived_at`,
    terms: [{ field: ["data", "log", "updated_at"] }]
  },
  {
    name: `${name}_by_archived_at__reverse`,
    terms: [{ field: ["data", "log", "updated_at"], reverse: true }, { field: "ref" }]
  },
  {
    name: `${name}_by_archived_by`,
    terms: [{ field: ["data", "log", "archived_by_id"] }]
  }
];
