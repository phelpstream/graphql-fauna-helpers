const collection = instanceName => `${instanceName}s`
const index_by_ref_id = instanceName => `all_${instanceName}s`
const index_by_id = instanceName => `${instanceName}s_by_id`

const inContextDataSimple = instanceName => `${instanceName}InContextDataSimple`
const inContextDataExists = instanceName => `${instanceName}InContextDataExists`
const inContextDataCount = instanceName => `${instanceName}InContextDataCount`
const inContextDataList = instanceName => `${instanceName}InContextDataList`
const inContextDataDefault = instanceName => `${instanceName}InContextDataDefault`

const instanceDefault = instanceName => `${instanceName}Default`
const instanceCreate = instanceName => `${instanceName}Create`
const instanceCreateJSON = instanceName => `${instanceName}CreateJSON`
const instanceImport = instanceName => `${instanceName}Import`
const instancesImport = instanceName => `${instanceName}sImport`
const instancePatch = instanceName => `${instanceName}Patch`
const instancesCreate = instanceName => `${instanceName}sCreate`
const instanceUpdateOwner = instanceName => `${instanceName}UpdateOwner`
const instanceReplace = instanceName => `${instanceName}Replace`
const instanceUpdate = instanceName => `${instanceName}Update`
const instanceUpdateJSON = instanceName => `${instanceName}UpdateJSON`
const instanceUpdateListItem = instanceName => `${instanceName}UpdateListItem`
const instanceSoftDelete = instanceName => `${instanceName}SoftDelete`
const instanceDelete = instanceName => `${instanceName}Delete`
const instancesDelete = instanceName => `${instanceName}sDelete`
const updateOwner = instanceName => `${instanceName}UpdateOwner`

const instance = instanceName => instanceName
const instanceById = instanceName => `${instanceName}ById`
const instanceSync = instanceName => `${instanceName}Sync`
const instanceSyncById = instanceName => `${instanceName}SyncById`
const instanceMatch = instanceName => `${instanceName}Match`
const instances = instanceName => `${instanceName}s`
const instancesSync = instanceName => `${instanceName}sSync`
const instancesList = instanceName => `${instanceName}sList`
const instancesLast = instanceName => `${instanceName}sLast`
const instanceExists = instanceName => `${instanceName}Exists`
const instanceByIdExists = instanceName => `${instanceName}ByIdExists`
const instancesCount = instanceName => `${instanceName}sCount`

const instanceCreateOwn = instanceName => `${instanceName}CreateOwn`
const instancePatchOwn = instanceName => `${instanceName}PatchOwn`
const instancesCreateOwn = instanceName => `${instanceName}sCreateOwn`
const instanceUpdateOwn = instanceName => `${instanceName}UpdateOwn`
const instanceSoftDeleteOwn = instanceName => `${instanceName}SoftDeleteOwn`
const instanceDeleteOwn = instanceName => `${instanceName}DeleteOwn`
const instancesDeleteAllOwn = instanceName => `${instanceName}sDeleteAllOwn`

const instanceOwn = instanceName => `${instanceName}Own`
const instancesOwn = instanceName => `${instanceName}sOwn`
const instanceOwnMatch = instanceName => `${instanceName}OwnMatch`
const instanceOwnById = instanceName => `${instanceName}OwnById`
const instancesOwnCount = instanceName => `${instanceName}sOwnCount`

module.exports = {
  collection,
  index_by_ref_id,
  index_by_id,

  inContextDataSimple,
  inContextDataExists,
  inContextDataCount,
  inContextDataList,
  inContextDataDefault,
  instanceDefault,
  instanceCreate,
  instanceCreateJSON,
  instanceImport,
  instancesImport,
  instanceUpdateOwner,
  instanceUpdate,
  instanceUpdateJSON,
  instancePatch,
  instanceReplace,
  updateOwner,
  instanceOwnMatch,
  instanceOwn,
  instanceOwnById,
  instancesOwn,
  instance,
  instanceById,
  instanceSync,
  instanceSyncById,
  instanceMatch,
  instanceUpdateListItem,
  instances,
  instancesSync,
  instancesCreate,
  instancesImport,
  instancesList,
  instancesLast,
  instanceExists,
  instanceByIdExists,
  instanceSoftDelete,
  instanceDelete,
  instancesDelete,
  instancesCount,
  instancesOwnCount,
  instanceCreateOwn,
  instancePatchOwn,
  instancesCreateOwn,
  instanceUpdateOwn,
  instanceSoftDeleteOwn,
  instanceDeleteOwn,
  instancesDeleteAllOwn
}
