module.exports = {
  import: 'import',
  importList: 'importList',
  get: 'get',
  sync: 'sync',
  getAll: 'getAll',
  syncList: 'syncList',
  create: 'create',
  createList: 'createList',
  update: 'update',
  updateList: 'updateList',
  patch: 'patch',
  patchList: 'patchList',
  delete: 'delete',
  deleteList: 'deleteList',
  indexCount: 'indexCount'
}
