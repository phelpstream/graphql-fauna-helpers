const Debug = require('debug')
const debug = Debug('gfh-server [back:events:index]')

const { is, not, pipe, isNil, merge } = require('ramda')
const { getOr, get, lower, equals } = require('svpr')

const nope = pipe(isNil, not)

const selectMethod = (methodName, methods) => {
  // console.log("methodName", methodName, "methods", methods);
  let fn = get(`${methodName}`, methods)
  // console.log("fn", fn);
  return is(Function, fn) ? fn : () => null
}

function EventsManager({ providers = [], domains = {} }) {
  // ctx
  // this.providers = providers

  return {
    emit: async function({ eventName, method, instanceName, idsList = [], index, definition, ref_id_path = 'ref_id', id_path = 'id', recursive = true }, ctx) {
      const collection = ctx.namings.collection(instanceName)
      debug('EventsManager emit', eventName, collection, method, instanceName, idsList)
      // debug("domains", domains, instanceName)
      const concernedDomainEvents = getOr(`${instanceName}.events`, domains)
      // debug("concernedDomainEvents", JSON.stringify(concernedDomainEvents, null, 2))
      // FIRES EVENTS
      // debug("concernedDomainEvents", JSON.stringify(concernedDomainEvents, null, 2))
      // debug('concernedDomainEvents && recursive', concernedDomainEvents, recursive)
      if (concernedDomainEvents && Object.keys(concernedDomainEvents).length > 0 && recursive) {
        const startEvents = get('start', concernedDomainEvents)
        Object.entries(startEvents).map(([ key, value = {} ]) => {
          if (key === '*') {
            let newValue = merge({ eventName, method, instanceName, idsList, index, definition, ref_id_path, id_path, recursive: false }, value)
            // debug("sub emit", newValue)
            this.emit(newValue, ctx).catch(console.error)
          } else if (equals(eventName, key)) {
            // ...
          }
        })
      }

      // debug('dispatch!!', eventName, instanceName, ids)
      // let refined_ids = objects.reduce((list, obj) => {
      //   if (get(ref_id_path, obj) && get(id_path, obj)) {
      //     list.push({ id: id_path, ref_id: ref_id_path })
      //   }
      //   return list
      // }, [])

      if (!collection) {
        debug('No collection for', instanceName)
        return
      }

      // debug('idsList', idsList)

      let refined_ids = idsList.map(getOr({}, 'data')).filter((i) => nope(i.ref_id) && nope(i.id))
      let fetchedData
      // debug('refined_ids', refined_ids)

      if (eventName && !lower(eventName).includes('delete')) {
        let methodName = method || ctx.namings.instancesList(instanceName)
        let methodType = ctx.graphql.methodType(methodName)
        let defaultDefinition = ctx.graphql.definition(methodType)
        let defaultIndex = `all_${collection}`
        // ref_ids: refined_ids.map(i => i.ref_id)
        // debug('ids', ids)
        fetchedData = await ctx.binding.query[methodName]({ index: index || defaultIndex, idsList, skipEvent: true }, definition || defaultDefinition, { context: ctx })
        debug('fetchedData for', methodName) //, fetchedData
      }

      let objects = get('data', fetchedData)

      // debug('objects', objects)

      try {
        let runMethods = providers.map(async (provider) => {
          // debug('provider:', provider.name, eventName, provider.methods)
          try {
            return selectMethod(eventName, provider.methods)({ method, collection, objects, idsList: refined_ids, ref_id_path, id_path, ctx })
          } catch (error) {
            debug('Error on method', error)
          }
        })
        return Promise.all(runMethods)
          .then((results) =>
            results.map((result, i) => {
              let providerName
              try {
                providerName = providers[i].eventName
              } catch (error) {}
              return { provider: providerName, data: result }
            })
          )
          .catch(console.error)
      } catch (error) {
        console.error(error)
        throw error
      }
    }
  }

  // const eventFnBuilder = ({ eventName, collection, index, definition, ids, async = true, ref_id_path = 'ref_id', id_path = 'id' }) => {
  //   let { async = true, collection, object = {}, objects = [], id_path = 'id' } = { eventName, collection, index, definition, ids }
  //   try {
  //     let runMethods = providers.map(provider => {
  //       try {
  //         return selectMethod(eventName, provider.methods)({ async, collection, object, objects, id_path, ctx })
  //       } catch (error) {
  //         debug('Error on method', error)
  //       }
  //     })
  //     return async ? Promise.all(runMethods).catch(console.error) : runMethods
  //   } catch (error) {
  //     // console.error(error);
  //     throw error
  //   }
  // }

  // return {
  //   get({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('get', {eventName, collection, index, definition, ids})
  //   },
  //   getAll({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('getAll', {eventName, collection, index, definition, ids})
  //   },
  //   sync({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('sync', {eventName, collection, index, definition, ids})
  //   },
  //   syncList({eventName, collection, index, definition, ids}) {
  //     // debug("syncList", {eventName, collection, index, definition, ids})
  //     return eventFnBuilder('syncList', {eventName, collection, index, definition, ids})
  //   },
  //   create({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('create', {eventName, collection, index, definition, ids})
  //   },
  //   createList({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('createList', {eventName, collection, index, definition, ids})
  //   },
  //   update({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('update', {eventName, collection, index, definition, ids})
  //   },
  //   updateList({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('updateList', {eventName, collection, index, definition, ids})
  //   },
  //   patch({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('patch', {eventName, collection, index, definition, ids})
  //   },
  //   patchList({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('patchList', {eventName, collection, index, definition, ids})
  //   },
  //   softDelete({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('softDelete', {eventName, collection, index, definition, ids})
  //   },
  //   softDeleteAll({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('softDeleteAll', {eventName, collection, index, definition, ids})
  //   },
  //   delete({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('delete', {eventName, collection, index, definition, ids})
  //   },
  //   deleteList({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('deleteList', {eventName, collection, index, definition, ids})
  //   },
  //   indexCount({eventName, collection, index, definition, ids}) {
  //     return eventFnBuilder('indexCount', {eventName, collection, index, definition, ids})
  //   }
  // }
}

module.exports = EventsManager
