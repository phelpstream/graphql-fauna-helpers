module.exports = {
  Algolia: require("./algolia"),
  Crisp: require("./crisp"),
  // Pusher: require('./pusher'),
  Test: require("./test")
};
