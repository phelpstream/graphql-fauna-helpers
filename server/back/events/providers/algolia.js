const Debug = require('debug')
const debug = Debug('gfh-server [back:events:providers:algolia]')

const { get, getOr, omitList, clone } = require('svpr')
const algoliasearch = require('algoliasearch')

const filterData = (objList, { id_path }) => objList.filter(o => get(id_path, o) !== null)
const filterLog = obj => omitList(['log.owned_by', 'log.owner_changed_by', 'log.created_by', 'log.updated_by', 'log.deleted_by', 'log.archived_by'], obj)

const prepareData = (objects, { id_path, dontFilterLog, transformer = x => x }) => {
  // debug('objects', objects)

  return Object.values(objects).map((obj = {}) => {
    try {
      let newObj = clone(transformer(obj))
      if (!dontFilterLog) newObj = filterLog(newObj)
      newObj.objectID = newObj[id_path]
      // if(!obj[id_path]){
      //   debug("id_path", id_path, obj)
      // }
      // debug("newObj", newObj);
      // debug("newObj.objectID", newObj.objectID);

      // if (!newObj.objectID) {
      // debug('[prepareData] EMPTY newObj.objectID', newObj.objectID, newObj)
      // }
      return newObj
    } catch (error) {
      debug('[ERROR] Algolia:', error)
      return {}
    }
  })
}

module.exports = function AlgoliaSync({ ALGOLIA_APP_ID, ALGOLIA_API_KEY, env, transformersMap = {}, eventFilter = input => true } = {}) {
  this.name = 'Algolia'

  const client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_API_KEY)

  const indexBuild = collection => `${collection}${env ? '_' + env : ''}`

  const getTransformer = collection => getOr(x => x, collection, transformersMap)

  const doesIndexExists = index =>
    new Promise(resolve => {
      client.listIndexes((err, content) => {
        if (err) resolve(false)
        resolve(content.items.find(i => i.name === index))
      })
    })

  const middleware = input => fn => {
    let filtered = eventFilter(input)
    debug('filtered', filtered)
    if (!filtered) { //  !== false
      return fn(input)
    }
  }

  const createOrUpdateSync = async ({ index, collection, objects = [], idsList = [], id_path, ref_id_path }) => {
    debug('createOrUpdateSync')
    let transformer = getTransformer(collection)
    let preparedData = prepareData(objects, { id_path, transformer })
    // debug("preparedData", preparedData)
    preparedData = preparedData.filter(obj => obj && obj.objectID)
    // debug("preparedData", preparedData)

    debug('createOrUpdateSync', preparedData.map((rd = {}) => `${rd.id} :: ${rd.ref_id}`))
    let exists = await doesIndexExists(index)
    if (!exists) return
    // debug("createOrUpdateSync");
    let clientIndex = client.initIndex(index)
    // debug("clientIndex", clientIndex);
    // debug("transformer", transformer);
    // debug('preparedData', preparedData)
    return new Promise((resolve, reject) => {
      // debug('preparedData', preparedData)
      clientIndex.saveObjects(preparedData, function(err, content) {
        if (err) {
          debug('[ERROR] Algolia:', err)
          reject(err)
        }
        // debug('saved', content)
        resolve(content)
        // callback(null, content);
      })
    })
  }

  const deleteSync = async ({ index, objects = [], idsList = [], id_path }) => {
    let exists = await doesIndexExists(index)
    if (!exists) return
    let clientIndex = client.initIndex(index)
    clientIndex.deleteObjects(ids.map(i => i.id), function(err, content) {
      if (err) debug('[ERROR] Algolia:', err)
      // callback(null, content);
    })
  }

  this.methods = {
    sync: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    syncList: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    create: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    createList: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    update: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    updateList: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    patch: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    patchList: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    softDelete: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    softDeleteAll: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(createOrUpdateSync)
    },
    delete: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(deleteSync)
    },
    deleteList: ({ method, objects = [], idsList = [], id_path, ref_id_path, collection } = {}) => {
      return middleware({ method, index: indexBuild(collection), collection, objects, id_path, ref_id_path, idsList })(deleteSync)
    }
  }

  return this
}
