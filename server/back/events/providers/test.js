module.exports = function TestSync({ transformer = x => x } = {}) {
  this.name = 'Test'

  const resolver = (name) => (objects = [], ids = [], id_path, ref_id_path, collection ) => {
    console.log(`[Test:${name}]`, 'collection', collection, 'objects', objects.length, 'ids', ids.length)
    // console.log(`[Test:${name}]`, 'collection', collection, 'objects', objects, 'ids', ids)
  }

  this.methods = {
    get: resolver("get"),
    getAll: resolver("getAll"),
    sync: resolver("sync"),
    syncList: resolver("syncList"),
    create: resolver("create"),
    createList: resolver("createList"),
    update: resolver("update"),
    updateList: resolver("updateList"),
    patch: resolver("patch"),
    patchList: resolver("patchList"),
    delete: resolver("delete"),
    deleteList: resolver("deleteList"),
    indexCount: resolver("indexCount"),
  }

  return this
}
