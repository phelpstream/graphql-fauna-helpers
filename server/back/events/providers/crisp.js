const Debug = require('debug')
const debug = Debug('gfh-server [back:events:providers:crisp]')

const Crisp = require('node-crisp-api')
const { get, rejectDeep } = require('svpr')
const { isNil } = require('ramda')

const collectionFilter = (collections = []) => (method = args => {}) => (args = {}) => {
  const collection = get('collection', args)
  let isOk = collections.length > 0 && collections.includes(collection)
  // debug('Filtering for', collections, 'on', collection, 'test?', isOk)
  if (isOk) {
    return method(args)
  }
}

function CrispSync({ CRISP_IDENTIFIER, CRISP_KEY, CRISP_WEBSITE_ID } = {}) {
  const CrispClient = new Crisp()
  CrispClient.authenticate(CRISP_IDENTIFIER, CRISP_KEY)

  const userCollectionOnly = collectionFilter(['users'])

  const getRefId = user => get('ref_id', user)
  const getPeopleId = user => get('identification.crisp_id', user)
  const getEmail = user => get('profile.email', user)

  const buildNickname = user => {
    let nickname = `${get('profile.identity.first_name', user)} ${get('profile.identity.last_name', user)}`
    if (nickname.length < 2) {
      nickname = getEmail(user)
    }
    if (nickname.length < 2) {
      nickname = 'John Doe'
    }
    return nickname
  }

  const pickForCrispUser = user =>
    rejectDeep(isNil, {
      nickname: buildNickname(user),
      avatar: get('profile.picture', user),
      gender: get('profile.identity.gender', user),
      phone: get('profile.phone.number', user)
    })

  const findUserByEmail = email => CrispClient.websitePeople.findByEmail(CRISP_WEBSITE_ID, email)
  const getUser = people_id => CrispClient.websitePeople.getPeopleProfile(CRISP_WEBSITE_ID, people_id)
  const createUser = ({ email, person }) => CrispClient.websitePeople.createNewPeopleProfile(CRISP_WEBSITE_ID, { email, person })
  const updateUser = (people_id, person) => CrispClient.websitePeople.updatePeopleProfile(CRISP_WEBSITE_ID, people_id, { person })
  // const removeUser = people_id => CrispClient.websitePeople.removePeopleProfile(CRISP_WEBSITE_ID, people_id)

  const createUserToCrisp = async ({ user, ctx }) => {
    let ref_id = getRefId(user)
    let email = getEmail(user)
    debug('createUserToCrisp', ref_id, email)
    if (email) {
      let people_id
      people_id = await findUserByEmail(email)
        .then(get('people_id'))
        .catch(err => debug(err))
      debug('findUserByEmail', people_id)
      let personData = pickForCrispUser(user)
      debug('personData', personData)
      if (people_id) {
        await updateUser(people_id, personData).catch(err => debug(err))
      } else {
        people_id = await createUser({ email, person: personData })
          .then(get('people_id'))
          .catch(err => debug(err))
      }

      debug('people_id', people_id)

      if (people_id) {
        let updatedUser = await ctx.binding.mutation
          .userUpdate({ ref_id, input: { identification: { crisp_id: people_id } } }, ctx.graphql.definition('UserInContextDataSimple'), { context: ctx })
          .catch(err => debug(err))
        debug('updatedUser > crisp_id', get('data.identification.crisp_id', updatedUser))
      }
    }
  }

  const upsertUserToCrisp = async ({ user, ctx }) => {
    // debug("user", user)
    let people_id = getPeopleId(user)
    debug('upsertUserToCrisp', people_id)
    let personData = pickForCrispUser(user)
    debug('personData', personData)
    if (people_id) {
      // const fetchedProfile = await getUser(people_id)
      // debug('fetchedProfile', fetchedProfile)
      return updateUser(people_id, personData).catch(err => {
        debug(err)
        if (err.message === 'people_not_found') {
          debug('people_not_found > createUserToCrisp')
          return createUserToCrisp({ user, ctx })
        }
      })
    } else {
      return createUserToCrisp({ user, ctx })
    }
  }

  // const deleteUserFromCrisp = async ({ user, ctx }) => {
  //   let people_id = getPeopleId(user)
  //   if (people_id) {
  //     const crisp_id = await createUser({ email, person: pickForCrispUser(user) }).then(get('data.people_id'))
  //     await ctx.query.updateUser({ ref_id, input: { identification: { crisp_id } } })
  //   }
  // }

  const upsertUserToCrispResolver = ({ collection, objects = [], id_path = 'id', options = {}, ctx } = {}) => {
    return Promise.all(objects.map(object => upsertUserToCrisp({ user: object, ctx })))
  }

  this.methods = {
    sync: userCollectionOnly(upsertUserToCrispResolver),
    syncList: userCollectionOnly(upsertUserToCrispResolver),
    create: userCollectionOnly(upsertUserToCrispResolver),
    createList: userCollectionOnly(upsertUserToCrispResolver),
    update: userCollectionOnly(upsertUserToCrispResolver),
    updateList: userCollectionOnly(upsertUserToCrispResolver),
    patch: userCollectionOnly(upsertUserToCrispResolver),
    patchList: userCollectionOnly(upsertUserToCrispResolver),
    delete: userCollectionOnly(({ collection, objects = [], id_path = 'id', options = {}, ctx } = {}) => {
      // Don't want to delete from Crisp I think
    }),
    deleteList: userCollectionOnly(({ collection, objects = [], id_path = 'id', options = {}, ctx } = {}) => {
      // Don't want to delete from Crisp I think
    })
  }

  return this
}

CrispSync.prototype.name = 'Crisp'

module.exports = CrispSync
