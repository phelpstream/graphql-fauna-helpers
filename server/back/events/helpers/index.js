const Debug = require('debug')
const debug = Debug('gfh-server [back:events:helpers:index]')

const eventNames = require('./../names')

// const { get } = require('svpr')
const { pick } = require('ramda')

const dispatch = ({ name, method, instanceName, skipEvent, ref_id_path = 'ref_id', id_path = 'id' }, ctx) => (response = {}) => {
  if (skipEvent) return response

  let input = {
    name,
    method,
    instanceName,
    // typeName,
    idsList: ctx.utils.context_data_ids(response, { ref_id_path, id_path }).map((d) => ({ data: d }))
  }

  let rules = {
    allow: [
      eventNames.sync,
      eventNames.syncList,
      eventNames.import,
      eventNames.importList,
      eventNames.create,
      eventNames.createList,
      eventNames.update,
      eventNames.updateList,
      eventNames.patch,
      eventNames.patchList,
      eventNames.delete,
      eventNames.deleteList
    ]
  }

  // debug("idsList", input.idsList)

  if (rules.allow.includes(name)) {
    ctx.aws.events.triggerBinding(
      {
        query: true,
        method: 'eventDispatch',
        args: { input },
        topicArn: ctx.aws.GRAPHQL_API_TOPIC_ARN
      },
      ctx
    )

    if (!response.events) {
      response.events = { list: [] }
    }

    if (!response.events.list) {
      response.events.list = []
    }

    response.events.list.push(pick([ 'name', 'method', 'instanceName', 'index', 'ids', 'definition' ], input))
  } else {
    debug('not dispatching event:', name)
  }

  return response
}

const dispatchList = (events = [], ctx) => (response) => {
  return events.reduce((res, event) => dispatch(event, ctx)(res), response)
}

module.exports = { dispatch, dispatchList }
