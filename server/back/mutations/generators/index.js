const Debug = require('debug')
const debug = Debug('gfh-server [back:mutations:generators:index]')

const { get } = require('svpr')

const s = require('./../../scalars')
const e = require('./../../entities')
const t = require('./../../../graphql/type')

const namings = require('./../../namings')

const { argumentsFilter } = require('./../helpers')

const setCollection = (collections, instanceName) => {
  return get(instanceName, collections) || collections
}

const argsBuilder = (args = {}, options = {}) => {
  let { skipEvent } = options
  if (skipEvent) {
    args.skipEvent = t.Def(s.Boolean)
  }
  return args
}

const instanceCreate = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceCreate(instanceName)]: {
    type: entity.in_context_simple,
    args: argsBuilder(
      {
        input: t.Def(entity.input),
        password: t.Def(s.String)
      },
      { skipEvent: true }
    ),
    resolve: async function (root, { input, password, skipEvent }, ctx, info) {
      return ctx.rsv[domain][namings.instanceCreate(instanceName)]({ input, password, skipEvent }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceCreateJSON = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceCreateJSON(instanceName)]: {
    type: s.JSON,
    args: argsBuilder(
      {
        input: t.Def(s.JSON),
        password: t.Def(s.String)
      },
      { skipEvent: true }
    ),
    resolve: async function (root, { input, password, skipEvent }, ctx, info) {
      return ctx.rsv[domain][namings.instanceCreate(instanceName)]({ input, password, skipEvent }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceImportMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceImport(instanceName)]: {
    type: entity.in_context_simple,
    args: argsBuilder(
      {
        input: t.Def(entity.input),
        password: t.Def(s.String),
        owner_id: t.Def(s.ID)
      },
      { skipEvent: true }
    ),
    resolve: async function (root, { input, password, owner_id, skipEvent }, ctx, info) {
      return ctx.rsv[domain][namings.instanceImport(instanceName)]({ input, password, owner_id, skipEvent }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instancesImportMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instancesImport(instanceName)]: {
    type: entity.in_context_list,
    args: argsBuilder(
      {
        input: t.Def(t.List(entity.input)),
        password: t.Def(s.String),
        owner_id: t.Def(s.ID)
      },
      { skipEvent: true }
    ),
    resolve: async function (root, { input, password, owner_id, skipEvent }, ctx, info) {
      return ctx.rsv[domain][namings.instancesImport(instanceName)]({ input, password, owner_id, skipEvent }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const createOwnMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceCreateOwn(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      input: t.Def(entity.input),
      password: t.Def(s.String)
    },
    resolve: async function (root, { input, password }, ctx, info) {
      return ctx.rsv[domain][namings.instanceCreate(instanceName)]({ input, password }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instancesCreateMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instancesCreate(instanceName)]: {
    type: entity.in_context_list,
    args: {
      input: t.Def(t.List(entity.input))
    },
    resolve: async function (root, { input = [] }, ctx, info) {
      return ctx.rsv[domain][namings.instancesCreate(instanceName)]({ input }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceUpdateListItemFn = ({ instanceName, domain } = {}) =>
  async function (source, { ref_id, input, listPath, uniquePath, uniqueValue, remove }, ctx, info) {
    let { queries, mutations } = ctx.graphql.methodsArgumentTypes
    let foundMethodDef = get(namings.instanceUpdate(instanceName), mutations)
    // console.log("foundMethodDef", foundMethodDef, namings.instanceUpdate(instanceName));
    if (foundMethodDef) {
      argumentsModifier = args => argumentsFilter(args, foundMethodDef, ctx.graphql.inputTypeFilters)
    }
    return ctx.rsv[domain][namings.instanceUpdateListItem(instanceName)](
      { ref_id, listPath, uniquePath, uniqueValue, input, remove, updateFn: ctx.rsv[domain][namings.instanceUpdate(instanceName)], argumentsModifier },
      ctx
    )
  }

const instanceUpdateListItem = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceUpdateListItem(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(t.NonNull(s.ID)),
      input: t.Def(t.NonNull(s.Any)),
      listPath: t.Def(s.String),
      uniquePath: t.Def(s.String),
      uniqueValue: t.Def(s.Any),
      remove: t.Def(s.Boolean, { default: false })
    },
    resolve: instanceUpdateListItemFn({ instanceName, domain }),
    _collections: setCollection(collections, 'instances')
  }
})

const patchMutationResolveFn = ({ instanceName, domain, entity } = {}) =>
  async function (root, { ref_id, input, blind_mode }, ctx, info) {
    let { queries, mutations } = ctx.graphql.methodsArgumentTypes
    let foundMethodDef = get(namings.instanceUpdate(instanceName), mutations)
    let argumentsModifier = x => x
    // console.log("foundMethodDef", foundMethodDef, namings.instanceUpdate(instanceName));
    if (foundMethodDef) {
      argumentsModifier = args => argumentsFilter(args, foundMethodDef, ctx.graphql.inputTypeFilters)
    }
    // debug('namings.instance(instanceName)', instanceName, namings.instance(instanceName))
    let prom = ctx.rsv[domain][namings.instancePatch(instanceName)](
      {
        ref_id,
        input,
        // updateFn: ctx.rsv[domain][namings.instanceUpdate(instanceName)],
        replaceFn: ctx.rsv[domain][namings.instanceReplace(instanceName)],
        argumentsModifier,
        instanceType: namings.instance(instanceName),
        instanceDefinition: ctx.graphql.definition(entity.in_context_simple.name)
      },
      ctx
    )
    if (blind_mode) return {}
    return prom
  }

const patchMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instancePatch(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID),
      input: t.Def(t.List(e.Patch.input)),
      blind_mode: t.Def(s.Boolean)
    },
    resolve: patchMutationResolveFn({ instanceName, domain, entity }),
    _collections: setCollection(collections, 'instances')
  }
})

const patchOwnMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instancePatchOwn(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID),
      input: t.Def(t.List(e.Patch.input)),
      path: t.Def(s.String)
    },
    resolve: patchMutationResolveFn({ instanceName, domain, entity }),
    _collections: setCollection(collections, 'instances')
  }
})

const instanceUpdateOwner = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceUpdateOwner(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(t.NonNull(s.REF_ID)),
      user_id: t.Def(t.NonNull(s.String))
    },
    resolve: async function (root, { ref_id, user_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceUpdateOwner(instanceName)]({ ref_id, user_id }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instanceUpdateJSON = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceUpdateJSON(instanceName)]: {
    type: s.JSON,
    args: {
      ref_id: t.Def(s.REF_ID),
      input: t.Def(s.JSON)
    },
    resolve: async function (root, { ref_id, input }, ctx, info) {
      return ctx.rsv[domain][namings.instanceUpdateJSON(instanceName)]({ ref_id, input }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const updateMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceUpdate(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID),
      input: t.Def(entity.input)
    },
    resolve: async function (root, { ref_id, input }, ctx, info) {
      return ctx.rsv[domain][namings.instanceUpdate(instanceName)]({ ref_id, input }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const updateOwnMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceUpdateOwn(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID),
      input: t.Def(entity.input)
    },
    resolve: async function (root, { ref_id, input }, ctx, info) {
      return ctx.rsv[domain][namings.instanceUpdate(instanceName)]({ ref_id, input }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const updateOwnerMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.updateOwner(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(t.NonNull(s.String)),
      user_id: t.Def(t.NonNull(s.String))
    },
    resolve: async function (root, { ref_id, user_id }, ctx, info) {
      return ctx.rsv[domain][namings.updateOwner(instanceName)]({ ref_id, user_id }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const softDeleteMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceSoftDelete(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID)
    },
    resolve: async function (root, { ref_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceSoftDelete(instanceName)]({ ref_id }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const softDeleteOwnMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceSoftDeleteOwn(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID)
    },
    resolve: async function (root, { ref_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceSoftDelete(instanceName)]({ ref_id }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const deleteMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceDelete(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID)
    },
    resolve: async function (root, { ref_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceDelete(instanceName)]({ ref_id }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const instancesDeleteMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instancesDelete(instanceName)]: {
    type: entity.in_context_list,
    args: {
      ref_ids: t.Def(t.List(s.REF_ID))
    },
    resolve: async function (root, { ref_ids }, ctx, info) {
      return ctx.rsv[domain][namings.instancesDelete(instanceName)]({ ref_ids }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const deleteOwnMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instanceDeleteOwn(instanceName)]: {
    type: entity.in_context_simple,
    args: {
      ref_id: t.Def(s.REF_ID)
    },
    resolve: async function (root, { ref_id }, ctx, info) {
      return ctx.rsv[domain][namings.instanceDelete(instanceName)]({ ref_id }, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const deleteListOwnMutation = ({ instanceName, entity, domain, collections }) => ({
  [namings.instancesDeleteAllOwn(instanceName)]: {
    type: entity.in_context_list,
    resolve: async function (root, _, ctx, info) {
      console.log('domain', domain, ctx.rsv[domain])
      return ctx.rsv[domain][namings.instancesDeleteAllOwn(instanceName)](_, ctx)
    },
    _collections: setCollection(collections, 'instances')
  }
})

const CUD = ({ instanceName, entity, domain, collections, includeOwn = false }) => {
  if (!collections) {
    collections = {
      [`${instanceName}s`]: {
        key: 'id'
      }
    }
  }

  let mutators = [
    instancesDeleteMutation,
    instanceImportMutation,
    instancesImportMutation,
    instanceUpdateListItem,
    instancesCreateMutation,
    instanceCreate,
    instanceCreateJSON,
    patchMutation,
    instanceUpdateOwner,
    instanceUpdateJSON,
    updateMutation,
    updateOwnerMutation,
    softDeleteMutation,
    deleteMutation
  ]

  let ownMutators = [createOwnMutation, patchOwnMutation, updateOwnMutation, softDeleteOwnMutation, deleteOwnMutation, deleteListOwnMutation]

  return [...mutators, ...(includeOwn ? ownMutators : [])].reduce((rsv, mutator) => {
    return { ...mutator({ instanceName, entity, domain, collections }), ...rsv }
  }, {})
}

module.exports = {
  CUD,
  instancesDeleteMutation,
  instanceImportMutation,
  instancesImportMutation,
  instancesCreateMutation,
  instanceUpdateListItem,
  instanceCreate,
  instanceCreateJSON,
  instanceUpdateOwner,
  instanceUpdateJSON,
  updateMutation,
  updateOwnerMutation,
  softDeleteMutation,
  deleteMutation,
  deleteListOwnMutation
}
