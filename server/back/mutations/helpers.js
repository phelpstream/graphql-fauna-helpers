const { is } = require('ramda')
const { omitList, getOr, clone } = require('svpr')

module.exports.argumentsFilter = function(args, argumentsSignature = {}, typeFilters) {
  // console.time("filter_arguments");
  let filteredArgs = {}

  try {
    filteredArgs = Object.entries(args).reduce((obj, [key, value]) => {
      let type = argumentsSignature[key]
      let hasValueToFilter = !!value
      // console.log("hasValueToFilter", hasValueToFilter, "type", type);
      if (type && hasValueToFilter) {
        let cleanType = type.replace(/\W/gim, '').replace('Input', '')
        let filters = getOr([], cleanType, typeFilters)
        // let isArray = type.includes("[")
        let isReallyArray = is(Array, value)
        // console.log("cleanType", cleanType, "filters", filters, "isReallyArray", isReallyArray);
        if (filters && filters.length > 0) {
          if (isReallyArray) {
            obj[key] = value.map(omitList(filters))
          } else {
            obj[key] = omitList(filters, value)
          }
        }
      }
      return obj
    }, clone(args))
  } catch (error) {
    // console.error(error);
  }

  // console.timeEnd("filter_arguments");
  // const argsLength = JSON.stringify(args).length;
  // const filteredArgsLength = JSON.stringify(filteredArgs).length;

  // console.log("Filtered args from", argsLength, "to", filteredArgsLength);

  return filteredArgs
}
