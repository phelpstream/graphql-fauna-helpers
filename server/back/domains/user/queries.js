const c = require('./collections')

const userAbstractDefinition = '{data{ref_id id profile {picture identity {first_name last_name full_name}}}}'

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...qg.R({ instanceName: 'user', entity: e.User, domain: 'user', includeOwn: true }),
  // userDefault: {
  //   type: e.User.type,
  //   args: {},
  //   resolve: async function(root, {}, ctx, info) {
  //     return {};
  //   },
  //   _collections: {
  //     userDefault: c.userDefault
  //   }
  // },
  myself: {
    type: e.User.in_context_simple,
    args: {},
    resolve: async function(root, {}, ctx, info) {
      const user_id = ctx.storage.user.id
      return ctx.rsv.user.userOwn({ id: user_id }, ctx)
    },
    _collections: {
      myself: c.myself
    }
  },
  // userLogin: {
  //   type: t.Def(s.String),
  //   args: {},
  //   resolve: async function(root, {}, ctx, info) {
  //     const user_id = ctx.storage.user.id
  //     return ctx.rsv.user.userOwn({ id: user_id }, ctx)
  //   },
  //   _collections: {
  //     myself: c.myself
  //   }
  // },
  usersLast10: {
    type: e.User.in_context_list,
    resolve: async function(root, {}, ctx, info) {
      return (
        ctx.rsv.user
          .usersLast({ size: 10 }, ctx)
          // .query(ctx.fq.instancesLast("users", { size: 10 }, ctx))
          .then(res => {
            console.log('res', res)
            return res
          })
          .catch(console.error)
      )
    },
    _collections: {
      users: c.users
    }
  },
  // usersCreatedAt: {
  //   type: t.List(e.User.type),
  //   resolve: async function(root, {}, ctx, info) {
  //     return ctx.query(ctx.fq.instancesCreatedAt("users", { reverse: true }, ctx)).then(res => {
  //       console.log("res", res);
  //       return res;
  //     });
  //   },
  //   _collections: {
  //     users: c.users
  //   }
  // },
  userByEmail: {
    type: e.User.in_context_simple,
    args: {
      email: t.Def(t.NonNull(s.EmailAddress))
    },
    resolve: async function(root, { email }, ctx, info) {
      return ctx.rsv.user.userMatch({ index: 'users_by_email', value: email }, ctx)
    },
    _collections: {
      users: c.users
    }
  },
  usersSponsoredBy: {
    type: e.User.in_context_list,
    args: {
      code: t.Def(t.NonNull(s.String))
    },
    resolve: async function(root, { code }, ctx, info) {
      return ctx.binding.query.users({ index: 'users_with_sponsor_code', matchValue: code }, userAbstractDefinition, { context: ctx })
    },
    _collections: {
      users: c.users
    }
  },
  userSponsorAbstract: {
    type: e.User.in_context_simple,
    args: {
      code: t.Def(t.NonNull(s.String))
    },
    resolve: async function(root, { code }, ctx, info) {
      return ctx.binding.query.userMatch({ index: 'sponsor_codes_from_users', value: code }, userAbstractDefinition, { context: ctx })
    },
    _collections: {
      usersSponsorAbstracts: c.usersSponsorAbstracts
    }
  },
  usersAbstractsByRole: {
    type: e.User.in_context_list,
    args: {
      role: t.Def(t.NonNull(s.String))
    },
    resolve: async function(root, { role }, ctx, info) {
      return ctx.binding.query.users({ index: 'users_by_role_code', matchValue: role }, userAbstractDefinition, { context: ctx })
    },
    _collections: {
      usersAbstractsByRole: c.usersAbstractsByRole
    }
  },
  userAbstract: {
    type: e.User.in_context_simple,
    args: {
      id: t.Def(t.NonNull(s.String))
    },
    resolve: async function(root, { id }, ctx, info) {
      return ctx.binding.query.userById({ id }, userAbstractDefinition, { context: ctx })
    },
    _collections: {
      usersAbstracts: c.usersAbstracts
    }
  }
})
