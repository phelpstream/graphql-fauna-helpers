const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:user:mutations]')

const { is } = require('ramda')
const { get, getOr } = require('svpr')

const arraylike = require('@mnmldev/arraylike')

const c = require('./collections')
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({ instanceName: 'user', entity: e.User, domain: 'user', collections: { users: c.users }, includeOwn: true }),
  userCreateFromAuth0: {
    type: e.User.in_context_simple,
    args: {
      input: t.Def(s.JSON),
      password: t.Def(t.NonNull(s.String))
    },
    resolve: async function(root, { password, input = {} }, ctx, info) {
      let roles_codes = input.roles && is(Array, input.roles) ? input.roles.map(r => ({ data: r })) : [{ data: 'user' }]
      let user = {
        profile: {
          picture: input.picture,
          identity: {
            nickname: input.nickname,
            first_name: input.given_name,
            last_name: input.family_name,
            gender: ['female', 'male'].includes(input.gender) ? input.gender : undefined
          },
          email: input.email
        },
        rights: {
          roles_codes
        },
        preferences: {
          language: input.locale
        },
        identification: {
          auth0_id: input.user_id
        }
      }

      return ctx.rsv.user.userCreate({ password, input: user }, ctx)
    }
  },
  userSponsorCodeCreate: {
    type: e.User.in_context_simple,
    args: {
      code: t.Def(s.String)
    },
    resolve: async function(root, { code }, ctx, info) {
      const ref_id = ctx.storage.user.ref_id
      if (!code) {
        const user = await ctx.binding.query.myself({}, '{data{ profile{identity{first_name last_name}}}}', { context: ctx }).then(get('data'))
        let shortCode = ctx.utils.random_string(3).toUpperCase()
        debug('shortCode', shortCode)

        let backupCode = ctx.utils.random_string(5).toUpperCase()
        debug('backupCode', backupCode)
        let { first_name, last_name } = getOr({}, 'profile.identity', user)
        if (first_name && last_name) {
          code = `${first_name[0].toUpperCase()}${last_name[0].toUpperCase()}${shortCode}`
        } else {
          code = `SPONSOR_${backupCode}`
        }
        debug('code', code)
      }

      let key = arraylike.keyCreate(code)
      return ctx.binding.mutation.userUpdateJSON(
        {
          ref_id,
          input: {
            sponsorships: {
              codes: {
                [key]: {
                  key,
                  data: { code }
                }
              }
            }
          }
        },
        undefined,
        {
          context: ctx
        }
      )
    }
  },
  userSponsorCodeRemove: {
    type: e.User.in_context_simple,
    args: {
      code_key: t.Def(t.NonNull(s.String))
    },
    resolve: async function(root, { code_key }, ctx, info) {
      const ref_id = ctx.storage.user.ref_id
      return ctx.binding.mutation.userUpdateJSON(
        {
          ref_id,
          input: {
            sponsorships: {
              codes: {
                [code_key]: null
              }
            }
          }
        },
        undefined,
        {
          context: ctx
        }
      )
    }
  },
  userCodeFromSponsorUpdate: {
    type: e.User.in_context_simple,
    args: {
      code: t.Def(t.NonNull(s.String)),
      remove: t.Def(s.Boolean, { defaut: false })
    },
    resolve: async function(root, { code, remove }, ctx, info) {
      const user_id = ctx.storage.user.id
      return ctx.rsv.user.userUpdateListItem(
        {
          remove,
          instanceType: 'userById',
          instanceDefinition: ctx.graphql.definition(e.User.in_context_simple.name),
          updateFn: ctx.rsv.user.userUpdate,
          id: user_id,
          uniqueValue: code,
          input: code,
          listPath: 'sponsorships.sponsors'
        },
        ctx
      )
    }
  },
  userRoleCodeUpdate: {
    type: e.User.in_context_simple,
    args: {
      role: t.Def(t.NonNull(s.String)),
      user_id: t.Def(s.ID),
      remove: t.Def(s.Boolean, { defaut: false })
    },
    resolve: async function(root, { role, remove, user_id }, ctx, info) {
      user_id = user_id || ctx.storage.user.id
      return ctx.rsv.user.userUpdateListItem(
        {
          remove,
          instanceType: 'userById',
          instanceDefinition: ctx.graphql.definition(e.User.in_context_simple.name),
          updateFn: ctx.rsv.user.userUpdate,
          id: user_id,
          uniqueValue: role,
          input: role,
          listPath: 'rights.roles_codes'
        },
        ctx
      )
    }
  }
})
