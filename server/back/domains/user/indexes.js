const fauna = require('faunadb')
const q = fauna.query

module.exports = ({ ig, indexGenerators }) => ({
  users: [
    ...ig('users'),
    {
      name: 'users_by_email',
      terms: [{ field: ['data', 'profile', 'email'] }]
    },
    {
      name: 'users_by_oid',
      terms: [{ field: ['data', 'metadata', 'oid'] }]
    },
    {
      name: 'users_by_role_code',
      terms: [{ binding: 'roles' }],
      source: {
        collection: q.Collection('users'),
        fields: {
          roles: q.Query(
            q.Lambda(
              'user',
              q.Map(
                q.Filter(q.ToArray(q.Select(['data', 'rights', 'roles_codes'], q.Var('user'))), q.Lambda('role_code', IsObject(Select(1, Var('role_code'))))),
                q.Lambda('role_code', q.Select([1, 'data'], q.Var('role_code')))
              )
            )
          )
        }
      }
    },
    {
      name: 'sponsor_codes_from_users',
      terms: [{ field: ['data', 'sponsorships', 'codes', 'code'] }],
      unique: true
    },
    {
      name: 'users_with_sponsor_code',
      terms: [{ field: ['data', 'sponsorships', 'sponsors'] }]
    }
  ]
})
