module.exports = {
  userDefault: {
    list: false
  },
  users: {
    key: 'id'
  },
  myself: {
    list: false
  },
  usersAbstracts: {
    key: 'id'
  },
  usersSponsorAbstracts: {
    key: 'id'
  },
  usersAbstractsByRole: {
    key: 'id'
  }
}
