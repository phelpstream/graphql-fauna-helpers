const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:algolia:queries]')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  build_algolia_indices: {
    type: s.Boolean,
    args: {},
    resolve: async function(root, _, ctx, info) {
      // console.log('ctx.algolia.indices', ctx.algolia.indices)
      return ctx.algolia.builders.indices(ctx.algolia.indices)
    }
  }
})
