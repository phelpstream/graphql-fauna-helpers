const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:auth:queries]')
const { get } = require('svpr')
// const stringify = require("json-stable-stringify");
const jwt = require('jsonwebtoken')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r }) => ({
  sign: {
    type: s.String,
    resolve: async function (root, _, ctx, info) {
      const token = ctx.storage.token
      // const checkToken = ctx.utils.jwt.
      return ctx.rsv.auth.jwtGenerate({ data: {} })
    }
  },
  signFromAuth0: {
    type: s.String,
    args: {
      token: t.Def(t.NonNull(s.String))
    },
    resolve: async function (root, { token }, ctx, info) {
      debug('signFromAuth0')
      debug('token', token)
      const parsedToken = ctx.utils.jwt.decode(token)
      const ref_id = parsedToken['https://alloclaim.com/ref_id']
      const user_id = parsedToken['https://alloclaim.com/user_id']
      debug('parsedToken', parsedToken)
      debug('ref_id', ref_id)
      debug('user_id', user_id)

      const foundUser = await ctx.binding.query
        .user({ ref_id }, '{data{id ref_id rights {roles_codes {data}}}}', { context: ctx })
        .then(get('data'))
        .catch(debug)
      debug('foundUser', foundUser)

      if (foundUser) {
        const checkToken = jwt.sign(JSON.stringify({ user: foundUser }), process.env.JWT_SECRET)
        debug('checkToken', checkToken)
        return checkToken
      }
      return undefined
    },
    collections: {
      _auth: {
        method_name_as_key: true
      }
    }
  },
  verify: {
    type: s.Boolean,
    resolve: async function (root, _, ctx, info) {
      // debug('ctx.storage.token == undefined', ctx.storage.token, typeof ctx.storage.token, ctx.storage.token === undefined)
      if (ctx.storage.token == undefined) {
        debug('Missing Authorization token')
        return false
      } else {
        try {
          // debug('ctx.storage.token', ctx.storage.token)
          return !!jwt.verify(ctx.storage.token, process.env.JWT_SECRET)
        } catch (error) {
          debug(error)
          // debug("token", ctx.storage.token);
          // debug("process.env.JWT_SECRET", process.env.JWT_SECRET);
          return false
        }
      }
      // debug("ctx.storage.token", ctx.storage.token, process.env.JWT_SECRET);
      // debug("process.env.JWT_SECRET", process.env.JWT_SECRET);
    },
    collections: {
      _auth: {
        method_name_as_key: true
      }
    }
  },
  decode: {
    type: s.JSON,
    resolve: async function (root, _, ctx, info) {
      if (!ctx.storage.token) {
        throw new Error('Missing Authorization token')
      }
      return ctx.utils.jwt.decode(ctx.storage.token)
    },
    collections: {
      _auth: {
        method_name_as_key: true
      }
    }
  }
})

// module.exports = {
//   sign: {
//     type: s.String,
//     resolve: async function(root, _, ctx, info) {
//       return ctx.rsv.auth.jwtGenerate({ token: ctx.storage.token, secret: process.env.JWT_SECRET });
//     }
//   },
//   roles: {
//     type: t.List(s.String),
//     resolve: async function(root, _, ctx, info) {
//       return ctx.rsv.binding.myself({}, `rights { roles_codes }`);
//     }
//   }
// verify: {
//   type: s.Boolean,
//   args: {
//     id: t.Def(s.ID),
//     password: t.Def(s.String)
//   },
//   resolve: async function(root, { id, password }, ctx, info) {
//     return ctx.rsv.auth.verify({ id, password });
//   }
// },
// hasIdentity: {
//   type: s.Boolean,
//   resolve: async function(root, {}, ctx, info) {
//     return ctx.rsv.auth.hasIdentity();
//   }
// },
// identity: {
//   type: s.FaunaRef,
//   resolve: async function(root, {}, ctx, info) {
//     return ctx.rsv.auth.identity();
//   }
// }
// };
