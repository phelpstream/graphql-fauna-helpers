module.exports = ({ rg, resolverGenerators }) => ({
  // Mutations
  userAddRole({ id, input }, ctx) {},
  userAddPermission({ id, input }) {},
  userRemoveRole({ id, input }) {},
  userRemovePermission({ id, input }) {},
  // Queries
  verify({ id, password }) {},
  hasIdentity() {},
  identity() {},
  checkAccess() {},
  jwtGenerate: async function({ data }, ctx) {
    return ctx.fm.jwtGenerate({ data, secret: process.env.JWT_SECRET });
  }
});
