module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r }) => ({
  userAddRole: {
    type: e.User.type,
    args: {
      id: t.Def(s.ID),
      role: t.Def(e.UserRole.input)
    },
    resolve: async function(root, { id, role }, ctx, info) {
      ctx.rsv.auth.userAddRole({ id, role });
    }
  },
  userAddPermission: {
    type: e.User.type,
    args: {
      id: t.Def(s.ID),
      role: t.Def(e.UserPermission.input)
    },
    resolve: async function(root, { id, permission }, ctx, info) {
      ctx.rsv.auth.userAddPermission({ id, permission });
    }
  },
  // userRemoveRole: {},
  // userRemovePermission: {}
});
