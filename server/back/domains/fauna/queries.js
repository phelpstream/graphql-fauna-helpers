const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:fauna:queries]')

const { get } = require('svpr')
const { is } = require('ramda')
const indexesGenerators = require('./../../indexes/generators')
const { buildIndexes } = require('./../../../fauna/builders/indexes')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  fauna_indexes_build: {
    type: s.JSON,
    resolve: async function(root, _, ctx, info) {
      let domainsList = ctx.domains.list
      domainsList.map(domains => {
        // debug(domains)
        Object.entries(domains).map(async ([domain_name, domain]) => {
          // debug('domain_name', domain_name)
          if (is(Function, domain.indexes)) {
            Object.entries(domain.indexes({ ig: indexesGenerators })).map(async ([class_name, indexes]) => {
              await buildIndexes(indexes, class_name, process.env.FAUNA_KEY)
              debug('indexes.length', indexes.length)
            })
          }
        })
      })

      return {
        done: true
      }
    }
  }
})
