const c = require("./collections");
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({
    instanceName: "userRole",
    entity: e.UserRole,
    domain: "userRole",
    collections: {
      userRoles: c.userRoles
    },
    includeOwn: false
  })
});
