const {get} = require('svpr')
const c = require('./collections');
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...qg.R({
    instanceName: 'userRole',
    entity: e.UserRole,
    domain: 'userRole',
    collections: {
      userRoles: c.userRoles
    },
    includeOwn: false
  }),
  userRolesFromCodes: {
    type: t.List(e.UserRole.type),
    args: {
      roles_codes: t.Def(t.List(s.String))
    },
    resolve: async function (root, { roles_codes }, ctx, info) {
      return ctx.rsv.userRole.userRoleMatch({ index: 'userRoles_by_code', matchValues: roles_codes }, ctx)
    },
    _collections: {
      usersAbstracts: c.usersAbstracts
    }
  }
});
