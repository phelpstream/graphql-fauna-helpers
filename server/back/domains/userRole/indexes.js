module.exports = ({ ig, indexGenerators }) => ({
  userRoles: [
    ...ig("userRoles"),
    {
      name: "userRoles_by_code",
      terms: [{ field: ["data", "code"] }],
      unique: true
    }
  ]
});
