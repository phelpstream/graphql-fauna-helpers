const c = require("./collections");
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({
    instanceName: "userSegment",
    entity: e.UserSegment,
    domain: "userSegment",
    collections: {
      userSegments: c.userSegments
    },
    includeOwn: false
  })
});
