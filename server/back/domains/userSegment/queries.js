const c = require('./collections')
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...qg.R({
    instanceName: 'userSegment',
    entity: e.UserRole,
    domain: 'userSegment',
    collections: {
      userSegments: c.userSegments
    },
    includeOwn: false
  })
})
