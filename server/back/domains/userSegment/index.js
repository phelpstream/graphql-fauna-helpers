module.exports = {
  firewall: require("./firewall"),
  indexes: require("./indexes"),
  mutations: require("./mutations"),
  queries: require("./queries"),
  resolvers: require("./resolvers")
};
