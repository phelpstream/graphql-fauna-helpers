const c = require("./collections");
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  // namings: {
  //   type: s.JSON, //t.List(e.Currency.type),
  //   args: {
  //     name: t.Def(s.String)
  //   },
  //   resolve: async function(root, { name = "" }, ctx, info) {
  //     return Object.entries(ctx.namings).reduce((obj, [key, nameFn]) => {
  //       obj[key] = nameFn(name);
  //       return obj;
  //     }, {});
  //   },
  //   _collections: {
  //     resources: {
  //       no_state: true
  //     }
  //   }
  // },
  currencies: {
    type: s.JSON, //t.List(e.Currency.type),
    resolve: async function(root, {}, ctx, info) {
      return r.currencies.list();
    },
    _collections: {
      resources: {
        method_name_as_key: true,
        not_a_list: false,
        key: "code"
      }
    }
  },
  countries: {
    type: s.JSON, //t.List(e.Country.type),
    resolve: async function(root, {}, ctx, info) {
      return r.countries.list();
    },
    _collections: {
      resources: {
        method_name_as_key: true,
        not_a_list: false,
        key: "country_code"
      }
    }
  },
  timezones: {
    type: s.JSON, //t.List(enu.TimeZone),
    resolve: async function(root, {}, ctx, info) {
      return r.timezones.list();
    },
    _collections: {
      resources: {
        method_name_as_key: true,
        not_a_list: false,
        key: "code"
      }
    }
  },
  languages: {
    type: s.JSON, //t.List(e.Language.type),
    resolve: async function(root, {}, ctx, info) {
      return r.languages.list();
    },
    _collections: {
      resources: {
        method_name_as_key: true,
        not_a_list: false,
        key: "code"
      }
    }
  },
  genders: {
    type: s.JSON, //t.List(enu.Gender),
    resolve: async function(root, {}, ctx, info) {
      return r.genders.list();
    },
    _collections: {
      resources: {
        method_name_as_key: true,
        not_a_list: false
      }
    }
  }
  // resources: {
  //   type: s.JSON,
  //   args: {
  //     resources: t.Def(t.List(s.String)),
  //     no_default: t.Def(s.Boolean)
  //   },
  //   resolve: async function(root, { resources = [], no_default }, ctx, info) {
  //     let defaultResources = ["currencies", "countries", "timezones", "languages", "genders"];
  //     let allResources = no_default ? resources : [...defaultResources, ...resources];
  //     let resourcesList = await Promise.all(
  //       allResources.map(resource => {
  //         if (ctx.binding.query[resource]) {
  //           return ctx.binding.query[resource]({}, "", { context: ctx });
  //         }
  //       })
  //     );
  //     return resourcesList.reduce((obj, resource, index) => {
  //       if (!resource) return obj;
  //       obj[allResources[index]] = resource;
  //       return obj;
  //     }, {});
  //   },
  //   _collections: {
  //     resources: {
  //       name: "resources"
  //     }
  //   }
  // }
});
