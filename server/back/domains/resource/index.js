module.exports = {
  indexes: require("./indexes"),
  queries: require("./queries"),
  collections: require("./collections")
};
