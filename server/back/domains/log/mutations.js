const c = require("./collections");
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({ instanceName: "log", entity: e.Log, domain: "log", collections: { logs: c.logs }, includeOwn: false })
});
