const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:algolia:queries]')

const { get } = require('svpr')
const { is } = require('ramda')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  eventDispatch: {
    type: s.JSON,
    // type: e.EventsResponse.type,
    args: {
      input: t.Def(e.EventRequest.input)
    },
    resolve: async function(root, { input = {} }, ctx, info) {
      // ids contains [{ref_id, id}]
      // debug("eventDispatch", input);
      let { name, instanceName, index, idsList, definition } = input
      return ctx.events.sync.emit({
        eventName: name,
        instanceName,
        idsList,
        index, // optional
        definition // optional
      }, ctx)
    }
  }
})
