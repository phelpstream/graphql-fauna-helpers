const c = require('./collections')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...qg.R({ instanceName: 'notification', entity: e.Notification, domain: 'notification', includeOwn: false }),
  // notificationsByCommentedItem: {
  //   type: e.Comment.in_context_list,
  //   args: {
  //     domain: t.Def(s.String),
  //     id: t.Def(t.NonNull(s.String)),
  //     paginateOptions: t.Def(e.PaginateOptions.input)
  //   },
  //   resolve: async function(root, { domain, id, paginateOptions }, ctx, info) {
  //     return ctx.rsv.notification.notifications({ index: 'notifications_by_notificationed_item', matchValue: [domain, id], paginateOptions }, ctx)
  //   },
  //   _collections: {
  //     notifications: c.notifications
  //   }
  // }
})
