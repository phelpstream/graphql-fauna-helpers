module.exports = {
  indexes: require("./indexes"),
  mutations: require("./mutations"),
  queries: require("./queries"),
  resolvers: require("./resolvers"),
  collections: require("./collections")
};
