const c = require('./collections')
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({ instanceName: 'notification', entity: e.Notification, domain: 'notification', collections: { notifications: c.notifications }, includeOwn: true })
})
