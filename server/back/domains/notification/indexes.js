module.exports = ({ ig, indexGenerators }) => ({
  notifications: [
    ...ig('notifications'),
    {
      name: 'notifications_by_for_user_id',
      terms: [{ field: ['data', 'for_user_id'] }]
    },
    {
      name: 'notifications_by_for_user_id_and_seen',
      terms: [{ field: ['data', 'replies_to'] }, { field: ['data', 'seen_by_user'] }]
    }
  ]
})
