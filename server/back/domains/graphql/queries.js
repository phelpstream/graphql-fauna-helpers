const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:graphql:queries]')

const { get } = require('svpr')
const c = require('./collections')

const resourcesResolve = async function(root, {}, ctx, info) {
  let resourceMethods = get(`graphql.methodsByDomain.resource`, ctx)

  return Promise.all(
    resourceMethods.map(method => {
      if (ctx.binding.query[method]) return ctx.binding.query[method]({}, null, { context: ctx })
      return null
    })
  ).then(data =>
    data.reduce((obj, resource, index) => {
      obj[resourceMethods[index]] = resource
      return obj
    }, {})
  )
}

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => {
  return {
    initiate: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        let _resources = await resourcesResolve(root, {}, ctx)
        return {
          _version: get('schema.version', ctx),
          _methodsArgumentTypes: get('graphql.methodsArgumentTypes', ctx),
          _collections: get('graphql.collections', ctx),
          _methodsCollections: ctx.graphql.methods_collections,
          _definitions: get('graphql.definitions', ctx),
          _methodTypes: get('graphql.methodTypes', ctx),
          _defaultValuesMap: get('graphql.defaultValuesMap', ctx),
          _methodsNames: get('graphql.methodsNames', ctx),
          _inputTypeFilters: get('graphql.inputTypeFilters', ctx),
          _resources
        }
      },
      _collections: {
        graphql: c.graphql
      }
    },
    version: {
      type: e.SchemaVersion.type,
      resolve: async function(root, {}, ctx, info) {
        return get('schema.version', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    versionDispatch: {
      type: e.SchemaVersion.type,
      resolve: async function(root, {}, ctx, info) {
        return ctx.graphql.dispatch('version')
      },
      _collections: {
        graphql: c.graphql
      }
    },
    collections: {
      type: t.List(e.GraphQLCollection.type),
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.collections', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    methodsCollections: {
      type: t.List(e.GraphQLMethodsCollectionsList.type),
      resolve: async function(root, {}, ctx, info) {
        return ctx.graphql.methods_collections_list()
      },
      _collections: {
        graphql: c.graphql
      }
    },
    methodsCollectionsByName: {
      type: e.GraphQLCollection.type,
      args: {
        name: t.Def(s.String)
      },
      resolve: async function(root, { name }, ctx, info) {
        return ctx.graphql.methods_collections[name][0]
      },
      _collections: {
        graphql: c.graphql
      }
    },
    definitions: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.definitions', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    defaultValuesMap: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.defaultValuesMap', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    defaultValues: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.defaultValues', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    methodsArgumentTypes: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.methodsArgumentTypes', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    methodTypes: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.methodTypes', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    methodsNames: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.methodsNames', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    inputTypeFilters: {
      type: s.JSON,
      resolve: async function(root, {}, ctx, info) {
        return get('graphql.inputTypeFilters', ctx)
      },
      _collections: {
        graphql: c.graphql
      }
    },
    resources: {
      type: s.JSON,
      resolve: resourcesResolve,
      _collections: {
        graphql: c.graphql
      }
    },
    run_method: {
      type: s.JSON,
      args: {
        // input: t.Def(s.JSON)
        input: t.Def(
          t.Input('RunMethod', {
            mutation: t.Def(s.Boolean),
            query: t.Def(s.Boolean),
            method: t.Def(s.String),
            args: t.Def(s.JSON),
            definition: t.Def(s.String),
            // typeName: t.Def(s.String),
            max_recursion: t.Def(s.Int),
            recursion_count: t.Def(s.Int)
          })
        )
      },
      resolve: async function(root, { input }, ctx, info) {
        let { query, mutation, method, args, definition, max_recursion, recursion_count } = input
        debug('run_method', input)
        let typeName = ctx.graphql.methodType(method)
        const bindingType = query ? 'query' : mutation ? 'mutation' : undefined
        if (bindingType) {
          try {
            // debug("ctx.binding", ctx.binding);
            let localeDefinition = definition || typeName ? ctx.graphql.definition(typeName) : undefined
            // debug("localeDefinition", localeDefinition, "for", method, "with", typeName)
            debug("input", JSON.stringify(input, null, 2))
            return ctx.binding[bindingType][method](args, localeDefinition, { context: ctx })
              // .then(res => {
              //   debug('res', res)
              //   return res
              // })
              // .catch(err => debug("err", err))
          } catch (error) {
            debug('run_method error', error)
          }
        }
      }
    }
  }
}
