module.exports = {
  queries: require("./queries"),
  collections: require("./collections")
};
