const c = require('./collections')
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({ instanceName: 'comment', entity: e.Comment, domain: 'comment', collections: { comments: c.comments }, includeOwn: true })
})
