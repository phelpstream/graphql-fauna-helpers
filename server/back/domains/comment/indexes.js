module.exports = ({ ig, indexGenerators }) => ({
  comments: [
    ...ig('comments'),
    {
      name: 'comments_by_commented_item',
      terms: [{ field: ['data', 'commented_domain'] }, { field: ['data', 'commented_id'] }]
    },
    {
      name: 'comments_by_replies_to',
      terms: [{ field: ['data', 'replies_to'] }]
    }
  ]
})
