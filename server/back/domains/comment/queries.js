const c = require('./collections')
const {get} = require('svpr')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...qg.R({ instanceName: 'comment', entity: e.Comment, domain: 'comment', includeOwn: true }),
  commentsByCommentedItem: {
    type: e.Comment.in_context_list,
    args: {
      domain: t.Def(s.String),
      id: t.Def(t.NonNull(s.String)),
      paginateOptions: t.Def(e.PaginateOptions.input)
    },
    resolve: async function(root, { domain, id, paginateOptions }, ctx, info) {
      
      return ctx.binding.query.comments({ index: 'comments_by_commented_item', matchValue: [domain, id] }, info, { context: ctx })
      // .then(get('data'))
      // return ctx.rsv.comment.comments({ index: 'comments_by_commented_item', matchValue: [domain, id], paginateOptions }, ctx)
    },
    _collections: {
      comments: c.comments
    }
  }
})
