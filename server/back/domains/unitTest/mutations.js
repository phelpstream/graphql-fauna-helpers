const c = require("./collections");
module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...mg.CUD({ instanceName: "unitTest", entity: e.UnitTest, domain: "unitTest", collections: { users: c.unitTests }, includeOwn: true })
});
