const Debug = require('debug')
const debug = Debug('gfh-server [back:domains:unitTest:queries]')

const c = require('./collections')

const fauna = require('faunadb')

module.exports = ({ types, t, scalars, s, entities, e, enums, enu, resources, r, mutationGenerators, mg, queryGenerators, qg }) => ({
  ...qg.R({ instanceName: 'unitTest', entity: e.UnitTest, domain: 'unitTest', includeOwn: true }),
  faunaTest: {
    type: s.JSON,
    resolve: async function(root, _, ctx, info) {
      // console.time('faunaTest')
      // const client = new fauna.Client({
      //   secret: process.env.FAUNA_KEY
      // })
      // let res = await client.query(ctx.q.Get(ctx.q.Select(['data', 0], ctx.q.Paginate(ctx.q.Match(ctx.q.Index('users_by_id'), ['f6587e17-389d-48d1-a39b-e857da580e56']), { size: 100 }))))
      // console.timeEnd('faunaTest')
      // return res
    }
  },
  objectTest: {
    type: t.Object('ObjectTest', {
      name: t.Def(s.String),
      properties: t.Def(
        t.Object('ObjectTest_Properties', {
          first: t.Def(s.Boolean),
          ok: t.Def(s.String)
        })
      )
    }),
    resolve: function(source, _, ctx, info) {
      return {
        name: 'Gabin',
        properties: null
        //  {
        //   // first: true,
        //   // ok: 'maybe'
        // }
      }
    }
  }
})
