const { is } = require('ramda')
const { lower } = require('svpr')
const currenciesList = require('./currencies.json')

const mustBeObject = o => (o && is(Object, o) ? o : {})

module.exports = {
	list: () => currenciesList,
	getCurrency: code => mustBeObject(is(String, code) ? currenciesList.find(c => lower(c.code) === lower(code)) : undefined)
}
