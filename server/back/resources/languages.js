const { is } = require('ramda')
const { lower } = require('svpr')
const languagesList = require('./languages.json')

const mustBeObject = o => (o && is(Object, o) ? o : {})

module.exports = {
	list: () => languagesList,
	getLanguage: code => mustBeObject(is(String, code) ? languagesList.find(c => lower(c.code) === lower(code)) : undefined)
}
