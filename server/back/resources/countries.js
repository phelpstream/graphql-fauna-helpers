const { is } = require('ramda')
const { lower } = require('svpr')
const countriesList = require('./countries.json')

const mustBeObject = o => (o && is(Object, o) ? o : {})

module.exports = {
	list: () => countriesList,
	getCountryByName: name => mustBeObject(name && is(String, name) ? countriesList.find(c => lower(c.country_name) === lower(name)) : undefined),
	getCountry: iso2 => mustBeObject(iso2 && is(String, iso2) ? countriesList.find(c => lower(c.iso2) === lower(iso2)) : undefined),
	getCountryByPhoneCode: phone_code => mustBeObject(phone_code && is(String, phone_code) ? countriesList.find(c => `${c.phone_code}` === `${phone_code}`) : undefined)
}
