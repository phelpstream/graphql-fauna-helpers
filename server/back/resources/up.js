const fs = require("fs");

fs.writeFileSync(
  "./languages-up.json",
  JSON.stringify(
    Object.entries(require("./languages.json")).map(
      ([key, val]) => {
        val.code = key;
        return val;
      },
      null,
      2
    )
  )
);
