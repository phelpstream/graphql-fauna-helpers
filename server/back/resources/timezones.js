const { is } = require('ramda')
const { lower } = require('svpr')
const timezonesList = require('./timezones.json')
const mustBeObject = o => (o && is(Object, o) ? o : {})
module.exports = {
	list: () => timezonesList,
	getTimeZone: code => mustBeObject(is(String, code) ? timezonesList.find(tz => lower(tz.code) === lower(code)) : undefined)
}
