module.exports = {
  countries: require("./resources/countries"),
  currencies: require("./resources/currencies"),
  timezones: require("./resources/timezones"),
  languages: require("./resources/languages"),
  genders: require("./resources/genders")
};
