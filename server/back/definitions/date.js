module.exports = (field, { t, s, enu } = {}) => ({
  args: {
    locale: t.Def(enu.LanguageCode, { default: "en" }),
    format: t.Def(s.String, { default: "L" })
  },
  resolve: (source, { format, locale }, ctx) => {
    return ctx.utils.dates.formattedDate(source[field], { format, locale });
  }
});
