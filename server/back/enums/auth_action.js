const t = require("./../../graphql/type");

module.exports = t.Enum("AuthAction", ["create", "read", "write", "delete", "soft_delete"]);
