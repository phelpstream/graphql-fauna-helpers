const countries = require("../resources/countries.json");
const t = require("./../../graphql/type");

module.exports = t.Enum("CountryCodeISO2", countries.map(c => c.iso2));
