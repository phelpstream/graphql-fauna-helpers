const t = require("./../../graphql/type");

module.exports = t.Enum("AuthScope", ["any", "own"]);
