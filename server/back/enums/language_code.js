const languages = require("../resources/languages.json");
const t = require("./../../graphql/type");

module.exports = t.Enum("LanguageCode", languages.map(l => l.code));
