const t = require("./../../graphql/type");

module.exports = t.Enum("SocialReaction", ["like", "dislike", "seen"]);
