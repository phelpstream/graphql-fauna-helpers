const currencies = require("../resources/currencies.json");
const t = require("./../../graphql/type");

module.exports = t.Enum("CurrencyCode", currencies.map(c => c.code));
