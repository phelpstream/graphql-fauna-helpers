const timezones = require("../resources/timezones");
const t = require("./../../graphql/type");

module.exports = t.Enum("TimeZone", timezones.list().map(tz => tz.code));
