const t = require('../../graphql/type')
const s = require('./../scalars')

// IS INDEX

const InstanceLog = require('./instance_log')

const ResultRequests = require('./result_requests')
const ResultErrors = require('./result_errors')
const ResultDataContext = require('./result_data_context')
const ResultEvents = require('./result_events')
const IndexCount = require('./index_count')
const InstanceDefault = require('./instance_default_value')
const namings = require('./../namings')

const type = {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),

  action: t.Def(s.String),
  args: t.Def(s.JSON),
  ref: t.Def(s.String), //t.Def(s.FaunaRef),
  ts: t.Def(s.Int),
  is_error: t.Def(s.Boolean),

  log: t.Def(InstanceLog.type)
}

const input = {
  action: t.Def(s.String),
  args: t.Def(s.JSON),
  ref: t.Def(s.String), //t.Def(s.FaunaRef),
  ts: t.Def(s.Int),
  is_error: t.Def(s.Boolean)
}

module.exports = t.Entity('Log', {
  objectDefinition: type,
  inputDefinition: input,
  contextType: ResultDataContext.type,
  requestsType: ResultRequests.type,
  errorsType: ResultErrors.type,
  eventsType: ResultEvents.type,
  countType: IndexCount.type,
  instanceDefaultType: InstanceDefault.type,
  namings
})
