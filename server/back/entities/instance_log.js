const Debug = require('debug')
const debug = Debug('gfh-server [back:entities:instance_log]')

const { max, isNil, is, reduce } = require('ramda')
const { get } = require('svpr')

const t = require('../../graphql/type')
const s = require('./../scalars')
const enu = require('./../enums')
const definitions = require('./../definitions')

const Person = require('./person')

const owner_changed_at_ts_resolver = ({ owner_changed_at }, _, ctx) => {
  return ctx.date.ts(owner_changed_at)
}

const created_at_ts_resolver = ({ created_at }, _, ctx) => {
  return ctx.date.ts(created_at)
}

const updated_at_ts_resolver = ({ updated_at }, _, ctx) => {
  return ctx.date.ts(updated_at)
}

const replaced_at_ts_resolver = ({ replaced_at }, _, ctx) => {
  return ctx.date.ts(replaced_at)
}

const deleted_at_ts_resolver = ({ deleted_at }, _, ctx) => {
  return ctx.date.ts(deleted_at)
}

const archived_at_ts_resolver = ({ archived_at }, _, ctx) => {
  return ctx.date.ts(archived_at)
}

const imported_at_ts_resolver = ({ imported_at }, _, ctx) => {
  return ctx.date.ts(imported_at)
}

const type = t.Object('InstanceLog', {
  last_action_ts: t.Def(s.Long, {
    resolve: (source, _, ctx) => {
      const run = fn => fn(source, _, ctx)
      let results = [owner_changed_at_ts_resolver, created_at_ts_resolver, updated_at_ts_resolver, deleted_at_ts_resolver, archived_at_ts_resolver, imported_at_ts_resolver].map(run)
      // debug('last_action_ts: results', results)
      let filteredResults = results.filter(x => !isNil(x) || !isNaN(x))
      // debug('last_action_ts: filteredResults', filteredResults)
      let maxResult = reduce(max, -Infinity, filteredResults)
      // debug('last_action_ts: maxResult', maxResult)
      return is(Number, maxResult) && maxResult > -Infinity ? maxResult : undefined
    }
  }),
  // Owned
  owned_by_id: t.Def(s.ID),
  owned_by: t.Def(Person.type, {
    resolve: ({ owned_by_id }, _, ctx) =>
      ctx.utils.if(owned_by_id, () => ctx.binding.query.userById({ id: owned_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data')))
  }),
  // Owned events
  owner_changed_by_id: t.Def(s.ID),
  owner_changed_at: t.Def(s.String),
  owner_changed_at_ts: t.Def(s.Long, {
    resolve: owner_changed_at_ts_resolver
  }),
  // owner_changed_at_date: t.Def(s.String), //t.Def(s.DateTime)
  owner_changed_at_formatted: t.Def(s.String, definitions.date('owner_changed_at', { t, s, enu })),
  owner_changed_by: t.Def(Person.type, {
    resolve: ({ owner_changed_by_id }, _, ctx) =>
      ctx.utils.if(owner_changed_by_id, () =>
        ctx.binding.query.userById({ id: owner_changed_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),
  // Created
  created_at: t.Def(s.String),
  created_at_ts: t.Def(s.Long, {
    resolve: created_at_ts_resolver
  }),
  created_at_formatted: t.Def(s.String, definitions.date('created_at', { t, s, enu })),
  created_by_id: t.Def(s.ID),
  created_by: t.Def(Person.type, {
    resolve: ({ created_by_id }, _, ctx) =>
      ctx.utils.if(created_by_id, () =>
        ctx.binding.query.userById({ id: created_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),
  // Update
  updated_at: t.Def(s.String),
  updated_at_ts: t.Def(s.Long, {
    resolve: updated_at_ts_resolver
  }),
  updated_at_formatted: t.Def(s.String, definitions.date('updated_at', { t, s, enu })),
  updated_by_id: t.Def(s.ID),
  updated_by: t.Def(Person.type, {
    resolve: ({ updated_by_id }, _, ctx) =>
      ctx.utils.if(updated_by_id, () =>
        ctx.binding.query.userById({ id: updated_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),
  // Replace
  replaced_at: t.Def(s.String),
  replaced_at_ts: t.Def(s.Long, {
    resolve: replaced_at_ts_resolver
  }),
  replaced_at_formatted: t.Def(s.String, definitions.date('replaced_at', { t, s, enu })),
  replaced_by_id: t.Def(s.ID),
  replaced_by: t.Def(Person.type, {
    resolve: ({ replaced_by_id }, _, ctx) =>
      ctx.utils.if(replaced_by_id, () =>
        ctx.binding.query.userById({ id: replaced_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),
  // Deleted
  deleted_at: t.Def(s.String),
  deleted_at_ts: t.Def(s.Long, {
    resolve: deleted_at_ts_resolver
  }),
  deleted_at_formatted: t.Def(s.String, definitions.date('deleted_at', { t, s, enu })),
  deleted_by_id: t.Def(s.ID),
  deleted: t.Def(s.Boolean, { default: false }),
  deleted_by: t.Def(Person.type, {
    resolve: ({ deleted_by_id }, _, ctx) =>
      ctx.utils.if(deleted_by_id, () =>
        ctx.binding.query.userById({ id: deleted_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),
  // Archived
  archived_at: t.Def(s.String),
  archived_at_ts: t.Def(s.Long, {
    resolve: archived_at_ts_resolver
  }),
  archived_at_formatted: t.Def(s.String, definitions.date('archived_at', { t, s, enu })),
  archived_by_id: t.Def(s.ID),
  archived: t.Def(s.Boolean, { default: false }),
  archived_by: t.Def(Person.type, {
    resolve: ({ archived_by_id }, _, ctx) =>
      ctx.utils.if(archived_by_id, () =>
        ctx.binding.query.userById({ id: archived_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),

  // Imported
  imported: t.Def(s.Boolean, { default: false }),
  imported_at: t.Def(s.String),
  imported_at_ts: t.Def(s.Long, {
    resolve: imported_at_ts_resolver
  }),
  imported_at_formatted: t.Def(s.String, definitions.date('imported_at', { t, s, enu })),
  imported_by_id: t.Def(s.ID),
  imported_by: t.Def(Person.type, {
    resolve: ({ imported_by_id }, _, ctx) =>
      ctx.utils.if(imported_by_id, () =>
        ctx.binding.query.userById({ id: imported_by_id }, `{data {id profile {picture identity {first_name last_name full_name}}}}`, { context: ctx }).then(get('data'))
      )
  }),

  // Context data
  ts: t.Def(s.Long)
  // _ref: t.Def(s.Any) //t.Def(s.FaunaRef)
})

const input = t.Input('InstanceLog', {
  // // Owned
  owned_by_id: t.Def(s.ID),

  // // Owned events
  // owner_changed_by_id: t.Def(s.ID),
  // owner_changed_at: t.Def(s.String),

  // // Created
  created_at: t.Def(s.String),
  created_by_id: t.Def(s.ID),

  // // Update
  updated_at: t.Def(s.String),
  updated_by_id: t.Def(s.ID),

  // // Deleted
  // deleted_at: t.Def(s.String),
  // deleted_by_id: t.Def(s.ID),
  // deleted: t.Def(s.Boolean),

  // // Archived
  // archived_at: t.Def(s.String),
  // archived_by_id: t.Def(s.ID),
  // archived: t.Def(s.Boolean)

  _empty: t.Def(s.String)
})

const helper = {
  own: (user_id, log = {}) => {
    log.owned_by_id = user_id || log.owned_by_id
    return log
  },
  ownershipTo: ({ user_id, to } = {}, log = {}) => {
    log.owner_changed_at = new Date().toISOString()
    log.owner_changed_by_id = user_id
    log.owned_by_id = to
    return log
  },
  create: (user_id, log = {}) => {
    log.created_at = new Date().toISOString()
    log.created_by_id = user_id
    return log
  },
  import: (user_id, log = {}) => {
    log.imported = true
    log.imported_at = new Date().toISOString()
    log.imported_by_id = user_id || log.created_by_id
    return log
  },
  replace: (user_id, log = {}) => {
    log.updated_at = new Date().toISOString()
    log.updated_by_id = user_id || log.updated_by_id
    return log
  },
  update: (user_id, log = {}) => {
    log.updated_at = new Date().toISOString()
    log.updated_by_id = user_id || log.updated_by_id
    return log
  },
  delete: (user_id, log = {}) => {
    log.deleted_at = new Date().toISOString()
    log.deleted_by_id = user_id || log.deleted_by_id
    log.deleted = true
    return log
  },
  archive: (user_id, log = {}) => {
    log.archived_at = new Date().toISOString()
    log.archived_by_id = user_id || log.archived_by_id
    log.archived = true
    return log
  }
}

module.exports = { type, input, helper }
