const t = require('../../graphql/type')
const s = require('./../scalars')

const PersonBanking = require('./person_banking')
const PersonProfile = require('./person_profile')
const PersonDocuments = require('./person_documents')
const UserRights = require('./user_rights')
const UserIdentification = require('./user_identification')
const UserClassification = require('./user_classification')
const UserSponsorships = require('./user_sponsorships')
const InstanceLog = require('./instance_log')

const ResultRequests = require('./result_requests')
const ResultErrors = require('./result_errors')
const ResultDataContext = require('./result_data_context')
const ResultEvents = require('./result_events')
const IndexCount = require('./index_count')
const InstanceDefault = require('./instance_default_value')
const namings = require('./../namings')

// IS INDEX

const type = {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),
  profile: t.Def(PersonProfile.type, { default: {} }),
  documents: t.Def(PersonDocuments.type, { default: {} }),
  banking: t.Def(PersonBanking.type),
  rights: t.Def(UserRights.type, { default: {} }),
  metadata: t.Def(s.JSON),
  identification: t.Def(UserIdentification.type),
  classification: t.Def(UserClassification.type),
  sponsorships: t.Def(UserSponsorships.type),
  log: t.Def(InstanceLog.type)
}

const input = {
  profile: t.Def(PersonProfile.input),
  documents: t.Def(PersonDocuments.input),
  banking: t.Def(PersonBanking.input),
  rights: t.Def(UserRights.input),
  metadata: t.Def(s.JSON),
  identification: t.Def(UserIdentification.input),
  classification: t.Def(UserClassification.input),
  sponsorships: t.Def(UserSponsorships.input)
  // log: t.Def(InstanceLog.input) // TEMPP!
}

module.exports = t.Entity('User', {
  objectDefinition: type,
  inputDefinition: input,
  contextType: ResultDataContext.type,
  requestsType: ResultRequests.type,
  errorsType: ResultErrors.type,
  eventsType: ResultEvents.type,
  countType: IndexCount.type,
  instanceDefaultType: InstanceDefault.type,
  namings
})
