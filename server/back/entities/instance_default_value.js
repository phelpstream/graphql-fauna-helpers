const t = require("../../graphql/type");
const s = require("./../scalars");

const InstanceLog = require("./instance_log");

const type = t.Object("InstanceDefaultValue", {
  type: t.Def(s.String),
  defaultValue: t.Def(s.JSON)
});

const input = t.Input("InstanceDefaultValue", {
  type: t.Def(s.String),
  defaultValue: t.Def(s.JSON)
});

module.exports = { type, input };
