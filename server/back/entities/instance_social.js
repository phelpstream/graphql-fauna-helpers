const t = require('../../graphql/type')
const s = require('./../scalars')

const InstanceSocialMention = require('./instance_social_mention')
const InstanceSocialReaction = require('./instance_social_reaction')

const type = t.Object('InstanceSocial', {
  mentions: t.Def(t.List(InstanceSocialMention.arraylike.type)),
  reactions: t.Def(t.List(InstanceSocialReaction.arraylike.type))
})

const input = t.Input('InstanceSocial', {
  mentions: t.Def(t.List(InstanceSocialMention.arraylike.input), { default: true }),
  reactions: t.Def(t.List(InstanceSocialReaction.arraylike.input), { default: true })
})

module.exports = { type, input }
