const t = require("../../graphql/type");
const s = require("./../scalars");

const CountryCodeEnum = require("./../enums/country_code_iso2");

const type = t.Object("Country", {
  country_code: t.Def(CountryCodeEnum),

  // GENERATED:
  country: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.countries.getCountry(source.country_code).country_name;
    }
  }),
  country_code_iso2: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.countries.getCountry(source.country_code).iso2;
    }
  }),
  country_code_iso3: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.countries.getCountry(source.country_code).iso3;
    }
  }),
  continent: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.countries.getCountry(source.country_code).continent;
    }
  }),
  phone_code: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.countries.getCountry(source.country_code).phone_code;
    }
  }),
  currency: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.countries.getCountry(source.country_code).currency;
    }
  }),
  postal_code_city: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.utils.texts.joinTexts([source.postal_code, source.city]);
    }
  })
});

const input = t.Input("Country", {
  country_code: t.Def(CountryCodeEnum)
});

module.exports = { type, input };
