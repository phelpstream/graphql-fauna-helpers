const t = require('../../graphql/type')
const s = require('./../scalars')

const Document = require('./document')

const type = t.Object('PersonDocuments', {
  id_card: t.Def(t.List(Document.arraylike.type)),
  passport: t.Def(t.List(Document.arraylike.type)),
  family_book: t.Def(t.List(Document.arraylike.type)),
  rib_bic_iban: t.Def(t.List(Document.arraylike.type)),
  juridic_protection: t.Def(t.List(Document.arraylike.type)),
  birth_certificates: t.Def(t.List(Document.arraylike.type)),
  marriage_certificates: t.Def(t.List(Document.arraylike.type)),
  divorce_certificates: t.Def(t.List(Document.arraylike.type)),
  tax_documents: t.Def(t.List(Document.arraylike.type)),
  social_security_card: t.Def(t.List(Document.arraylike.type)),
  curriculum_vitae: t.Def(t.List(Document.arraylike.type)),
  medical_records: t.Def(t.List(Document.arraylike.type))
})

const input = t.Input('PersonDocuments', {
  id_card: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  passport: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  family_book: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  rib_bic_iban: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  juridic_protection: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  birth_certificates: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  marriage_certificates: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  divorce_certificates: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  tax_documents: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  social_security_card: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  curriculum_vitae: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] }),
  medical_records: t.Def(t.NonNull(t.List(Document.arraylike.input)), { default: [] })
})

module.exports = { type, input }
