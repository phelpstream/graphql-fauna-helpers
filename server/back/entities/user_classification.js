const t = require('../../graphql/type')
const s = require('./../scalars')

const UserSegment = require('./user_segment')
const { ArrayLikeItem, ArrayLike_ID } = require('./arraylike')

const type = t.Object('UserClassification', {
  segment_ids: t.Def(t.List(ArrayLike_ID.type)),

  segments: t.Def(t.List(UserSegment.type), {
    resolve: ({ roles_code }, _, ctx) => {
      if (roles_code) {
        return ctx.binding.query.userRolesFromCodes({ roles_codes: source.roles_code }, ctx.graphql.definition(UserRole.in_context_list.name), { context: ctx }).then(get('data'))
      }
      return []
    }
  })
})

const input = t.Input('UserClassification', {
  segment_ids: t.Def(t.List(ArrayLike_ID.input))
})

module.exports = { type, input }
