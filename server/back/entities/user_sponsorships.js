const t = require('../../graphql/type')
const s = require('./../scalars')

const UserSponsorCode = require('./user_sponsor_code')

const type = t.Object('UserSponsorships', {
  // Onw codes
  codes: t.Def(t.List(UserSponsorCode.arraylike.type)),

  // Sponsor codes
  sponsor: t.Def(UserSponsorCode.arraylike.type),
  sponsors: t.Def(t.List(UserSponsorCode.arraylike.type)),

  // Generated
  codes_extended: t.Def(t.List(UserSponsorCode.arraylike.type), {
    resolve: ({ codes }, _, ctx, info) => {
      // fetch them
    }
  }),

  sponsorees: t.Def(t.List(s.String), {
    resolve: (source, _, ctx, info) => {
      // fetch them
    }
  })
})

const input = t.Input('UserSponsorships', {
  codes: t.Def(t.List(s.String)),
  sponsor: t.Def(UserSponsorCode.arraylike.input),
  sponsors: t.Def(t.List(UserSponsorCode.arraylike.input))
})

module.exports = { type, input }
