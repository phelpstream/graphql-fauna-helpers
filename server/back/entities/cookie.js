
const t = require("../../graphql/type");
const s = require("./../scalars");

const InstanceLog = require("./instance_log");

const type = t.Object("Cookie", {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),
  key: t.Def(s.String),
  value: t.Def(s.String),
  log: t.Def(InstanceLog.type)
});

const input = t.Input("Cookie", {
  key: t.Def(s.String),
  value: t.Def(s.String)
});

module.exports = { type, input };
