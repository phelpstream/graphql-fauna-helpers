
const t = require("../../graphql/type");
const s = require("./../scalars");

const InstanceLog = require("./instance_log");
const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("InstanceSocialMention", {
  mentionee: t.Def(s.ID), //t.Def(s.FaunaRef),
  log: t.Def(InstanceLog.type)
});

const input = t.Input("InstanceSocialMention", {
  mentionee: t.Def(s.ID), //t.Def(s.FaunaRef)
});

const arraylike = ArrayLikeItem("InstanceSocialMention", { type, input })


module.exports = { type, input, arraylike };
