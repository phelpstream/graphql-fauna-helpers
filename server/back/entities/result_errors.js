const t = require('../../graphql/type')
const s = require('./../scalars')

const ErrorEntity = require('./error')

const type = t.Object('ResultErrors', {
  list: t.Def(t.List(ErrorEntity.arraylike.type)),
  exists: t.Def(s.Boolean, {
    resolve: ({ list = [] }, _, ctx) => {
      return list.length > 0
    }
  })
})

const input = t.Input('ResultErrors', {
  list: t.Def(t.List(ErrorEntity.arraylike.input))
})

module.exports = { type, input }
