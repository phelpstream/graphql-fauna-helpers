const t = require('../../graphql/type')
const s = require('./../scalars')

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('ResultRequest', {
  method: t.Def(s.String),
  variables: t.Def(s.JSON)
})

const input = t.Input('ResultRequest', {
  method: t.Def(s.String),
  variables: t.Def(s.JSON)
})

const arraylike = ArrayLikeItem('ResultRequest', { type, input })

module.exports = { type, input, arraylike }
