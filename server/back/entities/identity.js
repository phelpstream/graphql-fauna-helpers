const t = require("../../graphql/type");
const s = require("./../scalars");

const GenderEnum = require("./../enums/gender");

const type = t.Object("Identity", {
  nickname: t.Def(s.String),
  first_name: t.Def(s.String),
  last_name: t.Def(s.String),
  birthday: t.Def(s.String), //s.Date),

  // ENUMS:
  gender: t.Def(GenderEnum),

  //GENERATED:
  first_name_formatted: t.Def(s.String, {
    resolve: ({ first_name }, _, ctx) => {
      return ctx.utils.texts.capitalize(first_name);
    }
  }),
  last_name_formatted: t.Def(s.String, {
    resolve: ({ last_name }, _, ctx) => ctx.utils.texts.capitalize(last_name)
  }),
  full_name: t.Def(s.String, {
    resolve: (source, _, ctx) => {
      return ctx.utils.texts.joinTexts([ctx.utils.texts.capitalize(source.first_name), ctx.utils.texts.capitalize(source.last_name)])
    }
  })
});

const input = t.Input("Identity", {
  nickname: t.Def(s.String),
  first_name: t.Def(s.String),
  last_name: t.Def(s.String),
  birthday: t.Def(s.String), //s.Date),
  gender: t.Def(GenderEnum)
});

module.exports = { type, input };
