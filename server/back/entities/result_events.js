const t = require('../../graphql/type')
const s = require('./../scalars')

const EventRequest = require('./event_request')

const type = t.Object('ResultEvents', {
  list: t.Def(t.List(EventRequest.arraylike.type)),
  count: t.Def(s.Int, {
    resolve: ({ list = [] }, _, ctx) => {
      return list.length
    }
  })
})

const input = t.Input('ResultEvents', {
  list: t.Def(t.List(EventRequest.arraylike.input))
})

module.exports = { type, input }
