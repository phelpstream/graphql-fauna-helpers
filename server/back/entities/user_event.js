
const t = require("../../graphql/type");
const s = require("./../scalars");

const InstanceLog = require("./instance_log");

const type = t.Object("Event", {
  id: t.Def(s.ID),
  device_id: t.Def(s.ID), // require('device-uuid')
  user_id: t.Def(s.ID),
  name: t.Def(s.String),
  content: t.Def(s.JSON),
  log: t.Def(InstanceLog.type)
});

const input = t.Input("Event", {
  name: t.Def(s.String),
  content: t.Def(s.JSON)
});

module.exports = { type, input };
