const t = require('../../graphql/type')
const s = require('./../scalars')

const CountryCodeEnum = require('./../enums/country_code_iso2')

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('Phone', {
  number: t.Def(s.String),
  country_code: t.Def(CountryCodeEnum),

  // GENERATED:

  phone_code: t.Def(s.String, {
    resolve: async function ({ country_code }, _, ctx) {
      if (country_code) {
        return ctx.resources.countries.getCountry(country_code).phone_code
      }
    }
  }),
  prefixed_number: {
    type: s.String,
    resolve: ({ number, country_code }, _, ctx) => {
      if (country_code && number) {
        return ctx.utils.texts.joinTexts([ctx.utils.texts.mask('' + ctx.resources.countries.getCountry(country_code).phone_code, '+(#)'), number])
      }
    }
  }
})

const input = t.Input('Phone', {
  number: t.Def(s.String, { default: null }),
  country_code: t.Def(CountryCodeEnum)
})

const arraylike = ArrayLikeItem("Phone", { type, input })

module.exports = { type, input, arraylike }
