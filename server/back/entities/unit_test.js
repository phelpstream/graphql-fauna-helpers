const t = require('../../graphql/type')
const s = require('./../scalars')

// IS INDEX

const InstanceLog = require('./instance_log')

const ResultRequests = require('./result_requests')
const ResultErrors = require('./result_errors')
const ResultDataContext = require('./result_data_context')
const ResultEvents = require('./result_events')
const IndexCount = require('./index_count')
const InstanceDefault = require('./instance_default_value')
const namings = require('./../namings')

const type = {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),

  text: t.Def(s.String),

  log: t.Def(InstanceLog.type)
}

const input = {
  text: t.Def(s.String)
}

module.exports = t.Entity('UnitTest', {
  objectDefinition: type,
  inputDefinition: input,
  contextType: ResultDataContext.type,
  requestsType: ResultRequests.type,
  errorsType: ResultErrors.type,
  eventsType: ResultEvents.type,
  countType: IndexCount.type,
  instanceDefaultType: InstanceDefault.type,
  namings
})
