const t = require('../../graphql/type')
const s = require('./../scalars')

const EventRequest = require('./event_request')
const EventsResponseItem = require('./events_response_item')

const type = t.Object('EventsResponse', {
  events: t.Def(t.List(EventsResponseItem.arraylike.type)),
  request: t.Def(EventRequest.type)
})

const input = t.Input('EventsResponse', {
  _empty: t.Def(t.String)
})

module.exports = { type, input }