const t = require('../../graphql/type')
const s = require('./../scalars')

const type = t.Object('PaginateOptions', {
  ts: t.Def(s.Long), //t.Def(s.DateTime)
  after_cursor: t.Def(s.JSON),
  after: t.Def(s.ID), //t.Def(s.FaunaRef),
  before_cursor: t.Def(s.JSON),
  before: t.Def(s.String),
  size: t.Def(s.Int)
})

const input = t.Input('PaginateOptions', {
  ts: t.Def(s.Long), //t.Def(s.DateTime)
  after_cursor: t.Def(s.JSON),
  after: t.Def(s.ID), //t.Def(s.FaunaRef),
  before_cursor: t.Def(s.JSON),
  before: t.Def(s.String),
  size: t.Def(s.Int)
})

module.exports = { type, input }
