const t = require("./../../graphql/type");
const s = require("./../scalars");

const AuthActionEnum = require("./../enums/auth_action");
const AuthScopeEnum = require("./../enums/auth_scope");

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("UserPermission", {
  resource: t.Def(s.String),
  scope: t.Def(AuthScopeEnum, { value: "any" }),
  action: t.Def(AuthActionEnum),
  attributes: t.Def(s.String, { value: "*" })
});

const input = t.Input("UserPermission", {
  resource: t.Def(t.NonNull(s.String)),
  scope: t.Def(AuthScopeEnum),
  action: t.Def(AuthActionEnum),
  attributes: t.Def(s.String)
});

const arraylike = ArrayLikeItem('UserPermission', { type, input })

module.exports = { type, input, arraylike };
