const { get, getOr } = require('svpr')

const t = require('../../graphql/type')
const s = require('./../scalars')

const UserRole = require('./user_role')
const UserPermission = require('./user_permission')
const { ArrayLike_String } = require('./arraylike')

const type = t.Object('UserRight', {
  roles_codes: t.Def(t.List(ArrayLike_String.type)),

  // permissions: t.Def(t.List(UserPermission.type), { value: [] }),

  // GENERATED:
  roles_codes_csv_list: t.Def(s.String, {
    resolve: ({ roles_code = [] }, _, ctx) => {
      return roles_code.join(', ')
    }
  }),
  roles: t.Def(t.List(UserRole.type), {
    resolve: ({ roles_codes }, _, ctx) => {
      // if (roles_codes) {
      //   return ctx.binding.query.userRolesFromCodes({ roles_codes: roles_codes.map(get('data')) }, ctx.graphql.definition(UserRole.in_context_list.name), { context: ctx }).then(get('data'))
      // }
      return []
    }
  }),
  permissions: t.Def(t.List(UserPermission.type), {
    resolve: (source, _, ctx) => {
      return [
        ...getOr([], 'roles', source).reduce(
          (list, r) =>
            list.push(
              ...r.permissions.map(p => {
                p.role = r
                return p
              })
            ),
          []
        )
      ]
    }
  })
})

const input = t.Input('UserRight', {
  roles_codes: t.Def(t.List(ArrayLike_String.input), { value: [] })
})

module.exports = { type, input }
