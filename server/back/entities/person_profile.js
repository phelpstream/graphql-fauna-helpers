
const t = require("../../graphql/type");
const s = require("./../scalars");

const ContactDetails = require("./contact_details");
const Identity = require("./identity");
const Phone = require("./phone");

const type = t.Object("PersonProfile", {
  picture: t.Def(s.URL),
  identity: t.Def(Identity.type),
  contact_details: t.Def(ContactDetails.type),
  email: t.Def(s.EmailAddress),
  phone: t.Def(Phone.type)
});

const input = t.Input("PersonProfile", {
  picture: t.Def(s.URL),
  identity: t.Def(Identity.input),
  contact_details: t.Def(ContactDetails.input),
  email: t.Def(s.EmailAddress),
  phone: t.Def(Phone.input)
});

module.exports = { type, input };
