const t = require("../../graphql/type");
const s = require("./../scalars");

const TimeZoneEnum = require("./../enums/timezone");

const type = t.Object("TimeZone", {
  code: t.Def(TimeZoneEnum),

  // GENERATED:
  name: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.timezones.getTimeZone(source.code).name;
    }
  })
});

const input = t.Input("TimeZone", {
  code: t.Def(TimeZoneEnum)
});

module.exports = { type, input };
