
const t = require("../../graphql/type");
const s = require("./../scalars");

const type = t.Object("What3Words", {
  address: t.Def(s.String)
});

const input = t.Input("What3Words", {
  address: t.Def(s.String)
});

module.exports = { type, input };
