
const t = require("../../graphql/type");
const s = require("./../scalars");

const LanguageCodeEnum = require("./../enums/language_code");

const type = t.Object("Language", {
  code: t.Def(LanguageCodeEnum),

  // GENERATED:
  name: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.languages.getLanguage(source.code).name;
    }
  }),
  native: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.languages.getLanguage(source.code).native;
    }
  }),
  rtl: t.Def(s.Int, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.languages.getLanguage(source.code).rtl;
    }
  })
});

const input = t.Input("Language", {
  code: t.Def(LanguageCodeEnum)
});

module.exports = { type, input };
