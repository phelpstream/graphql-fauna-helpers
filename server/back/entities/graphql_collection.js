const t = require("../../graphql/type");
const s = require("./../scalars");

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("GraphQLCollection", {
  name: t.Def(s.String),
  key: t.Def(s.String),
  not_a_list: t.Def(s.Boolean, { default: false }),
  method_name_as_key: t.Def(s.Boolean, { default: false })
});

const input = t.Input("GraphQLCollection", {});

const arraylike = ArrayLikeItem("GraphQLCollection", { type, input })

module.exports = { type, input, arraylike };
