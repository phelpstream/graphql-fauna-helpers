const t = require('../../graphql/type')
const s = require('./../scalars')

const Document = require('./document')
const InstanceAuth = require('./instance_auth')
const InstanceLog = require('./instance_log')

const ResultRequests = require('./result_requests')
const ResultErrors = require('./result_errors')
const ResultDataContext = require('./result_data_context')
const ResultEvents = require('./result_events')
const IndexCount = require('./index_count')
const InstanceDefault = require('./instance_default_value')
const namings = require('./../namings')

const { ArrayLike_ID, ArrayLikeItem } = require('./arraylike')

// IS INDEX


const type = {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),

  commented_domain: t.Def(s.ID),
  commented_id: t.Def(s.ID),

  replies_to: t.Def(t.List(ArrayLike_ID.type)), // other comment

  content: t.Def(t.NonNull(s.String)),

  linked_users_ids: t.Def(t.List(ArrayLike_ID.type)),
  seen_by_users_ids: t.Def(t.List(ArrayLike_ID.type)),
  marked_by_users_ids: t.Def(t.List(ArrayLike_ID.type)),

  attached_documents: t.Def(t.List(Document.arraylike.type)),

  auth: t.Def(InstanceAuth.type),
  log: t.Def(InstanceLog.type)
}

const input = {
  commented_domain: t.Def(t.NonNull(s.ID)),
  commented_id: t.Def(t.NonNull(s.ID)),

  replies_to: t.Def(t.List(ArrayLike_ID.input)), // other comment

  content: t.Def(t.NonNull(s.String)),

  linked_users_ids: t.Def(t.List(ArrayLike_ID.input)),
  seen_by_users_ids: t.Def(t.List(ArrayLike_ID.input)),
  marked_by_users_ids: t.Def(t.List(ArrayLike_ID.input)),

  attached_documents: t.Def(t.List(Document.arraylike.input)),

  auth: t.Def(InstanceAuth.input)
}

module.exports = t.Entity('Comment', {
  objectDefinition: type,
  inputDefinition: input,
  contextType: ResultDataContext.type,
  requestsType: ResultRequests.type,
  errorsType: ResultErrors.type,
  eventsType: ResultEvents.type,
  countType: IndexCount.type,
  instanceDefaultType: InstanceDefault.type,
  ArrayLikeItem,
  namings
})
