const t = require('../../graphql/type')
const s = require('./../scalars')

const uuid = require('uuid')

const PersonProfile = require('./person_profile')
const PersonDocuments = require('./person_documents')
const PersonBanking = require('./person_banking')
const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('Person', {
  id: t.Def(s.ID),
  profile: t.Def(PersonProfile.type),
  documents: t.Def(PersonDocuments.type),
  banking: t.Def(PersonBanking.type)
  // GENERATED:
})

const input = t.Input('Person', {
  id: t.Def(s.ID, {
    default: uuid()
  }),
  profile: t.Def(PersonProfile.input),
  documents: t.Def(PersonDocuments.input),
  banking: t.Def(PersonBanking.input)
})

const arraylike = ArrayLikeItem("Person", { type, input })

module.exports = { type, input, arraylike }
