const t = require('../../graphql/type')
const s = require('./../scalars')

const type = t.Object('IndexCount', {
  index: t.Def(s.String),
  number: t.Def(s.Int),
  input: t.Def(s.Any),
  query: t.Def(s.JSON)
})

const input = t.Input('IndexCount', {
  index: t.Def(s.String),
  number: t.Def(s.Int),
  input: t.Def(s.Any),
  query: t.Def(s.JSON)
})

module.exports = { type, input }
