
const t = require("../../graphql/type");
const s = require("./../scalars");

const type = t.Object("Location", {
  latitude: t.Def(s.Float),
  longitude: t.Def(s.Float)
});

const input = t.Input("Location", {
  latitude: t.Def(s.Float),
  longitude: t.Def(s.Float)
});

module.exports = { type, input };
