const t = require('../../graphql/type')
const s = require('./../scalars')

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('InstanceIdentity', {
  id: t.Def(s.String),
  ref_id: t.Def(s.String),
  collection: t.Def(s.String),
  ts: t.Def(s.Long)
})

const input = t.Input('InstanceIdentity', {
  id: t.Def(s.String),
  ref_id: t.Def(s.String),
  collection: t.Def(s.String),
  ts: t.Def(s.Long)
})

const arraylike = ArrayLikeItem("InstanceIdentity", { type, input })

module.exports = { type, input, arraylike }
