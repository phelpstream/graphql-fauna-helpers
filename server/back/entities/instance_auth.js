const t = require('../../graphql/type')
const s = require('./../scalars')

const UserRole = require('./user_role')
const { ArrayLike_String } = require('./arraylike')

const type = t.Object('InstanceAuth', {
  roles_codes: t.Def(t.List(ArrayLike_String.type)),

  // Generated
  // roles: t.Def(t.List(UserRole.type), {
  //   resolve: ({ roles_code }, _, ctx) => {
  //     if (roles_code) {
  //       return ctx.binding.query.userRolesFromCodes({ roles_codes: source.roles_code }, ctx.graphql.definition(UserRole.in_context_list.name), { context: ctx }).then(get('data'))
  //     }
  //     return []
  //   }
  // })
})

const input = t.Input('InstanceAuth', {
  roles_codes: t.Def(t.List(ArrayLike_String.input))
})

module.exports = { type, input }
