const { is, isNil, isEmpty } = require('ramda')
const { get } = require('svpr')

const t = require('../../graphql/type')
const s = require('./../scalars')

const CountryCodeEnum = require('./../enums/country_code_iso2')
const ItemTranslation = require('./item_translation')
const Location = require('./location')

const city_fr = async function (source, _, ctx) {
  return is(Array, source.city_translations) ? get('content', source.city_translations.find(ct => ct.language.code === 'fr') || {}) : undefined
}
const country = async function (source, _, ctx) {
  return ctx.resources.countries.getCountry(source.country_code).country_name
}
const country_code_iso2 = async function (source, _, ctx) {
  return ctx.resources.countries.getCountry(source.country_code).iso2
}
const country_code_iso3 = async function (source, _, ctx) {
  return ctx.resources.countries.getCountry(source.country_code).iso3
}
const continent = async function (source, _, ctx) {
  return ctx.resources.countries.getCountry(source.country_code).iso3
}
const phone_code = async function (source, _, ctx) {
  return ctx.resources.countries.getCountry(source.country_code).phone_code
}
const currency = async function (source, _, ctx) {
  return ctx.resources.countries.getCountry(source.country_code).currency
}
const street_lines = async function (source, _, ctx) {
  let result = [source.additionals_1, source.additionals_2, source.street, source.street_complement].filter(i => !isNil(i) && !isEmpty(i))
  return result
}
const postal_code_city = async function (source, _, ctx) {
  return ctx.utils.texts.joinTexts([source.postal_code, source.city])
}

const type = t.Object('Address', {
  additionals_1: t.Def(s.String),
  additionals_2: t.Def(s.String),
  street_number: t.Def(s.String),
  street: t.Def(s.String),
  street_complement: t.Def(s.String),
  postal_code: t.Def(s.String),
  city: t.Def(s.String),
  city_translations: t.Def(t.List(ItemTranslation.arraylike.type)),
  region: t.Def(s.String),
  location: t.Def(Location.type),

  // ENUMS:
  country_code: t.Def(CountryCodeEnum),

  // GENERATED:
  complete: t.Def(s.String, {
    resolve: async function (source, _, ctx) {
      return Promise.all([street_lines, async source => source.postal_code, async source => source.city, country].map(fn => fn(source, _, ctx))).then(results =>
        results.filter(x => x !== null).join(', ')
      )
    }
  }),
  city_fr: t.Def(s.String, {
    resolve: city_fr
  }),
  country: t.Def(s.String, {
    resolve: country
  }),
  country_code_iso2: t.Def(s.String, {
    resolve: country_code_iso2
  }),
  country_code_iso3: t.Def(s.String, {
    resolve: country_code_iso3
  }),
  continent: t.Def(s.String, {
    resolve: continent
  }),
  phone_code: t.Def(s.String, {
    resolve: phone_code
  }),
  currency: t.Def(s.String, {
    resolve: currency
  }),
  street_lines: t.Def(t.List(s.String), {
    resolve: street_lines
  }),
  postal_code_city: t.Def(s.String, {
    resolve: postal_code_city
  })
})

const input = t.Input('Address', {
  additionals_1: t.Def(s.String),
  additionals_2: t.Def(s.String),
  street_number: t.Def(s.String),
  street: t.Def(s.String),
  street_complement: t.Def(s.String),
  postal_code: t.Def(s.String),
  city: t.Def(s.String),
  city_translations: t.Def(t.List(ItemTranslation.arraylike.input), { default: [] }),
  region: t.Def(s.String),
  country_code: t.Def(CountryCodeEnum),
  location: t.Def(Location.input)
})

module.exports = { type, input }
