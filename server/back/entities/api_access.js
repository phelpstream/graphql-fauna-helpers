
const t = require("../../graphql/type");
const s = require("./../scalars");

const type = t.Object("ApiAccess", {
  // api domain
  domain: t.Def(s.String),
  // api function name
  resource: t.Def(s.String),
  //
  name: t.Def(s.String)
});

const input = t.Input("ApiAccess", {
  name: t.Def(s.String)
});

module.exports = { type, input };
