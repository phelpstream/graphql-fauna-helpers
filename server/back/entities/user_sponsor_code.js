const t = require('../../graphql/type')
const s = require('./../scalars')

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('UserSponsorCode', {
  code: t.Def(s.String),
  segment: t.Def(s.String),
  valid_until: t.Def(s.Long),

  // Generated
  is_valid: t.Def(s.Boolean, {
    resolve: ({ valid_until }, _, ctx) => {
      let ts = ctx.date.ts(valid_until)
      return ts && valid_until > -1 ? ts >= valid_until : true
    }
  })
})

const input = t.Input('UserSponsorCode', {
  code: t.Def(s.String),
  segment: t.Def(s.String),
  valid_until: t.Def(s.Long, { default: -1 })
})

const arraylike = ArrayLikeItem("UserSponsorCode", { type, input })

module.exports = { type, input, arraylike }
