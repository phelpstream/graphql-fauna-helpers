const t = require('../../graphql/type')
const s = require('./../scalars')

const Document = require('./document')
const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('PersonBanking', {
  sepa_accounts: t.Def(
    t.List(
      ArrayLikeItem("PersonBanking_SepaAccount", {
        type: t.Object('PersonBanking_SepaAccount', {
          iban: t.Def(s.String),
          documents: t.Def(t.List(Document.type)),
          active: t.Def(s.Boolean, { default: true }),
          favorite: t.Def(s.Boolean, { default: false })
        })
      }).type
    ),
    { default: [1] }
  )
})

const input = t.Input('PersonBanking', {
  sepa_accounts: t.Def(
    t.List(
      ArrayLikeItem("PersonBanking_SepaAccount", {
        input: t.Input('PersonBanking_SepaAccount', {
          iban: t.Def(s.String),
          documents: t.Def(t.List(Document.input)),
          active: t.Def(s.Boolean, { default: true }),
          favorite: t.Def(s.Boolean, { default: false })
        })
      }).input
    ),
    { default: [1] }
  )
})

module.exports = { type, input }
