const t = require("../../graphql/type");
const s = require("./../scalars");

const type = t.Object("InstanceHistory", {
  ts: t.Def(s.Long), //t.Def(s.DateTime)
  action: t.Def(s.String),
  data: t.Def(s.JSON)
});

const input = t.Input("InstanceHistory", {
  ts: t.Def(s.Long), //t.Def(s.DateTime)
  action: t.Def(s.String),
  data: t.Def(s.JSON)
});

module.exports = { type, input };
