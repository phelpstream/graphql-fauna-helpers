const moment = require('moment-timezone')

const { is } = require('ramda')

const t = require('../../graphql/type')
const s = require('./../scalars')

// const TimeZone = require("./../enums/timezone");
const TimeZone = require('./timezone')

const type = t.Object('LocalizedTime', {
  datetime: t.Def(s.String), //t.Def(s.DateTime)
  time: t.Def(s.String),
  timezone: t.Def(TimeZone.type),

  // GENERATED:
  datetime_tz: t.Def(s.String, {
    resolve: ({ datetime, timezone }, _, ctx) => {
      if (is(String, datetime)) {
        let date
        if (is(String, timezone)) {
          date = moment.tz(datetime, timezone)
        } else {
          date = moment(datetime)
        }
        return date.isValid() ? date.toISOString() : undefined
      }
      return undefined
    }
  }),

  datetime_text: t.Def(s.String, {
    resolve: ({ datetime }, _, ctx) => {
      if (datetime) {
        return moment(datetime)
          .locale('fr')
          .format('LL')
      }
    }
  }),

  time_text: t.Def(s.String, {
    resolve: ({ time }, _, ctx) => {
      if (time) {
        return moment(time, 'HH:mm')
          .locale('fr')
          .format('LT')
      }
    }
  })
})

const input = t.Input('LocalizedTime', {
  datetime: t.Def(s.String), //t.Def(s.DateTime)
  time: t.Def(s.String),
  timezone: t.Def(TimeZone.input)
})

module.exports = { type, input }
