const t = require("../../graphql/type");
const s = require("./../scalars");

const Language = require("./language");
const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("ItemTranslation", {
  language: t.Def(Language.type),
  content: t.Def(s.String)
});

const input = t.Input("ItemTranslation", {
  language: t.Def(Language.input),
  content: t.Def(s.String)
});

const arraylike = ArrayLikeItem("ItemTranslation", { type, input })

module.exports = { type, input, arraylike };
