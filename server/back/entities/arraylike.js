
const t = require("../../graphql/type");
const s = require("./../scalars");


function ArrayLikeItem(entityName, dataObject) {

  let type = {}
  let input = {}

  if (dataObject.type) {
    type = t.Object(`ArrayLikeItem_${entityName}`, {
      key: t.Def(s.String),
      index: t.Def(s.Int),
      data: t.Def(dataObject.type)
    });
  }

  if (dataObject.input) {
    input = t.Input(`ArrayLikeItem_${entityName}`, {
      key: t.Def(s.String),
      index: t.Def(s.Int),
      data: t.Def(dataObject.input)
    });
  }

  return { type, input };
}


const ArrayLike_ID = ArrayLikeItem("ID", { type: s.ID, input: s.ID })
const ArrayLike_Any = ArrayLikeItem("Any", { type: s.Any, input: s.Any })
const ArrayLike_String = ArrayLikeItem("String", { type: s.String, input: s.String })
const ArrayLike_FaunaRef = ArrayLikeItem("FaunaRef", { type: s.FaunaRef, input: s.FaunaRef })

module.exports = { ArrayLikeItem, ArrayLike_ID, ArrayLike_Any, ArrayLike_String, ArrayLike_FaunaRef }