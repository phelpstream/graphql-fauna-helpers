const t = require('../../graphql/type')
const s = require('./../scalars')

const InstanceLog = require('./instance_log')
const LocalizedTime = require('./localized_time')

const type = t.Object('Signature', {
  base64: t.Def(s.String),
  url: t.Def(s.URL),
  date: t.Def(LocalizedTime.type),
  log: t.Def(InstanceLog.type)
})

const input = t.Input('Signature', {
  base64: t.Def(s.String),
  url: t.Def(s.URL),
  date: t.Def(LocalizedTime.input)
})

module.exports = { type, input }
