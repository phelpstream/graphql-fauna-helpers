const t = require('../../graphql/type')
const s = require('./../scalars')

const ResultRequest = require('./result_request')

const type = t.Object('ResultRequests', {
  list: t.Def(t.List(ResultRequest.arraylike.type)),
  count: t.Def(s.Int, {
    resolve: ({ list = [] }, _, ctx) => {
      return list.length
    }
  })
})

const input = t.Input('ResultRequests', {
  list: t.Def(t.List(ResultRequest.arraylike.input))
})

module.exports = { type, input }
