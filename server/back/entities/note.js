const t = require('../../graphql/type')
const s = require('./../scalars')

const InstanceLog = require('./instance_log')
const { ArrayLike_FaunaRef } = require('./arraylike')
// IS INDEX

const type = t.Object('Note', {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),
  log: t.Def(InstanceLog.type),
  content: t.Def(s.String, { default: '' }),
  associatedContents: t.Def(t.List(ArrayLike_FaunaRef))
})

const input = t.Input('Note', {
  log: t.Def(InstanceLog.input),
  content: t.Def(s.String, { default: '' }),
  associatedContents: t.Def(t.List(ArrayLike_FaunaRef), { default: [] })
})

module.exports = { type, input }
