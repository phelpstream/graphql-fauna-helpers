const t = require('../../graphql/type')
const s = require('./../scalars')

const Document = require('./document')
const InstanceAuth = require('./instance_auth')
const InstanceLog = require('./instance_log')

const ResultRequests = require('./result_requests')
const ResultErrors = require('./result_errors')
const ResultDataContext = require('./result_data_context')
const ResultEvents = require('./result_events')
const IndexCount = require('./index_count')
const InstanceDefault = require('./instance_default_value')
const InstanceIdentity = require('./instance_identity')
const namings = require('./../namings')

// IS INDEX

const type = {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),

  for_user_id: t.Def(s.ID),
  seen_by_user: t.Def(s.Boolean, { default: false }),

  for_instance: t.Def(InstanceIdentity.type),

  content: t.Def(t.NonNull(s.String)),
  url: t.Def(s.URL),

  log: t.Def(InstanceLog.type)
}

const input = {
  for_user_id: t.Def(s.ID),
  seen_by_user: t.Def(s.Boolean, { default: false }),

  for_instance: t.Def(InstanceIdentity.input),

  content: t.Def(t.NonNull(s.String)),
  url: t.Def(s.URL)
}

module.exports = t.Entity('Notification', {
  objectDefinition: type,
  inputDefinition: input,
  contextType: ResultDataContext.type,
  requestsType: ResultRequests.type,
  errorsType: ResultErrors.type,
  eventsType: ResultEvents.type,
  countType: IndexCount.type,
  instanceDefaultType: InstanceDefault.type,
  namings
})
