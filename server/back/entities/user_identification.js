const t = require('../../graphql/type')
const s = require('./../scalars')

const type = t.Object('UserIdentification', {
  auth0_id: t.Def(s.ID),
  crisp_id: t.Def(s.ID)
})

const input = t.Input('UserIdentification', {
  auth0_id: t.Def(s.ID),
  crisp_id: t.Def(s.ID)
})

module.exports = { type, input }
