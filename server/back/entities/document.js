
const t = require("../../graphql/type");
const s = require("./../scalars");

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("Document", {
  name: t.Def(s.String),
  placeholder: t.Def(s.String),
  filename: t.Def(s.String),
  handle: t.Def(s.String),
  mimetype: t.Def(s.String),
  size: t.Def(s.Int),
  url: t.Def(s.URL)
});

const input = t.Input("Document", {
  name: t.Def(s.String),
  placeholder: t.Def(s.String),
  filename: t.Def(s.String),
  handle: t.Def(s.String),
  mimetype: t.Def(s.String),
  size: t.Def(s.Int),
  url: t.Def(s.URL)
});


const arraylike = ArrayLikeItem("Document", { type, input })

module.exports = { type, input, arraylike };
