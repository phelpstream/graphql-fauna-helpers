const t = require('../../graphql/type')
const s = require('./../scalars')

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('EventsResponseItem', {
  provider: t.Def(t.NonNull(s.String)),
  data: t.Def(s.JSON)
})

const input = t.Input('EventsResponseItem', {
  _empty: t.Def(t.String)
})

const arraylike = ArrayLikeItem("EventsResponseItem", { type, input })

module.exports = { type, input, arraylike }
