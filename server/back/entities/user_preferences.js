const t = require("../../graphql/type");
const s = require("./../scalars");

const Language = require("./language");

const type = t.Object("UserPreferences", {
  language: t.Def(Language.type)
});

const input = t.Input("UserPreferences", {
  language: t.Def(Language.input)
});

module.exports = { type, input };
