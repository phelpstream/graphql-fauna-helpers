
const t = require("../../graphql/type");
const s = require("./../scalars");

const Currency = require("./currency");

const type = t.Object("MonetaryAmount", {
  value: t.Def(s.Float),
  currency: t.Def(Currency.type)
});

const input = t.Input("MonetaryAmount", {
  value: t.Def(s.Float),
  currency: t.Def(Currency.input)
});

module.exports = { type, input };
