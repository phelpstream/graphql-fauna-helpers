const t = require('../../graphql/type')
const s = require('./../scalars')

const UserPermission = require('./user_permission')
const InstanceLog = require('./instance_log')

const ResultRequests = require('./result_requests')
const ResultErrors = require('./result_errors')
const ResultDataContext = require('./result_data_context')
const ResultEvents = require('./result_events')
const IndexCount = require('./index_count')
const InstanceDefault = require('./instance_default_value')
const namings = require('./../namings')

const { ArrayLikeItem } = require("./arraylike")

const type = {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),
  name: t.Def(s.String),
  code: t.Def(s.String),
  permissions: t.Def(t.List(UserPermission.arraylike.type)),
  log: t.Def(InstanceLog.type)
}

const input = {
  name: t.Def(s.String),
  code: t.Def(s.String),
  permissions: t.Def(t.List(UserPermission.arraylike.input), { default: [] })
}

module.exports = t.Entity('UserRole', {
  objectDefinition: type,
  inputDefinition: input,
  contextType: ResultDataContext.type,
  requestsType: ResultRequests.type,
  errorsType: ResultErrors.type,
  eventsType: ResultEvents.type,
  countType: IndexCount.type,
  instanceDefaultType: InstanceDefault.type,
  namings,
  ArrayLikeItem
})
