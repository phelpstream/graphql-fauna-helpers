const t = require('../../graphql/type')
const s = require('./../scalars')

const InstanceIdentity = require('./instance_identity')

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object('EventRequest', {
  name: t.Def(s.String),
  method: t.Def(s.String),
  instanceName: t.Def(s.String),
  index: t.Def(s.String),
  idsList: t.Def(t.List(InstanceIdentity.arraylike.type)),
  definition: t.Def(s.String)
})

const input = t.Input('EventRequest', {
  name: t.Def(t.NonNull(s.String)),
  method: t.Def(s.String),
  instanceName: t.Def(t.NonNull(s.String)),
  index: t.Def(s.String),
  idsList: t.Def(t.List(t.NonNull(InstanceIdentity.arraylike.input))),
  definition: t.Def(s.String)
})

const arraylike = ArrayLikeItem("EventRequest", { type, input })

module.exports = { type, input, arraylike }
