const t = require('../../graphql/type')
const s = require('./../scalars')

const Address = require('./address')
const What3Words = require('./what_3_words')
const Phone = require('./phone')
const Email = require('./email')

const type = t.Object('ContactDetails', {
  address: t.Def(Address.type),
  words: t.Def(What3Words.type),
  phones: t.Def(t.List(Phone.arraylike.type)),
  emails: t.Def(t.List(Email.arraylike.type)),
  faxes: t.Def(t.List(Phone.arraylike.type))
})

const input = t.Input('ContactDetails', {
  address: t.Def(Address.input),
  words: t.Def(What3Words.input),
  phones: t.Def(t.List(Phone.arraylike.input), { default: [] }),
  emails: t.Def(t.List(Email.arraylike.input), { default: [] }),
  faxes: t.Def(t.List(Phone.arraylike.input), { default: [] })
})

module.exports = { type, input }
