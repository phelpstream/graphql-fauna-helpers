const t = require('../../graphql/type')
const s = require('./../scalars')

const { ArrayLikeItem, ArrayLike_Any } = require('./arraylike')

const type = t.Object('Error', {
  message: t.Def(s.String),
  locations: t.Def(t.List(ArrayLike_Any.type))
})

const input = t.Input('Error', {
  message: t.Def(s.String),
  locations: t.Def(t.List(ArrayLike_Any.input))
})

const arraylike = ArrayLikeItem("Error", { type, input })

module.exports = { type, input, arraylike }
