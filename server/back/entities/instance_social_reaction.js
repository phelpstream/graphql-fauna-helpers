
const t = require("../../graphql/type");
const s = require("./../scalars");

const InstanceLog = require("./instance_log");
const SocialReaction = require("./../enums/social_reaction");
const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("InstanceSocialReaction", {
  type: t.Def(SocialReaction.type),
  log: t.Def(InstanceLog.type)
});

const input = t.Input("InstanceSocialReaction", {
  type: t.Def(SocialReaction.input),
  log: t.Def(InstanceLog.input)
});

const arraylike = ArrayLikeItem("InstanceSocialReaction", { type, input })

module.exports = { type, input, arraylike };
