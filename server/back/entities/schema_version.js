const t = require('../../graphql/type')
const s = require('./../scalars')

const type = t.Object('SchemaVersion', {
  ts: t.Def(s.Long)
})

const input = t.Input('SchemaVersion', {})

module.exports = { type, input }
