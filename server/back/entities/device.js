
const t = require("../../graphql/type");
const s = require("./../scalars");

const InstanceLog = require("./instance_log");

const type = t.Object("Event", {
  ref_id: t.Def(s.REF_ID),
  id: t.Def(s.ID),
  device_id: t.Def(s.ID), // require('device-uuid')

  // TODO: MORE
  // https://github.com/biggora/express-useragent
  browser: t.Def(s.String),

  log: t.Def(InstanceLog.type)
});

const input = t.Input("Event", {
  name: t.Def(s.String),
  content: t.Def(s.JSON)
});

module.exports = { type, input };
