
const t = require("../../graphql/type");
const s = require("./../scalars");

const { ArrayLikeItem } = require('./arraylike')

const type = t.Object("Email", {
  name: t.Def(s.String),
  address: t.Def(s.EmailAddress)
});

const input = t.Input("Email", {
  name: t.Def(s.String),
  address: t.Def(s.EmailAddress)
});

const arraylike = ArrayLikeItem("Email", { type, input })

module.exports = { type, input, arraylike };
