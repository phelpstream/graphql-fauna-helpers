
const t = require("../../graphql/type");
const s = require("./../scalars");

const CurrencyCodeEnum = require("./../enums/currency_code");

const type = t.Object("Currency", {
  code: t.Def(CurrencyCodeEnum),

  // GENERATED:
  name: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.currencies.getCurrency(source.code).name;
    }
  }),
  name_plural: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.currencies.getCurrency(source.code).name_plural;
    }
  }),
  symbol: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.currencies.getCurrency(source.code).symbol;
    }
  }),
  symbol_native: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.currencies.getCurrency(source.code).symbol_native;
    }
  }),
  decimal_digits: t.Def(s.Int, {
    resolve: async function(source, _, ctx) {
      return ctx.resources.currencies.getCurrency(source.code).decimal_digits;
    }
  })
});

const input = t.Input("Currency", {
  code: t.Def(CurrencyCodeEnum)
});

module.exports = { type, input };
