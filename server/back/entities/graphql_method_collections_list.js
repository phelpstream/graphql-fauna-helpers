const t = require('../../graphql/type')
const s = require('./../scalars')

const GraphQLCollection = require('./graphql_collection')

const type = t.Object('GraphQLMethodCollectionsList', {
  method: t.Def(s.String),
  collections: t.Def(t.List(GraphQLCollection.arraylike.type))
})

const input = t.Input('GraphQLMethodCollectionsList', {})

module.exports = { type, input }
