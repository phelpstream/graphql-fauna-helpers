const t = require('../../graphql/type')
const s = require('./../scalars')

const type = t.Object('ResultDataContext', {
  ref: t.Def(s.Any),
  ts: t.Def(s.Long),
  next_ref_id: t.Def(s.REF_ID),
  previous_ref_id: t.Def(s.REF_ID),
  request_start: t.Def(s.Long, {
    resolve: async function(source, _, ctx) {
      // console.log('request_start', ctx.request.start)
      // return 1
      return ctx.request.start
    }
  }),
  request_start_formatted: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      if (ctx.request.start) {
        return ctx.utils.dates.formattedDate(ctx.request.start, { format: 'MMMM Do YYYY, h:mm:ss a', locale: 'en' })
      }
    }
  }),
  request_end: t.Def(s.Long, {
    resolve: async function(source, _, ctx) {
      // console.log('request_end', (source.request_duration || 0) + ctx.request.start)
      return (source.request_duration || 0) + ctx.request.start
      // return 1
    }
  }),
  request_end_formatted: t.Def(s.String, {
    resolve: async function(source, _, ctx) {
      if (ctx.request.start && source.request_duration) {
        return ctx.utils.dates.formattedDate((source.request_duration || 0) + ctx.request.start, { format: 'MMMM Do YYYY, h:mm:ss a', locale: 'en' })
      }
    }
  }),
  request_duration: t.Def(s.Long, {
    resolve: async function(source, _, ctx) {
      // console.log('request_duration', source.request_duration)
      return source.request_duration
      // return 1
    }
  })
})

const input = t.Input('ResultDataContext', {
  _empty: t.Def(s.String)
})

module.exports = { type, input }
