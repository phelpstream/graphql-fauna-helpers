
const t = require("../../graphql/type");
const s = require("./../scalars");

const type = t.Object("Patch", {
  op: t.Def(s.String),
  path: t.Def(s.String),
  value: t.Def(s.JSON)
});

const input = t.Input("Patch", {
  op: t.Def(s.String),
  path: t.Def(s.String),
  value: t.Def(s.JSON)
});

module.exports = { type, input };
