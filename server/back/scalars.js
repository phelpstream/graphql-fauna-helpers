module.exports = {
  FaunaRef: require("./scalars/fauna_ref"),
  Any: require("./scalars/any"),
  ...require("./scalars/externals")
};
