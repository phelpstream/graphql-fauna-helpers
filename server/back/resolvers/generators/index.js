const Debug = require('debug')
const debug = Debug('gfh-server [back:resolvers:generators:index]')

const { is, path, pick } = require('ramda')
const { getOr, get } = require('svpr')

const namings = require('./../../namings')
const eventNames = require('./../../events/names')
const InstanceLogHelper = require('./../../entities/instance_log').helper

const defaultValue = require('./../../../utils/defaultValue')

const instanceDefault = instanceName => ({
  [namings.instanceDefault(instanceName)]({ typeName, skipEvent }, ctx, info) {
    return {
      type: typeName,
      defaultValue: ctx.schema.typeMap[typeName]
        ? defaultValue(typeName, ctx.schema.typeMap)
        : {
          error: 'Type not found'
        }
    }
  }
})

const instanceCreate = instanceName => ({
  [namings.instanceCreate(instanceName)]({ input, password, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = { ...input, log: InstanceLogHelper.own(user_id, InstanceLogHelper.create(user_id, getOr({}, 'log', input))) }
    debug('instanceCreate data:', data)
    return ctx
      .query(ctx.fq.instanceCreate(namings.collection(instanceName), { input: data, credentials: { password } }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.create, instanceName, skipEvent }, ctx))
  }
})

const instanceImport = instanceName => ({
  [namings.instanceImport(instanceName)]({ input, password, owner_id, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = { ...input, log: InstanceLogHelper.import(user_id, InstanceLogHelper.own(owner_id || user_id, getOr({}, 'log', input))) }
    return ctx
      .query(ctx.fq.instanceCreate(namings.collection(instanceName), { input: data, credentials: { password } }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.create, instanceName, skipEvent }, ctx))
  }
})

const instancesImport = instanceName => ({
  [namings.instancesImport(instanceName)]({ input, password, owner_id, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = input.map(item => ({ ...item, log: InstanceLogHelper.import(user_id, InstanceLogHelper.own(owner_id || user_id, getOr({}, 'log', input))) }))
    return ctx
      .query(ctx.fq.instancesCreate(namings.collection(instanceName), { input: data, credentials: { password } }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.create, instanceName, skipEvent }, ctx))
  }
})

const instancesCreate = instanceName => ({
  [namings.instancesCreate(instanceName)]({ input = [], skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = input.map(i => ({ ...i, log: InstanceLogHelper.own(user_id, InstanceLogHelper.create(user_id, getOr({}, 'log', i))) }))
    return ctx
      .query(ctx.fq.instancesCreate(namings.collection(instanceName), { input: data }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.create, instanceName, skipEvent }, ctx))
  }
})

const instancePatch = instanceName => ({
  [namings.instancePatch(instanceName)]({ ref_id, input, replaceFn, argumentsModifier, instanceType, instanceDefinition, skipEvent }, ctx, info) {
    return ctx.fm
      .instancePatch(ctx)(namings.collection(instanceName), { ref_id, input, replaceFn, argumentsModifier, instanceType, instanceDefinition })
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.patch, instanceName, skipEvent }, ctx))
  }
})

const instanceUpdateOwner = instanceName => ({
  [namings.instanceUpdateOwner(instanceName)]({ ref_id, user_id, skipEvent }, ctx, info) {
    const user_id_query = ctx.storage.user.id
    const data = { log: InstanceLogHelper.ownershipTo({ user_id: user_id_query, to: user_id }) }
    return ctx
      .query(ctx.fq.instanceUpdate(namings.collection(instanceName), { ref_id, input: data }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.update, instanceName, skipEvent }, ctx))
    // NOTE: Add eventNames.ownership
  }
})


const instanceUpdateJSON = instanceName => ({
  [namings.instanceUpdateJSON(instanceName)]({ ref_id, input, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = { ...input, log: InstanceLogHelper.update(user_id) }
    return ctx
      .query(ctx.fq.instanceUpdate(namings.collection(instanceName), { ref_id, input: data }))
      .then(ctx.fm.addDataContext(ctx))
      // .then(result => get(`data.${namings.instanceUpdateJSON(instanceName)}.data`, result))
      .then(ctx.events.dispatch({ name: eventNames.update, instanceName, skipEvent }, ctx))
  }
})

const instanceUpdate = instanceName => ({
  [namings.instanceUpdate(instanceName)]({ ref_id, input, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = { ...input, log: InstanceLogHelper.update(user_id) }
    return ctx
      .query(ctx.fq.instanceUpdate(namings.collection(instanceName), { ref_id, input: data }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.update, instanceName, skipEvent }, ctx))
  }
})

const instanceReplace = instanceName => ({
  [namings.instanceReplace(instanceName)]({ ref_id, input, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = { ...input }
    return ctx
      .query(ctx.fq.instanceReplace(namings.collection(instanceName), { ref_id, input: data }))
      .then(res => {
        // Add update time info
        ctx.query(ctx.fq.instanceUpdate(namings.collection(instanceName), { ref_id, input: { log: InstanceLogHelper.replace(user_id) } }))
        return res
      })
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.update, instanceName, skipEvent }, ctx))
    // NOTE: Add eventNames.replace
  }
})

const instanceUpdateListItem = instanceName => ({
  [namings.instanceUpdateListItem(instanceName)](
    { instanceType, instanceDefinition, id, ref_id, listPath, uniquePath, uniqueValue, input, remove, updateFn, argumentsModifier, skipEvent } = {},
    ctx,
    info
  ) {
    return ctx.fm
      .instanceUpdateListItem(ctx)(namings.collection(instanceName), { instanceType, instanceDefinition, id, ref_id, listPath, uniquePath, uniqueValue, input, remove, updateFn, argumentsModifier })
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.update, instanceName, skipEvent }, ctx))
  }
})

const instanceOwnMatch = instanceName => ({
  [namings.instanceOwnMatch(instanceName)]({ index, value, values, user_id, skipEvent }, ctx, info) {
    user_id = user_id || ctx.storage.user.id
    let processedMatchValue = values && is(Array, values) ? values : value
    return ctx
      .query(ctx.fq.instanceOwnMatch(namings.collection(instanceName), { user_id, index, matchValue: processedMatchValue }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instanceOwn = instanceName => ({
  [namings.instanceOwn(instanceName)]({ id, ref_id, user_id, skipEvent }, ctx, info) {
    user_id = user_id || ctx.storage.user.id
    return ctx
      .query(ctx.fq.instanceOwn(namings.collection(instanceName), { id, ref_id, user_id }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instanceOwnById = instanceName => ({
  [namings.instanceOwnById(instanceName)]({ id, user_id, skipEvent }, ctx, info) {
    user_id = user_id || ctx.storage.user.id
    return ctx
      .query(ctx.fq.instanceOwnMatch(namings.collection(instanceName), { user_id, index: `${namings.collection(instanceName)}_by_id`, matchValue: id, paginateOptions: { size: 1 } }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instancesOwn = instanceName => ({
  [namings.instancesOwn(instanceName)]({ user_id, paginateOptions, skipEvent }, ctx, info) {
    user_id = user_id || ctx.storage.user.id
    return ctx
      .query(ctx.fq.instancesOwn(namings.collection(instanceName), { user_id, paginateOptions }))
      .then(res => {
        console.log('res', res)
        return res
      })
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.getAll, instanceName, skipEvent }, ctx))
  }
})

const instance = instanceName => ({
  [namings.instance(instanceName)]({ ref_id, ids, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instance(namings.collection(instanceName), { ref_id, ids }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instanceById = instanceName => ({
  [namings.instanceById(instanceName)]({ id, skipEvent }, ctx, info) {
    // debug('resolver.instanceById')
    return ctx
      .query(ctx.fq.instanceMatch(namings.collection(instanceName), { index: `${namings.collection(instanceName)}_by_id`, matchValue: id, paginateOptions: { size: 1 } }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instanceSync = instanceName => ({
  [namings.instanceSync(instanceName)]({ ref_id, typeName, skipEvent }, ctx, info) {
    return ctx.binding.query[namings.instance(instanceName)]({ ref_id, skipEvent: true }, ctx.graphql.definition(typeName), { context: ctx }).then(
      ctx.events.dispatch({ name: eventNames.sync, instanceName, skipEvent }, ctx)
    )
  }
})

const instanceSyncById = instanceName => ({
  [namings.instanceSyncById(instanceName)]({ id, typeName, skipEvent }, ctx, info) {
    return ctx.binding.query[namings.instanceById(instanceName)]({ id, skipEvent: true }, ctx.graphql.definition(typeName), { context: ctx }).then(
      ctx.events.dispatch({ name: eventNames.sync, instanceName, skipEvent }, ctx)
    )
  }
})

const instanceMatch = instanceName => ({
  [namings.instanceMatch(instanceName)]({ index, value, values, matchValue, skipEvent }, ctx, info) {
    let processedMatchValue = matchValue || (values && is(Array, values)) ? values : value
    return ctx
      .query(ctx.fq.instanceMatch(namings.collection(instanceName), { index, matchValue: processedMatchValue }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instances = instanceName => ({
  [namings.instances(instanceName)]({ index, matchValue, paginateOptions, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instances(namings.collection(instanceName), { index, matchValue, paginateOptions }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.getAll, instanceName, skipEvent }, ctx))
  }
})

const instancesSync = instanceName => ({
  [namings.instancesSync(instanceName)]({ index, matchValue, paginateOptions = {}, typeName, skipEvent }, ctx, info) {
    paginateOptions.size = getOr(30, 'size', paginateOptions)

    return ctx.binding.query[namings.instances(instanceName)]({ index, matchValue, paginateOptions }, ctx.graphql.definition(typeName), { context: ctx })

      .then(response => {
        // debug('response', response)
        let nextId = get('context.next_ref_id', response)
        debug('context.next_ref_id', nextId)

        // debug('instancesSynced', response.data.map((rd = {}) => `${rd.id} :: ${rd.ref_id}`))
        let requests = []
        if (nextId) {
          requests.push({
            method: namings.instancesSync(instanceName),
            variables: { index, matchValue, paginateOptions: { size: paginateOptions.size, after: nextId } }
          })

          response.requests = requests

          ctx.aws.events.triggerBinding(
            {
              query: true,
              method: namings.instancesSync(instanceName),
              args: { index, matchValue, paginateOptions: { size: paginateOptions.size, after: nextId } },
              topicArn: ctx.aws.GRAPHQL_API_TOPIC_ARN
            },
            ctx
          )
        }

        return response
      })
      .then(ctx.events.dispatch({ name: eventNames.syncList, instanceName, skipEvent }, ctx))
  }
})

const instancesList = instanceName => ({
  [namings.instancesList(instanceName)]({ index, matchValue, ref_ids, idsList, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instancesList(namings.collection(instanceName), { index, matchValue, ref_ids, idsList }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.getAll, instanceName, skipEvent }, ctx))
  }
})

const instancesLast = instanceName => ({
  [namings.instancesLast(instanceName)]({ size, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instancesLast(namings.collection(instanceName), { size }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.get, instanceName, skipEvent }, ctx))
  }
})

const instanceExists = instanceName => ({
  [namings.instanceExists(instanceName)]({ ref_id, skipEvent }, ctx, info) {
    return ctx.query(ctx.fq.instanceExists(namings.collection(instanceName), { ref_id })).then(ctx.fm.addDataContext(ctx))
  }
})

const instanceByIdExists = instanceName => ({
  [namings.instanceByIdExists(instanceName)]({ id, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instanceMatchExists(namings.collection(instanceName), { index: `${namings.collection(instanceName)}_by_id`, matchValue: id }))
      .then(ctx.fm.addDataContext(ctx, { boolean: true }))
  }
})

const instanceSoftDelete = instanceName => ({
  [namings.instanceSoftDelete(instanceName)]({ ref_id, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    const data = { log: InstanceLogHelper.delete(user_id) }
    return ctx
      .query(ctx.fq.instanceUpdate(namings.collection(instanceName), { ref_id, input: data }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.softDelete, instanceName, skipEvent }, ctx))
  }
})

const instanceDelete = instanceName => ({
  [namings.instanceDelete(instanceName)]({ ref_id, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instanceDelete(namings.collection(instanceName), { ref_id }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.delete, instanceName, skipEvent }, ctx))
  }
})

const instancesDelete = instanceName => ({
  [namings.instancesDelete(instanceName)]({ ref_ids, skipEvent }, ctx, info) {
    return ctx
      .query(ctx.fq.instancesDelete(namings.collection(instanceName), { ref_ids }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.deleteList, instanceName, skipEvent }, ctx))
  }
})

const instancesDeleteAllOwn = instanceName => ({
  [namings.instancesDeleteAllOwn(instanceName)]({ skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    return ctx
      .query(ctx.fq.instancesDeleteAllOwn(namings.collection(instanceName), { user_id }))
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.deleteList, instanceName, skipEvent }, ctx))
  }
})

const instancesOwnCount = instanceName => ({
  [namings.instancesOwnCount(instanceName)]({ index, skipEvent }, ctx, info) {
    const user_id = ctx.storage.user.id
    return ctx.query(ctx.fm
      .indexCount(`${instanceName}s`, { index, queryDefinition: ctx.fq.instancesOwnDef(namings.collection(instanceName), { user_id }) }))
      .then(ctx.fm.addDataContext(ctx))

      .then(ctx.events.dispatch({ name: eventNames.indexCount, instanceName, skipEvent }, ctx))
  }
})

const instancesCount = instanceName => ({
  [namings.instancesCount(instanceName)]({ index, value, values, skipEvent }, ctx, info) {
    return ctx.fm
      .indexCount(namings.collection(instanceName), { index, matchValue: value, matchValues: values }, ctx)
      .then(ctx.fm.addDataContext(ctx))
      .then(ctx.events.dispatch({ name: eventNames.indexCount, instanceName, skipEvent }, ctx))
  }
})

// const middleware = fn => (args, ctx, info) => {

//   return fn(args, ctx, info)
// }

const CRUD = instanceName => {
  //
  let generators = [
    instanceImport,
    instancesImport,
    instanceSync,
    instanceSyncById,
    instanceReplace,
    instancePatch,
    instanceDefault,
    instanceOwn,
    instanceOwnById,
    instanceOwnMatch,
    instancesOwn,
    instancesOwnCount,
    instanceMatch,
    instancesCreate,
    instanceById,
    instanceCreate,
    instanceUpdateOwner,
    instanceUpdate,
    instanceUpdateJSON,
    instanceUpdateListItem,
    instance,
    instances,
    instancesSync,
    instancesLast,
    instancesList,
    instanceExists,
    instanceByIdExists,
    instanceSoftDelete,
    instanceDelete,
    instancesDelete,
    instancesDeleteAllOwn,
    instancesCount
  ]
  return generators.reduce((rsv, generator) => {
    return { ...generator(instanceName), ...rsv }
    // .reduce((pile, fn, key) => {
    //   pile[key] =
    //   return pile
    // }, {})
  }, {})
}

module.exports = {
  CRUD,
  instanceImport,
  instancesImport,
  instancePatch,
  instanceReplace,
  instanceDefault,
  instancesLast,
  instanceOwnMatch,
  instanceOwn,
  instanceOwnById,
  instancesOwn,
  instancesOwnCount,
  instanceMatch,
  instancesCreate,
  instanceById,
  instanceSync,
  instanceSyncById,
  instanceCreate,
  instanceUpdateOwner,
  instanceUpdate,
  instanceUpdateJSON,
  instanceUpdateListItem,
  instance,
  instances,
  instancesSync,
  instancesList,
  instanceExists,
  instanceByIdExists,
  instanceSoftDelete,
  instancesDeleteAllOwn,
  instanceDelete,
  instancesDelete,
  instancesCount
}
